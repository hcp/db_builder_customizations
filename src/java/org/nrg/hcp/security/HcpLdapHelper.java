// Copyright 2012 Washington University School of Medicine All Rights Reserved
package org.nrg.hcp.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.servlet.http.HttpSession;

import org.json.*;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.entities.XDATUserDetails;
import org.nrg.xdat.om.XdatUser;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.security.UserGroup;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFT;
import org.nrg.xft.XFTItem;
import org.nrg.xft.db.ViewManager;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.schema.design.SchemaElementI;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.utils.SaveItemHelper;

import com.google.common.collect.Iterators;

public class HcpLdapHelper {

	static org.apache.log4j.Logger logger = Logger
			.getLogger(HcpLdapHelper.class);

	// LDAP server to connect to
	private static String LDAP_HOST = "ldap://path.to.ldap.host";

	// account to use when connecting to LDAP server to verify account details
	private static String LDAP_USER = "CN=xnat-account,OU=Users,DC=your,DC=edu";
	private static String SEARCHBASE = "dc=your,dc=edu";
	private static String LDAP_PASS = "PASSWORD";

	private static String INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	private static String SECURITY_AUTHENTICATION = "simple";
	private static String REFERRAL = "follow";

	// search used to query for the user's distinguishedName
	private static String SEARCH_TEMPLATE = "(&(objectClass=user)(CN=%USER%))";
	private static String LDAP_USER_PK = "distinguishedName";

	private static String LDAP_TO_XNAT_PK_CACHE = "xdat:user/quarantine_path";

	private static String[] USER_CLASS = {};

	private final ArrayList<String> groupNames = new ArrayList<String>();
	private final Map<String, Map<AccessProp, String[]>> groupMap = new HashMap<String, Map<AccessProp, String[]>>();

	private static int AUTHENTICATION_EXPIRATION = 3600; // seconds

	private static boolean IGNORE_MULTIPLE_MATCHES = false;
	private static boolean CREATE_MISSING_ACCOUNTS = true;

	// HCP-specific fields
	private static boolean AUTO_ENABLE_ACCOUNTS = true;
	private static String USER_CONTEXT;

	private static final String XDAT_USER_ELEMENT = "xdat:user";

	private final static Map<String, String> params = new HashMap<String, String>();

	private LdapContext ctx;
	private Hashtable<String, String> env;

	static File AUTH_PROPS = null;

	public enum DUTStatus {
		TRUE, FALSE, MOD
	};

	public enum AccessProp {
		ldapGroup, xnatGroups, removeGroups, dutMessage
	}

	private static HcpLdapHelper helper;

	public static HcpLdapHelper getInstance() throws HcpLdapException {
		if (helper == null) {
			helper = new HcpLdapHelper();
		}
		return helper;
	}

	private HcpLdapHelper() throws HcpLdapException {

			/*
			 * NOTE: Not using configuration service for now ConfigService
			 * configService = XDAT.getConfigService(); String config =
			 * configService.getConfigContents("ldap","groups.xml"); //
			 * convert String into InputStream InputStream is = new
			 * ByteArrayInputStream(config.getBytes()); Properties
			 * groupProps = new Properties(); groupProps.loadFromXML(is);
			 */

			File AUTH_PROPS = new File(XFT.GetConfDir(), "services.properties");
			InputStream inputs;
			final Properties properties = new Properties();
			try {
				inputs = new FileInputStream(AUTH_PROPS);
				properties.load(inputs);
			} catch (FileNotFoundException e) {
				throw new HcpLdapException("Authentication file does not exist",e);
			} catch (IOException e) {
				throw new HcpLdapException("Could not read authentication file",e);
			}

			if (properties.containsKey("LDAP_HOST"))
				LDAP_HOST = properties.getProperty("LDAP_HOST");
			else
				throw new HcpLdapException("Missing LDAP_HOST");

			if (properties.containsKey("LDAP_USER"))
				LDAP_USER = properties.getProperty("LDAP_USER");
			else
				throw new HcpLdapException("Missing LDAP_USER");

			if (properties.containsKey("SEARCHBASE"))
				SEARCHBASE = properties.getProperty("SEARCHBASE");
			else
				throw new HcpLdapException("Missing SEARCHBASE");

			if (properties.containsKey("LDAP_PASS"))
				LDAP_PASS = properties.getProperty("LDAP_PASS");
			else
				throw new HcpLdapException("Missing LDAP_PASS");

			if (properties.containsKey("INITIAL_CONTEXT_FACTORY"))
				INITIAL_CONTEXT_FACTORY = properties
						.getProperty("INITIAL_CONTEXT_FACTORY");
			else
				throw new HcpLdapException("Missing INITIAL_CONTEXT_FACTORY");

			if (properties.containsKey("SECURITY_AUTHENTICATION"))
				SECURITY_AUTHENTICATION = properties
						.getProperty("SECURITY_AUTHENTICATION");
			else
				throw new HcpLdapException("Missing SECURITY_AUTHENTICATION");

			if (properties.containsKey("REFERRAL"))
				REFERRAL = properties.getProperty("REFERRAL");
			else
				throw new HcpLdapException("Missing REFERRAL");

			if (properties.containsKey("SEARCH_TEMPLATE"))
				SEARCH_TEMPLATE = properties.getProperty("SEARCH_TEMPLATE");
			else
				throw new HcpLdapException("Missing SEARCH_TEMPLATE");

			if (properties.containsKey("LDAP_USER_PK"))
				LDAP_USER_PK = properties.getProperty("LDAP_USER_PK");
			else
				throw new HcpLdapException("Missing LDAP_USER_PK");

			if (properties.containsKey("USER_CLASS"))
				USER_CLASS = properties.getProperty("USER_CLASS").split(
						"[;:]");

			if (properties.containsKey("IGNORE_MULTIPLE_MATCHES"))
				IGNORE_MULTIPLE_MATCHES = Boolean.parseBoolean(properties
						.getProperty("IGNORE_MULTIPLE_MATCHES"));

			if (properties.containsKey("CREATE_MISSING_ACCOUNTS"))
				CREATE_MISSING_ACCOUNTS = Boolean.parseBoolean(properties
						.getProperty("CREATE_MISSING_ACCOUNTS"));

			if (properties.containsKey("AUTHENTICATION_EXPIRATION"))
				AUTHENTICATION_EXPIRATION = Integer.parseInt(properties
						.getProperty("AUTHENTICATION_EXPIRATION"));
			else
				throw new HcpLdapException("Missing AUTHENTICATION_EXPIRATION");
			
			if (properties.containsKey("AUTO_ENABLE"))
				AUTO_ENABLE_ACCOUNTS = properties
						.getProperty("AUTO_ENABLE").trim()
						.equalsIgnoreCase("Y");
			
			if (properties.containsKey("USER_CONTEXT"))
				USER_CONTEXT = properties.getProperty("USER_CONTEXT");
			else
				throw new HcpLdapException("Missing USER_CONTEXT");

			params.put("CN", "xdat:user/login");
			params.put("givenName", "xdat:user/firstname");
			params.put("sn", "xdat:user/lastname");
			params.put("mail", "xdat:user/email");
			params.put(LDAP_USER_PK, LDAP_TO_XNAT_PK_CACHE);

			////////////////////////////////////////
			// LOADED FROM CONFIGURATION SERVICE) //
			////////////////////////////////////////
			populateGroupMapFromConfig();
	}

	private void populateGroupMapFromConfig() throws HcpLdapException {

		String config = XDAT.getConfigService().getConfigContents("ldap", "access.json");
		if (config == null) {
			throw new HcpLdapException("No LDAP group config found");
		}
		JSONArray accessList = null;

		try {
			JSONObject obj = new JSONObject(config);
			accessList = obj.getJSONArray("accessList");
		} catch (JSONException e) {
			logger.error(e);
		}

		for (int i = 0; i < accessList.length(); i++) {
			// Populate groupMap
			Map<AccessProp, String[]> accessMap = new HashMap<AccessProp, String[]>();

			try {
				String groupKey = accessList.getJSONObject(i).getString("name").toUpperCase();
				groupNames.add(i, groupKey.toUpperCase());

				// Create map for all Access Property keys
				for (AccessProp key : AccessProp.values()) {
					Object value = new Object();

					if (!accessList.getJSONObject(i).isNull((key.toString()))) {
						value = accessList.getJSONObject(i).get(key.toString());
						ArrayList<String> valList = new ArrayList<String>();

						if (value instanceof String) {
							valList.add(0, value.toString());
						} else if (value instanceof JSONArray) {
							JSONArray values = (JSONArray) value;
							for (int j = 0; j < values.length(); j++) {
								valList.add(values.getString(j));
							}
						}
						String[] valArray = valList.toArray(new String[valList.size()]);
						accessMap.put(key, valArray);
					}
				}
				groupMap.put(groupKey, accessMap);
			} catch (JSONException e) {
				logger.error(e);
			}
		}
	}

	public static JSONArray getTierConfig() throws HcpLdapException {
		String config = XDAT.getConfigService().getConfigContents("tiers", "setup.json");
		if (config == null) {
			throw new HcpLdapException("No access tiers config found");
		}
		JSONArray tiers = null;

		try {
			JSONObject obj = new JSONObject(config);
			tiers = obj.getJSONArray("tierConfig");
		} catch (JSONException e) {
			logger.error(e);
		}
		return tiers;
	}

	/*
	Combine the user's group membership with the tiered data access config and return a data access map
	*/
	public Map<String, Object> getUserTiers(XDATUser user) {
		Map<String, Object> userTiers = new HashMap<String, Object>();
		JSONArray tierConfig;

		try {
			tierConfig = getTierConfig();
		}
		catch (Exception e) {
			logger.error(e);
			return null;
		}

		// Go through each project's tiers
		for (int i = 0; i < tierConfig.length(); i++) {
			String projectId = null;
			JSONArray projectTiers = null;

			try {
				projectTiers = tierConfig.getJSONObject(i).getJSONArray("tiers");
				projectId = tierConfig.getJSONObject(i).getString("project");
			}
			catch (JSONException e) {
				logger.error(e);
			}

			// Initialize tiers map for project
			if (!userTiers.containsKey(projectId)) {
				Map<String, Object> tiersMap = new HashMap<String, Object>();
				ArrayList<String> tiersDisplay = new ArrayList<String>();

				tiersMap.put("displayNames", tiersDisplay);
				tiersMap.put("accessLevel", -1);
				userTiers.put(projectId, tiersMap);
			}

			Map<String, Object> projTiersMap = (HashMap)userTiers.get(projectId);

			// Find the user's tier level for this project
			int tierLevel = getUserTierLevel(user, projectTiers);
			projTiersMap.put("accessLevel", tierLevel);

			// Add all tiers up to and including the users access level
			ArrayList<String> tierDisplayNameList = new ArrayList<String>();

			for (int t = 0; t <= tierLevel; t++) {
				try {
					String displayName = projectTiers.getJSONObject(t).getString("displayName");
					tierDisplayNameList.add(displayName);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			projTiersMap.put("displayNames", tierDisplayNameList);
		}
		return userTiers;
	}

	public int getUserTierLevel(XDATUser user, JSONArray projectTiers) {
		Hashtable<String, UserGroup> userGroups = user.getGroups();
		int tierLevel = -1;

		for (int j = 0; j < projectTiers.length(); j++) {
			String xnatGroup = null;
			try {
				xnatGroup = projectTiers.getJSONObject(j).getString("xnatGroup");
			}
			catch (JSONException e) {
				logger.error(e);
			}

			if (userGroups.containsKey(xnatGroup)) {
				try {
					String tierStr = projectTiers.getJSONObject(j). getString("tier");
					tierLevel = Integer.parseInt(tierStr);
				}
				catch (JSONException e) {
					logger.error(e);
				}
			}
		}
		return tierLevel;
	}

	private void closeContext() {
		try {
			if (ctx != null)
				ctx.close();
			ctx = null;
		} catch (NamingException e) {
			logger.error("Problem during close", e);

		}
	}

	private void openContext(String user, String pass) throws NamingException {
		try {
			env = new Hashtable<String, String>();
			env.put(Context.SECURITY_PRINCIPAL, user);
			env.put(Context.SECURITY_CREDENTIALS, pass);
			env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
			env.put(Context.SECURITY_AUTHENTICATION, SECURITY_AUTHENTICATION);
			env.put(Context.PROVIDER_URL, LDAP_HOST);
			// FOR NOW, we're not using SSL for LDAP connections
			//env.put(Context.SECURITY_PROTOCOL, "ssl");
			env.put(Context.REFERRAL, REFERRAL);

			// Create the initial directory context
			ctx = new InitialLdapContext(env, null);
		} catch (NamingException e) {
			throw e;
		}
	}

	/**
	 * Gets LDAP information for users based on search filter
	 * 
	 * Step 1: query the server for a list of matching users (based on search
	 * filter and submitted cred) Step 2: use the results to populate XDATUser
	 * objects
	 * 
	 * @param cred
	 *            The JNDI search filter you want to use
	 * @return Array of LoginBean objects for users found.
	 */
	private XDATUser getUsers(Credentials cred) {
		return getUsers(cred.getUsername());
	}

	private XDATUser getUsers(String username) {
		logger.debug("\n\ngetUsers:" + username);
		final String searchFilter = buildSearchFilter(username);
		XDATUser loginBean = null;
		final ArrayList<XDATUser> users = new ArrayList<XDATUser>();
		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		logger.debug(params.toString());
		final String[] returnedAtts = new String[params.size()];
		int c = 0;
		for (final Map.Entry<String, String> e : params.entrySet()) {
			returnedAtts[c++] = e.getKey();
		}

		// String returnedAtts[] = {"displayName", "sn", "givenName",
		// "sAMAccountName", "mail", "department", "telephoneNumber", "company",
		// "operations", "memberOf"};
		searchCtls.setReturningAttributes(returnedAtts);
		try {
			this.openContext(LDAP_USER, LDAP_PASS);
			logger.debug(ctx.getEnvironment().toString());
			logger.debug("SEARCHBASE:" + SEARCHBASE);
			logger.debug("FILTER:" + searchFilter);
			logger.debug("searchCtls:" + searchCtls.toString());
			for (String s : returnedAtts) {
				logger.debug("returnedAtts:" + s);
			}
			// Search for objects using the filter
			final NamingEnumeration answer = ctx.search(SEARCHBASE,
					searchFilter, searchCtls);
			int match = 0;
			// Loop through the search results
			while (answer.hasMoreElements()) {
				final SearchResult sr = (SearchResult) answer.next();
				final Attributes attrs = sr.getAttributes();
				logger.debug("getUsers:answer[" + match++ + "]=" + sr.getName()
						+ " " + sr.getClassName() + " " + attrs.size());

				try {
					final XFTItem i = XFTItem.NewItem("xdat:user", null);

					if (attrs != null) {
						try {
							// multiple attributes returned from server need to
							// be mapped to a single xdat:user
							for (NamingEnumeration ae = attrs.getAll(); ae
									.hasMore();) {
								Attribute attr = (Attribute) ae.next();

								logger.debug("getUsers:answer:row:attr:"
										+ attr.getID() + "=" + attr.get());
								for (final Map.Entry<String, String> e : params
										.entrySet()) {
									if (e.getKey().equalsIgnoreCase(
											attr.getID())) {
										i.setProperty(e.getValue(), attr.get());
										break;
									}

								}
							}
						} catch (NamingException e) {
							logger.error(username
									+ ":getUser:Error retrieving data for "
									+ " from results", e);
						}
					}
					loginBean = new XDATUser(i);
					if (loginBean.getUsername() == null) {
						logger.error(username + ":getUser:Missing login");
					} else {
						users.add(loginBean);
					}
				} catch (XFTInitException e) {
					logger.error("Problem during getUsers", e);
				} catch (ElementNotFoundException e) {
					logger.error("Problem during getUsers", e);
				} catch (FieldNotFoundException e) {
					logger.error("Problem during getUsers", e);
				} catch (Exception e) {
					logger.error("Problem during getUsers", e);
				}
			}

		} catch (NamingException e) {
			logger.error("Problem during getUsers", e);
		} finally {
			this.closeContext();
		}

		if (users.size() == 1) {
			logger.info(username + ":getUser:Matched user account:"
					+ users.get(0));
			return users.get(0);
		} else if (users.size() == 0) {
			logger.error(username + ":getUser:No Results Found");
			return null;
		} else {
			if (IGNORE_MULTIPLE_MATCHES) {
				logger.error(username
						+ ":getUser:Multiple matches ignored- returning first:"
						+ users.get(0));
				return users.get(0);
			} else {
				logger.error(username + ":getUser:Multiple Records Found for "
						+ username + " Account");
				return null;
			}
		}
	}

	private String buildSearchFilter(Credentials cred) {
		return StringUtils.replace(SEARCH_TEMPLATE, "%USER%",
				cred.getUsername());
	}

	private String buildSearchFilter(String username) {
		return StringUtils.replace(SEARCH_TEMPLATE, "%USER%", username);
	}

	/**
	 * Query server for the DN for this cred
	 * 
	 * @param cred
	 * @return String DN
	 */
	private String getDN(Credentials cred) {
		logger.debug("\n\ngetDN:" + cred.getUsername());
		final String searchFilter = buildSearchFilter(cred);
		return getDN(cred.getUsername(), searchFilter);
	}

	private String getDN(String username) {
		logger.debug("\n\ngetDN:" + username);
		final String searchFilter = buildSearchFilter(username);
		return getDN(username, searchFilter);
	}

	private String getDN(String username, String searchFilter) {

		ArrayList<String> dns = new ArrayList<String>();
		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { LDAP_USER_PK };
		searchCtls.setReturningAttributes(returnedAtts);
		try {
			this.openContext(LDAP_USER, LDAP_PASS);
			logger.debug(ctx.getEnvironment().toString());
			logger.debug("SEARCHBASE:" + SEARCHBASE);
			logger.debug("FILTER:" + searchFilter);
			logger.debug("searchCtls:" + searchCtls.toString());
			// Search for objects using the filter
			final NamingEnumeration answer = ctx.search(SEARCHBASE,
					searchFilter, searchCtls);

			// Loop through the search results
			while (answer.hasMoreElements()) {
				final SearchResult sr = (SearchResult) answer.next();
				final Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (final NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							final Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							for (final NamingEnumeration e = attr.getAll(); e
									.hasMore();) {
								if (attr.getID().equalsIgnoreCase(LDAP_USER_PK)) {
									dns.add((String) e.next());
								}
							}
						}
					} catch (NamingException e) {
						logger.error(username + ":Error retrieving DN for "
								+ " from results", e);
					}
				}
			}

		} catch (NamingException e) {
			logger.error(username + ":Error retrieving DN for " + username
					+ " from server", e);
		} finally {
			this.closeContext();
		}

		if (dns.size() == 1) {
			return dns.get(0);
		} else if (dns.size() == 0) {
			logger.info(username + ":No DN Found for " + username + " Account");
			return null;
		} else {
			logger.info(username + ":Multiple DNs Found for " + username
					+ " Account");
			return null;
		}
	}

	private String retrieveStoredDN(XDATUser u) {
		// this version stores the DN in a dead field (quarantine_path). This
		// prevented having to modify the user schema, but really seems
		// in-appropriate.
		return u.getQuarantinePath();
	}

	private void updateStoredDN(final String newDN, XDATUser u) {
		try {
			u.setQuarantinePath(newDN);
			SaveItemHelper.authorizedSave(u, null, true, false, true, false,
					EventUtils.newEventInstance(EventUtils.CATEGORY.SIDE_ADMIN,
							EventUtils.TYPE.PROCESS, "Created user from LDAP"));
		} catch (Exception e) {
			logger.error(u.getUsername()
					+ ":Failed to update stored DN for user. Proceeding...", e);
		}
	}

	public boolean isAutoEnabled() {
		return AUTO_ENABLE_ACCOUNTS;
	}

	public static String RegisterNewLdapAccount(String username, String email,
			String pw, String firstname, String lastname, String institution,
			String duAccepted, String wbAccepted) throws RegistrationException {
		try {
			final HcpLdapHelper ldap = getInstance();
			return ldap.registerNewLdapAccount(username, email, pw, firstname,
					lastname, institution, duAccepted, wbAccepted);
		} catch (HcpLdapException e) {
			throw new RegistrationException("Server-side registration error",e);
		}
	}

	public synchronized String registerNewLdapAccount(String username, String email,
			String pw, String firstname, String lastname, String institution,
			String duAccepted, String wbAccepted) throws RegistrationException {
		try {
			final int UF_ACCOUNTDISABLE = 0x0002;
			final int UF_PASSWD_NOTREQD = 0x0020;
			final int UF_PASSWD_CANT_CHANGE = 0x0040;
			final int UF_NORMAL_ACCOUNT = 0x0200;
			final int UF_DONT_EXPIRE_PASSWD = 0x10000;
			final int UF_PASSWORD_EXPIRED = 0x800000;

			if (USER_CONTEXT == null || USER_CONTEXT.length() < 1) {
				throw new RegistrationException(
						"Could not register LDAP account - User context unknown");
			}

			// Make sure no existing XNAT account for user. Would result in
			// unusable account for LDAP user
			if (isXdatUser(email)) {
				throw new RegistrationException(
						"Could not register LDAP account - An account already exists for this Username");
			}

			final String dn = "CN=" + username + "," + USER_CONTEXT;

			this.openContext(LDAP_USER, LDAP_PASS);

			final BasicAttributes attrs = new BasicAttributes(true);
			final Attribute objclass = new BasicAttribute("objectClass");
			for (String clazz : USER_CLASS) {
				objclass.add(clazz);
			}
			attrs.put(objclass);

			final Attribute cnA = new BasicAttribute("cn");
			cnA.add(username);
			attrs.put(cnA);

			final Attribute samA = new BasicAttribute("sAMAccountName");
			samA.add(username);
			attrs.put(samA);

			final Attribute instA = new BasicAttribute("department");
			instA.add(institution);
			attrs.put(instA);

			final Attribute emailA = new BasicAttribute("mail");
			emailA.add(email);
			attrs.put(emailA);

			final Attribute uidA = new BasicAttribute("uid");
			uidA.add(username);
			attrs.put(uidA);

			final Attribute gnA = new BasicAttribute("givenName");
			gnA.add(firstname);
			attrs.put(gnA);

			final Attribute snA = new BasicAttribute("sn");
			snA.add(lastname);
			attrs.put(snA);

			if (duAccepted.equalsIgnoreCase("true")
					|| duAccepted.equalsIgnoreCase("y")
					|| duAccepted.equalsIgnoreCase("yes")) {
				final Attribute commentA = new BasicAttribute("comment");
				commentA.add(getMsgFromMap("FTP") + " ("
						+ new SimpleDateFormat("yyyy-MM-dd").format(new Date())
						+ ")");
				attrs.put(commentA);
			}
			if (wbAccepted.equalsIgnoreCase("true")
					|| wbAccepted.equalsIgnoreCase("y")
					|| wbAccepted.equalsIgnoreCase("yes")) {
				final Attribute commentA = new BasicAttribute("comment");
				commentA.add(getMsgFromMap("WB") + " ("
						+ new SimpleDateFormat("yyyy-MM-dd").format(new Date())
						+ ")");
				attrs.put(commentA);
			}

			// Set account password
			try {
				attrs.put("unicodePwd", getActiveDirectoryEncodedPW(pw));
			} catch (UnsupportedEncodingException e) {
				throw new RegistrationException(
						"Registration couldn't be completed.  Could not set account password.  "
								+ "Verify that password complexity requirements have been met.",
						e);
			}

			// Enable account, set control settings
			attrs.put("userAccountControl",
					Integer.toString(UF_NORMAL_ACCOUNT + UF_DONT_EXPIRE_PASSWD));

			// Create user context
			ctx.createSubcontext(dn, attrs);

			// Set display name (fails when set above. Must be modified post
			// context account creation)
			try {
				ModificationItem mod[] = new ModificationItem[1];
				mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute("displayName", firstname + " "
								+ lastname));
				ctx.modifyAttributes(dn, mod);
			} catch (NamingException e) {
				logger.error(
						"Registration couldn't be completed.  Could not set display name",
						e);
				throw new RegistrationException(
						"Registration couldn't be completed.  Could not set display name",
						e);
			}

			// Add user to groups
			try {
				// Modification MRH 2012-09-06. Now registering ConnectomeDB
				// access without consenting to any
				// data use terms. Consent will be done from inside the DB to
				// gain access to projects.
				final ModificationItem mod[] = new ModificationItem[1];
				mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute("member", dn));
				final String[] USER_GROUPS = getGroupArrayFromMap("ConnectomeUsers");
				final String[] FTP_GROUPS = getGroupArrayFromMap("FtpUsers");
				final String[] WB_GROUPS = getGroupArrayFromMap("WB");
				for (final String group : USER_GROUPS) {
					ctx.modifyAttributes(group, mod);
				}
				if (duAccepted.equalsIgnoreCase("true")
						|| duAccepted.equalsIgnoreCase("y")
						|| duAccepted.equalsIgnoreCase("yes")) {
					for (String group : FTP_GROUPS) {
						ctx.modifyAttributes(group, mod);
					}
				} else if (wbAccepted.equalsIgnoreCase("true")
						|| wbAccepted.equalsIgnoreCase("y")
						|| wbAccepted.equalsIgnoreCase("yes")) {
					for (String group : WB_GROUPS) {
						ctx.modifyAttributes(group, mod);
					}
				}
			} catch (NamingException e) {
				logger.error(
						"Registration couldn't be completed.  Could not set groups", e);
				throw new RegistrationException(
						"Registration couldn't be completed.  Could not set groups", e);
			}

			return dn;

		} catch (NamingException e) {
			if (e instanceof NameAlreadyBoundException) {
				logger.error(
						"Could not complete registration - An account already exists for this Username ("
								+ username + ")", e);
				throw new RegistrationException(
						"Could not complete registration - An account already exists for this Username",
						e);
			}
			logger.error("Could not complete registration", e);
			throw new RegistrationException("Could not complete registration",
					e);
		} finally {
			this.closeContext();
		}

	}

	private String[] getGroupArrayFromMap(String string) {
		if (groupMap.containsKey(string.toUpperCase())) {
			final Map<AccessProp, String[]> map = groupMap.get(string.toUpperCase());
			if (map.containsKey(AccessProp.ldapGroup)) {
				if (map.get(AccessProp.ldapGroup) != null
						&& map.get(AccessProp.ldapGroup).length > 0) {
					return map.get(AccessProp.ldapGroup);
				}
			}
		}
		return new String[0];
	}

	private String getMsgFromMap(String string) {
		if (groupMap.containsKey(string)) {
			final Map<AccessProp, String[]> map = groupMap.get(string.toUpperCase());
			if (map.containsKey(AccessProp.dutMessage)) {
				if (map.get(AccessProp.dutMessage) != null
						&& map.get(AccessProp.dutMessage).length > 0) {
					return map.get(AccessProp.dutMessage)[0];
				}
			}
		}
		return "Accepted " + string + " DU terms";
	}

	private byte[] getActiveDirectoryEncodedPW(String inpw)
			throws UnsupportedEncodingException {
		final String quotedPW = "\"" + inpw + "\"";
		return quotedPW.getBytes("UTF-16LE");
	}

	private boolean isXdatUser(String login) {

		try {

			final SchemaElementI e = SchemaElement
					.GetElement(XDAT_USER_ELEMENT);
			final ItemSearch search = new ItemSearch(null,
					e.getGenericXFTElement());
			search.addCriteria(
					XDAT_USER_ELEMENT + XFT.PATH_SEPERATOR + "login", login);
			search.setLevel(ViewManager.ACTIVE);
			final ArrayList found = search.exec(true).items();

			if (found.size() > 0) {
				return true;
			}

			return false;

		} catch (Exception e) {
			// Assume no current xdat user for these purposes
			return false;
		}

	}

	public static boolean RegisterDataUseAcceptance(String username, String pw) throws HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		return ldap.registerDataUseAcceptance(username, pw);
	}

	private synchronized boolean registerDataUseAcceptance(String username, String pw) {

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", username);

		final String dn = "CN=" + username + "," + USER_CONTEXT;

		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { "comment" };
		searchCtls.setReturningAttributes(returnedAtts);

		try {
			// First open context with user permissions, to ensure
			// authentication
			try {
				this.openContext(dn, pw);
				this.closeContext();
			} catch (AuthenticationException e) {
				return false;
			} catch (Exception e) {
				return false;
			}

			// Then open with RW account permissions
			this.openContext(LDAP_USER, LDAP_PASS);
			final NamingEnumeration answer = ctx.search(SEARCHBASE,
					searchFilter, searchCtls);
			// Loop through the search results
			String currComment = "";
			if (!answer.hasMore()) {
				final ModificationItem mod[] = new ModificationItem[1];
				mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute("comment", getMsgFromMap("FTP")
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));
				ctx.modifyAttributes(dn, mod);
				try {
					mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							new BasicAttribute("member", dn));
					final String[] FTP_GROUPS = getGroupArrayFromMap("FTP");
					for (final String group : FTP_GROUPS) {
						ctx.modifyAttributes(group, mod);
					}
				} catch (Exception ie) {
					// Do nothing for now
				}
				return true;
			}
			while (answer.hasMoreElements()) {
				final SearchResult sr = (SearchResult) answer.next();
				final Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					for (NamingEnumeration ae = attrs.getAll(); ae.hasMore();) {
						final Attribute attr = (Attribute) ae.next();
						// System.out.println("Attribute: " + attr.getID());
						if (attr.getID().equalsIgnoreCase("comment")) {
							for (final NamingEnumeration e = attr.getAll(); e
									.hasMore();) {
								if (currComment == null
										|| currComment.length() < 1) {
									currComment = e.next().toString();
								} else {
									currComment = currComment + "; "
											+ e.next().toString();
								}
								if (currComment.indexOf(getMsgFromMap("FTP")) >= 0) {
									return true;
								}
							}
						}
					}
				}
			}
			final ModificationItem mod[] = new ModificationItem[1];
			if (currComment == null || currComment.trim().length() < 1) {
				mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("comment", getMsgFromMap("FTP")
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));
			} else {
				mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("comment", currComment
								+ "; "
								+ getMsgFromMap("FTP")
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));

			}
			ctx.modifyAttributes(dn, mod);
			try {
				mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute("member", dn));
				final String[] FTP_GROUPS = getGroupArrayFromMap("FTP");
				for (final String group : FTP_GROUPS) {
					ctx.modifyAttributes(group, mod);
				}
			} catch (Exception ie) {
				// Do nothing for now
			}
			return true;
		} catch (NamingException e) {
			logger.error("Could not register data use acceptance", e);
			return false;
		} finally {
			this.closeContext();
		}

	}

	public static void updateUserTiersMap(HttpSession session) {
		try {
			final HcpLdapHelper ldap = getInstance();
			XDATUser user = (XDATUser) session.getAttribute("user");
			session.setAttribute("userTiers", ldap.getUserTiers(user));
		} catch (Exception e) {
			logger.error("userTiers map could not be updated in the http session", e);
		}
	}

	public static boolean RegisterWBDataUseAcceptance(String username, String pw) throws HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		return ldap.registerWBDataUseAcceptance(username, pw);
	}

	private synchronized boolean registerWBDataUseAcceptance(String username, String pw) {

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", username);

		final String dn = "CN=" + username + "," + USER_CONTEXT;

		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { "comment" };
		searchCtls.setReturningAttributes(returnedAtts);

		try {
			// First open context with user permissions, to ensure
			// authentication
			try {
				this.openContext(dn, pw);
				this.closeContext();
			} catch (AuthenticationException e) {
				return false;
			} catch (Exception e) {
				return false;
			}

			// Then open with RW account permissions
			this.openContext(LDAP_USER, LDAP_PASS);
			final NamingEnumeration answer = ctx.search(SEARCHBASE,
					searchFilter, searchCtls);
			// Loop through the search results
			String currComment = "";
			if (!answer.hasMore()) {
				final ModificationItem mod[] = new ModificationItem[1];
				mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute("comment", getMsgFromMap("WB")
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));
				ctx.modifyAttributes(dn, mod);
				try {
					mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							new BasicAttribute("member", dn));
					final String[] WB_GROUPS = getGroupArrayFromMap("WB");
					for (final String group : WB_GROUPS) {
						ctx.modifyAttributes(group, mod);
					}
				} catch (Exception ie) {
					// Do nothing for now
				}
				return true;
			}
			while (answer.hasMoreElements()) {
				final SearchResult sr = (SearchResult) answer.next();
				final Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					for (final NamingEnumeration ae = attrs.getAll(); ae
							.hasMore();) {
						final Attribute attr = (Attribute) ae.next();
						// System.out.println("Attribute: " + attr.getID());
						if (attr.getID().equalsIgnoreCase("comment")) {
							for (final NamingEnumeration e = attr.getAll(); e
									.hasMore();) {
								if (currComment == null
										|| currComment.length() < 1) {
									currComment = e.next().toString();
								} else {
									currComment = currComment + "; "
											+ e.next().toString();
								}
								if (currComment.indexOf(getMsgFromMap("WB")) >= 0) {
									return true;
								}
							}
						}
					}
				}
			}
			final ModificationItem mod[] = new ModificationItem[1];
			if (currComment == null || currComment.trim().length() < 1) {
				mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("comment", getMsgFromMap("WB")
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));
			} else {
				mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("comment", currComment
								+ "; "
								+ getMsgFromMap("WB")
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));

			}
			ctx.modifyAttributes(dn, mod);
			try {
				mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute("member", dn));
				final String[] WB_GROUPS = getGroupArrayFromMap("WB");
				for (final String group : WB_GROUPS) {
					ctx.modifyAttributes(group, mod);
				}
			} catch (Exception ie) {
				// Do nothing for now
			}
			return true;
		} catch (NamingException e) {
			logger.error(e);
			return false;
		} finally {
			this.closeContext();
		}

	}

	// ///////////////////////////////////
	// DATA USE TERMS RESOURCE SUPPORT //
	// ///////////////////////////////////

	public static boolean RecordDataUseAcceptance(XDATUser user, String terms)
			throws DataUseException, HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		return ldap.recordDataUseAcceptance(user, terms);
	}

	private synchronized boolean recordDataUseAcceptance(XDATUser user, String terms)
			throws DataUseException {

		final String username = user.getLogin();
		String acceptString = null;
		String[] ldapGroup = {};
		String[] xnatGroupsArray = {};
		String[] removeArray = {};

		if (!groupMap.containsKey(terms.toUpperCase())) {
			throw new DataUseException("Invalid value for terms (" + terms + ")");
		}

		final Map<AccessProp, String[]> map = groupMap.get(terms.toUpperCase());
		for (final AccessProp ext : map.keySet()) {
			if (map.get(ext) == null || map.get(ext).length <= 0) {
				continue;
			}
			if (ext == AccessProp.dutMessage) {
				acceptString = map.get(ext)[0];
			} else if (ext == AccessProp.ldapGroup) {
				ldapGroup = map.get(ext);
			} else if (ext == AccessProp.removeGroups) {
				removeArray = map.get(ext);
			} else if (ext == AccessProp.xnatGroups) {
				xnatGroupsArray = map.get(ext);
			}
		}

		HcpUserDatabaseEntryUtils.addUserToXnatGroups(user, xnatGroupsArray);
		HcpUserDatabaseEntryUtils.removeUserFromXnatGroups(user, removeArray);

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", username);

		final String dn = "CN=" + username + "," + USER_CONTEXT;

		// ///////////////////////////
		// MODIFY GROUP MEMBERSHIP //
		// ///////////////////////////

		try {

			final SearchControls searchCtls = new SearchControls();
			final String returnedAtts2[] = { "memberOf" };
			searchCtls.setReturningAttributes(returnedAtts2);
			boolean alreadyMember = false;

			this.openContext(LDAP_USER, LDAP_PASS);
			logger.debug(ctx.getEnvironment().toString());
			logger.debug("SEARCHBASE:" + SEARCHBASE);
			logger.debug("FILTER:" + searchFilter);
			logger.debug("searchCtls:" + searchCtls.toString());
			// Search for objects using the filter
			NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter,
					searchCtls);

			// Loop through the search results
			while (answer.hasMoreElements()) {
				final SearchResult sr = (SearchResult) answer.next();
				final Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (final NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							if (attr.getID().equalsIgnoreCase("memberOf")) {
								for (final NamingEnumeration e = attr.getAll(); e
										.hasMore();) {
									final String member = e.next().toString();
									for (final String group : ldapGroup) {
										if (member.equalsIgnoreCase(group)) {
											alreadyMember = true;
										}
									}
								}
							}
						}
					} catch (NamingException e) {
						logger.error(LDAP_USER + ":Error retrieving DN for "
								+ LDAP_USER + " from results", e);
					}
				}
			}

			if (!alreadyMember) {
				try {
					final ModificationItem mod[] = new ModificationItem[1];
					mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							new BasicAttribute("member", dn));
					for (final String group : ldapGroup) {
						ctx.modifyAttributes(group, mod);
					}
				} catch (Exception ie) {
					// Do nothing for now
				}
			}

			// //////////////////
			// MODIFY COMMENT //
			// //////////////////

			// Create the search controls
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			final String returnedAtts[] = { "comment" };
			searchCtls.setReturningAttributes(returnedAtts);

			// Then open with RW account permissions
			this.openContext(LDAP_USER, LDAP_PASS);
			answer = ctx.search(SEARCHBASE, searchFilter, searchCtls);
			// Loop through the search results
			String currComment = "";
			if (!answer.hasMore()) {
				final ModificationItem mod[] = new ModificationItem[1];
				mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute("comment", acceptString
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));
				ctx.modifyAttributes(dn, mod);
				try {
					mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							new BasicAttribute("member", dn));
					for (String group : ldapGroup) {
						ctx.modifyAttributes(group, mod);
					}
				} catch (Exception ie) {
					// Do nothing for now
				}
			}
			while (answer.hasMoreElements()) {
				final SearchResult sr = (SearchResult) answer.next();
				final Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					for (final NamingEnumeration ae = attrs.getAll(); ae
							.hasMore();) {
						final Attribute attr = (Attribute) ae.next();
						// System.out.println("Attribute: " + attr.getID());
						if (attr.getID().equalsIgnoreCase("comment")) {
							for (final NamingEnumeration e = attr.getAll(); e
									.hasMore();) {
								if (currComment == null
										|| currComment.length() < 1) {
									currComment = e.next().toString();
								} else {
									currComment = currComment + "; "
											+ e.next().toString();
								}
							}
						}
					}
				}
			}
			currComment = currComment.replaceAll("[; ]*" + acceptString
					+ "[^)]*[)]", "");
			if (currComment.startsWith("; ")) {
				currComment = currComment.substring(2);
			}
			final ModificationItem mod[] = new ModificationItem[1];
			if (currComment == null || currComment.trim().length() < 1) {
				mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("comment", acceptString
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));
			} else {
				mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("comment", currComment
								+ "; "
								+ acceptString
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));

			}
			ctx.modifyAttributes(dn, mod);
			return true;

		} catch (NamingException e) {
			logger.error(e);
			return false;
		} finally {
			this.closeContext();
		}

	}

	public static boolean RemoveDataUseAcceptance(XDATUser user, String terms)
			throws DataUseException, HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		return ldap.removeDataUseAcceptance(user, terms);
	}

	private synchronized boolean removeDataUseAcceptance(XDATUser user, String terms)
			throws DataUseException {

		final String username = ((XDATUserDetails) user).getAuthorization().getAuthUser();
		// String username = user.getLogin();
		String acceptString = null;
		String[] ldapGroupArray = {};
		String[] removeArray = {};
		if (!groupMap.containsKey(terms.toUpperCase())) {
			throw new DataUseException("Invalid value for terms (" + terms
					+ ")");
		}
		final Map<AccessProp, String[]> map = groupMap.get(terms.toUpperCase());
		for (AccessProp ext : map.keySet()) {
			if (map.get(ext) == null || map.get(ext).length <= 0) {
				continue;
			}
			if (ext == AccessProp.dutMessage) {
				acceptString = map.get(ext)[0];
			} else if (ext == AccessProp.removeGroups) {
				removeArray = map.get(ext);
			} else if (ext == AccessProp.ldapGroup) {
				ldapGroupArray = map.get(ext);
			}
		}

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", username);

		final String dn = "CN=" + username + "," + USER_CONTEXT;

		// TODO Not removing from correct XNAT group
		HcpUserDatabaseEntryUtils.removeUserFromXnatGroups(user, removeArray);

		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { "comment" };
		searchCtls.setReturningAttributes(returnedAtts);

		try {
			// Then open with RW account permissions
			this.openContext(LDAP_USER, LDAP_PASS);
			final NamingEnumeration answer = ctx.search(SEARCHBASE,
					searchFilter, searchCtls);
			// Loop through the search results
			String currComment = "";
			while (answer.hasMoreElements()) {
				final SearchResult sr = (SearchResult) answer.next();
				final Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (final NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							final Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							if (attr.getID().equalsIgnoreCase("comment")) {
								for (NamingEnumeration e = attr.getAll(); e
										.hasMore();) {
									if (currComment == null
											|| currComment.length() < 1) {
										currComment = e.next().toString();
									} else {
										currComment = currComment + "; "
												+ e.next().toString();
									}
								}
							}
						}
					} catch (NamingException e) {
						return false;
					}
				}
			}
			try {
				final ModificationItem mod[] = new ModificationItem[1];
				currComment = currComment.replaceAll("[; ]*" + acceptString
						+ "[^)]*[)]", "");
				if (currComment.length() < 1) {
					mod[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE,
							new BasicAttribute("comment"));
					// new BasicAttribute("comment", dn));
				} else {
					mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
							new BasicAttribute("comment", currComment));
				}
				ctx.modifyAttributes(dn, mod);
				try {
					mod[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE,
							new BasicAttribute("member", dn));
					for (String group : ldapGroupArray) {
						ctx.modifyAttributes(group, mod);
					}
				} catch (Exception ie) {
					// Do nothing for now
				}
				return true;
			} catch (NamingException e) {
				return false;
			}
		} catch (NamingException e) {
			return false;
		} finally {
			this.closeContext();
		}

	}

	public static String GroupsByUsername(String user, String group) throws HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		return ldap.groupsByUsername(user, group);
	}

	public static String GroupsByUsername(String user) throws DataUseException, HcpLdapException {
		return GroupsByUsername(user, null);
	}

	private String groupsByUsername(String user, String group) {

		// private static String SEARCH_TEMPLATE =
		// "(&(objectClass=user)(CN=%USER%))";

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", user);

		// Create the search controls
		return getGroups(searchFilter, group);

	}

	public static Map<String, Object> AttrsByUser(String user)
			throws DataUseException, HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		return ldap.attrsByUser(user);
	}

	private Map<String, Object> attrsByUser(String user) {

		// private static String SEARCH_TEMPLATE =
		// "(&(objectClass=user)(CN=%USER%))";
		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", user);
		return getAttrs(searchFilter,
				new String[] { "mail", "givenName", "sn" });

	}

	public static Map<String, Object> AttrsByEmail(String mail)
			throws DataUseException, HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		return ldap.attrsByEmail(mail);
	}

	private Map<String, Object> attrsByEmail(String mail) {

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"CN=%USER%", "mail=" + mail);
		return getAttrs(searchFilter, new String[] { "cn" });

	}

	public static String GroupsByEmail(String mail, String group)
			throws DataUseException, HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		return ldap.groupsByEmail(mail, group);
	}

	public static String GroupsByEmail(String mail) throws DataUseException, HcpLdapException {
		return GroupsByEmail(mail, null);
	}

	private String groupsByEmail(String mail, String group) {

		// private static String SEARCH_TEMPLATE =
		// "(&(objectClass=user)(CN=%USER%))";

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"CN=%USER%", "mail=" + mail);

		// Create the search controls
		return getGroups(searchFilter, group);

	}

	private Map<String, Object> getAttrs(String searchFilter, String[] attribs) {

		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = attribs;
		searchCtls.setReturningAttributes(returnedAtts);
		final Map<String, Object> returnMap = new HashMap<String, Object>();

		try {
			this.openContext(LDAP_USER, LDAP_PASS);
			logger.debug(ctx.getEnvironment().toString());
			logger.debug("SEARCHBASE:" + SEARCHBASE);
			logger.debug("FILTER:" + searchFilter);
			logger.debug("searchCtls:" + searchCtls.toString());
			// Search for objects using the filter
			final NamingEnumeration answer = ctx.search(SEARCHBASE,
					searchFilter, searchCtls);

			if (!answer.hasMoreElements()) {
				// Will return not found
				return null;
			}

			// Loop through the search results
			while (answer.hasMoreElements()) {
				final SearchResult sr = (SearchResult) answer.next();

				final Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (final NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							final Attribute attr = (Attribute) ae.next();
							returnMap.put(attr.getID(), attr.get());
						}
					} catch (NamingException e) {
						return returnMap;
					}
				}
			}
		} catch (NamingException e) {
			return returnMap;
		} finally {
			this.closeContext();
		}
		return returnMap;

	}

	private String getGroups(String searchFilter, String group) {

		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { "memberOf" };
		searchCtls.setReturningAttributes(returnedAtts);
		final StringBuilder rtn = new StringBuilder();

		try {
			this.openContext(LDAP_USER, LDAP_PASS);
			logger.debug(ctx.getEnvironment().toString());
			logger.debug("SEARCHBASE:" + SEARCHBASE);
			logger.debug("FILTER:" + searchFilter);
			logger.debug("searchCtls:" + searchCtls.toString());
			// Search for objects using the filter
			final NamingEnumeration answer = ctx.search(SEARCHBASE,
					searchFilter, searchCtls);

			if (!answer.hasMoreElements()) {
				// Will return not found
				return null;
			}

			// Loop through the search results
			while (answer.hasMoreElements()) {
				final SearchResult sr = (SearchResult) answer.next();
				final Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (final NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							final Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							if (attr.getID().equalsIgnoreCase("memberOf")) {
								for (NamingEnumeration e = attr.getAll(); e
										.hasMore();) {
									final String member = e.next().toString();
									if (rtn.length() <= 0) {
										rtn.append(member);
									} else {
										rtn.append(",").append(member);
									}
								}
							}
						}
					} catch (NamingException e) {
						return null;
					}
				}
			}
		} catch (NamingException e) {
			return null;
		} finally {
			this.closeContext();
		}

		final String[] rtnArr = rtn.toString().split(",");
		final Iterator<String> it = Iterators.forArray(rtnArr);
		rtn.delete(0, rtn.length());
		while (it.hasNext()) {
			final String part = it.next();
			if (part.startsWith("CN=")) {
				if (rtn.length() > 0) {
					rtn.append(',');
				}
				if (group != null && part != null) {
					if (group.equals(part.substring(3))) {
						return "true";
					}
				}
				rtn.append(part.substring(3));
			}
		}
		if (group != null) {
			return "false";
		}
		return rtn.toString();

	}

    public synchronized DUTStatus updateAllGroupPermissions(XDATUser user) throws DataUseException {
        DUTStatus status = DUTStatus.FALSE;
        for(String terms : groupNames){
            DUTStatus currStat;
            currStat = acceptedDataUse(user, terms);
            if(currStat==DUTStatus.MOD){
                status = DUTStatus.MOD;
            }
        }
        return status;
    }

	public static DUTStatus AcceptedDataUse(XDATUser user, String terms)
			throws DataUseException, HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		return ldap.acceptedDataUse(user, terms);
	}

	private synchronized DUTStatus acceptedDataUse(XDATUser user, String terms)
			throws DataUseException {

		// TODO: Can we user user.getLogin() here? Need to account for instances
		// where LDAP user matches database user.
		final String username = user.getLogin();
//		final String username = ((XDATUserDetails) user).getAuthorization().getAuthUser();

		String[] ldapGroupArray = {};
		String[] xnatGroupsArray = {};
		String[] removeArray = {};

		if (!groupMap.containsKey(terms.toUpperCase())) {
			throw new DataUseException("Invalid value for terms (" + terms + ")");
		}
		final Map<AccessProp, String[]> map = groupMap.get(terms.toUpperCase());
		for (AccessProp ext : map.keySet()) {
			if (map.get(ext) == null || map.get(ext).length <= 0) {
				continue;
			}

			if (ext == AccessProp.removeGroups) {
				removeArray = map.get(ext);
			} else if (ext == AccessProp.ldapGroup) {
				ldapGroupArray = map.get(ext);
			} else if (ext == AccessProp.xnatGroups) {
				xnatGroupsArray = map.get(ext);
			}
		}

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE, "%USER%", username);

		final String dn = "CN=" + username + "," + USER_CONTEXT;

		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { "memberOf" };
		searchCtls.setReturningAttributes(returnedAtts);
		boolean acceptedDataUse = false;

		try {
			this.openContext(LDAP_USER, LDAP_PASS);
			logger.debug(ctx.getEnvironment().toString());
			logger.debug("SEARCHBASE:" + SEARCHBASE);
			logger.debug("FILTER:" + searchFilter);
			logger.debug("searchCtls:" + searchCtls.toString());
			// Search for objects using the filter
			final NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter, searchCtls);

			// Loop through the search results
			while (answer.hasMoreElements()) {
				final SearchResult sr = (SearchResult) answer.next();

				final Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (final NamingEnumeration ae = attrs.getAll(); ae.hasMore();) {
							final Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							if (attr.getID().equalsIgnoreCase("memberOf")) {
								for (final NamingEnumeration e = attr.getAll(); e.hasMore();) {
									final String member = e.next().toString();
									for (final String group : ldapGroupArray) {
										if (member.equalsIgnoreCase(group)) {
											acceptedDataUse = true;
										}
									}
								}
							}
						}
					} catch (NamingException e) {
						logger.error(LDAP_USER + ":Error retrieving DN for " + LDAP_USER + " from results", e);
					}
				}
			}
		} catch (NamingException e) {
			logger.error(LDAP_USER + ":Unable to authenticate " + LDAP_USER + " with given password using DN: " + dn, e);
		} finally {
			this.closeContext();
		}

		if (acceptedDataUse) {
			try {
				boolean groupAdded = HcpUserDatabaseEntryUtils.addUserToXnatGroups(user, xnatGroupsArray);
				boolean groupRemoved = HcpUserDatabaseEntryUtils.removeUserFromXnatGroups(user, removeArray);
				// Check for any modifications, otherwise return TRUE
				DUTStatus status =  groupAdded | groupRemoved ? DUTStatus.MOD : DUTStatus.TRUE;
				return status;
			} catch (Exception e) {
				logger.error(e);
				return DUTStatus.TRUE;
			}
		} else {
			try {
				boolean groupRemoved = HcpUserDatabaseEntryUtils.removeUserFromXnatGroups(user, removeArray);
				DUTStatus status = groupRemoved ? DUTStatus.MOD : DUTStatus.FALSE;
				return status;
			} catch (Exception e) {
				logger.error(e);
				return DUTStatus.FALSE;
			}
		}
	}

	// ///////////////////////////////////////
	// END DATA USE TERMS RESOURCE SUPPORT //
	// ///////////////////////////////////////

	public static void UpdateLdapAccount(RunData data,
			Hashtable<String, String> props) throws AccountUpdateException, HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		if (props.containsKey("login")) {
			ldap.updateLdapAccount(XDATUser.getXdatUsersByLogin(
					props.get("login"), TurbineUtils.getUser(data), false), props);
		}
//		updateUserTiersMap(data, ldap);
	};

	private synchronized void updateLdapAccount(XdatUser user,
			Hashtable<String, String> props) throws AccountUpdateException {
		try {
			final Set<Map.Entry<String, String>> entryset = props.entrySet();

			String firstname = null;
			String lastname = null;
			for (final Map.Entry<String, String> entry : entryset) {

				if (entry.getKey().equals("primary_password")) {

					modifyLdapAttribute(getDN(user.getLogin()), "unicodePwd",
							getActiveDirectoryEncodedPW(entry.getValue()));

				} else if (entry.getKey().equals("email")) {

					modifyLdapAttribute(user, "mail", entry.getValue());

				} else if (entry.getKey().equals("firstname")) {

					firstname = entry.getValue();
					modifyLdapAttribute(user, "givenName", entry.getValue());

				} else if (entry.getKey().equals("lastname")) {

					lastname = entry.getValue();
					modifyLdapAttribute(user, "sn", entry.getValue());

				}

			}
			if (firstname != null && lastname != null) {
				modifyLdapAttribute(user, "displayName", firstname + " " + lastname);
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException("Could not update LDAP account", e);
		}

	}

	public static void UpdateLdapAccountPW(RunData data, String newPW)
			throws AccountUpdateException, HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		ldap.updateLdapAccountPW(TurbineUtils.getUser(data), newPW);
	}

	private synchronized void updateLdapAccountPW(XDATUser user, String newPW)
			throws AccountUpdateException {
		// // These checks don't work as of XNAT 1.6
		// if (!(user.isEnabled() && user.isLoggedIn())) {
		// throw new AccountUpdateException(
		// "Couldn't update LDAP account.  User not logged in.");
		// }
		try {
			modifyLdapAttribute(getDN(user.getLogin()), "unicodePwd", getActiveDirectoryEncodedPW(newPW));
		} catch (UnsupportedEncodingException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException("Couldn't update LDAP account.  Could not modify attribute.");
		}
	}

	public static String AccountPasswordReset(String username)
			throws AccountUpdateException, HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		return ldap.accountPasswordReset(username);
	}

	private synchronized String accountPasswordReset(String username)
			throws AccountUpdateException {
		try {
			// LDAP requires mixed-case passwords, XFT function returns only
			// lower-case character values.
			final String newPW = Character.toUpperCase(XFT
					.CreateRandomCharacter(new Random()))
					+ XFT.CreateRandomAlphaNumeric(9);
			modifyLdapAttribute(getDN(username), "unicodePwd",
					getActiveDirectoryEncodedPW(newPW));
			return newPW;
		} catch (UnsupportedEncodingException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException("Could not update LDAP account", e);
		} catch (AccountUpdateException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException(
					"Could not update LDAP account.  Verify complexity requirements met.",
					e);
		}
	}

	public static void UpdateLdapAccountEmail(RunData data, String newEmail)
			throws AccountUpdateException, HcpLdapException {
		final HcpLdapHelper ldap = getInstance();
		ldap.updateLdapAccountEmail(TurbineUtils.getUser(data), newEmail);
	}

	private synchronized void updateLdapAccountEmail(XDATUser user, String newEmail)
			throws AccountUpdateException {
		// These checks don't work as of XNAT 1.6
		// if (!(user.isEnabled() && user.isLoggedIn())) {
		// throw new AccountUpdateException(
		// "Couldn't update LDAP account.  User not logged in.");
		// }
		modifyLdapAttribute(user, "mail", newEmail);
	}

	private void modifyLdapAttribute(XdatUser user, String attr, Object value)
			throws AccountUpdateException {

		final String authUser = ((XDATUserDetails) user).getAuthorization().getAuthUser();
		final String dn = getDN(authUser);
		modifyLdapAttribute(dn, attr, value);

	}

	private synchronized void modifyLdapAttribute(String dn, String attr, Object value)
			throws AccountUpdateException {
		try {
			this.openContext(LDAP_USER, LDAP_PASS);
			// Search for objects using the filter
			try {
				final ModificationItem mod[] = new ModificationItem[1];
				mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(attr, value));
				ctx.modifyAttributes(dn, mod);
			} catch (NamingException e) {
				logger.error("Couldn't update LDAP account.  Could not access modify attribute.");
				throw new AccountUpdateException("Couldn't update LDAP account.  Could not modify attribute.");
			}

		} catch (NamingException e) {
			logger.error("Couldn't update LDAP account.  Could not access LDAP account.");
			throw new AccountUpdateException(
					"Couldn't update LDAP account.  Could not access LDAP account.");
		} finally {
			this.closeContext();
		}
	}

	public static class DataUseException extends Exception {
		private static final long serialVersionUID = 5255441123074983022L;

		public DataUseException() {
			super("You must agree to the Data Use Terms.");
		}

		public DataUseException(String message) {
			super(message);
		}

		public DataUseException(String message, Exception e) {
			super(message, e);
		}
	};

	public static class AccountUpdateException extends Exception {
		private static final long serialVersionUID = 5513045836496734644L;

		public AccountUpdateException(String message) {
			super(message);
		}

		public AccountUpdateException(String message, Exception e) {
			super(message, e);
		}
	};

	public static class HcpLdapException extends Exception {
		private static final long serialVersionUID = 2497202898288855020L;

		public HcpLdapException(String message) {
			super(message);
		}

		public HcpLdapException(String message, Exception e) {
			super(message, e);
		}
	}

	public static class RegistrationException extends Exception {
		private static final long serialVersionUID = 2497202898288855021L;

		public RegistrationException(String message) {
			super(message);
		}

		public RegistrationException(String message, Exception e) {
			super(message, e);
		}
	}

	public static class Credentials {
		String username = null;
		String password = null;

		public Credentials(String u, String p) {
			username = u;
			password = p;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

	}

}

