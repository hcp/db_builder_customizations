// Copyright 2012 Washington University School of Medicine All Rights Reserved
package org.nrg.hcp.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.nrg.hcp.security.HcpLdapHelper;
/**
 * @author MRH
 */
@SuppressWarnings("serial")
public class HCPDataUseTermsServlet extends HttpServlet {
	
	static org.apache.log4j.Logger logger = Logger
			.getLogger(HCPDataUseTermsServlet.class);
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			// Pull parameters and verify input
		        final String username = assignValue(request.getParameter("doagree-user"));
		        final String pw = assignValue(request.getParameter("doagree-pw"));
		        final String agree = assignValue(request.getParameter("datause-agree"));
		        final String wbAgree = assignValue(request.getParameter("wb-datause-agree"));
		        // Validate input
			if (agree!=null && agree.equalsIgnoreCase("true")) {
		       	if (HcpLdapHelper.RegisterDataUseAcceptance(username,pw)) {
        				response.setStatus(HttpServletResponse.SC_OK);
		       			return;
				} else {
		       			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		       			response.getWriter().print("Could not register Data Use Terms acceptance");
		       			return;
				}
			}
			if (wbAgree!=null && wbAgree.equalsIgnoreCase("true")) {
		       	if (HcpLdapHelper.RegisterWBDataUseAcceptance(username,pw)) {
        				response.setStatus(HttpServletResponse.SC_OK);
		       			return;
				} else {
		       			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		       			response.getWriter().print("Could not register Data Use Terms acceptance");
		       			return;
				}
			}
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    response.getWriter().print("Could not register Data Use Terms acceptance");

		} catch (Exception e) {
		       	response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		       	try {
					response.getWriter().print("Could not register Data Use Terms acceptance");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
		}
		
	}

	private String assignValue(String parameter) {
	       if (parameter!=null) {
	       	return parameter.trim();
	       } else {
	       	return null;
	       }
	}

}
