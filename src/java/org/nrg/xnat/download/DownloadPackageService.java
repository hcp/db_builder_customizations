/**
 * Copyright (c) 2013 Washington University School of Medicine
 */
package org.nrg.xnat.download;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XdatStoredSearch;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.XFTTableI;
import org.nrg.xnat.entities.PackageInfo;
import org.nrg.xnat.services.PackageFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
@Controller
@RequestMapping("/download")
public final class DownloadPackageService {
    public static final String DOWNLOAD_LOGGER_NAME = "org.nrg.xnat.download.monitor";
    public static final String ASPERA_TOKEN_VIEW = "application/x-aspera-download-token";
    public static final String SUBJECT_ROOTED_SEARCH = "xnat:subjectData";

    public static final String MODEL_FILTERS = "filters";
    public static final String MODEL_PATHS = "paths";
    public static final String MODEL_FAILURES = "failures";

    private final Logger downloadLogger = LoggerFactory.getLogger(DOWNLOAD_LOGGER_NAME);
    private final Logger logger = LoggerFactory.getLogger(DownloadPackageService.class);

    @Inject private URL asperaNodeURL;
    @Inject private String nodeUser;
    @Inject private String nodePassword;
    @Inject private PackageBuilderProvider builderProvider;
    @Inject private String tokenEncryptionKey;
    @Inject private Integer target_rate_kbps;
    @Inject private FilterMap session_types_map;
    @Inject private FilterMap processing_level_map;
    @Inject private FilterMap package_types_map;
    @Inject private FilterMap modalities_map;

    // TODO: better Exception management

    private static class NotSubjectRootedSearchException extends Exception {
        private static final long serialVersionUID = -7849280718097468847L;

        public NotSubjectRootedSearchException(final String searchID, final String rootName) {
            super("requires search root " + SUBJECT_ROOTED_SEARCH + ", " + searchID + " has " + rootName);
        }        
    }

    private static class SearchFailedException extends Exception {
        private static final long serialVersionUID = -2652274238849961402L;

        public SearchFailedException(final String id, final Throwable cause) {
            super("search " + id + " failed", cause);
        }
    }

    private static class NoPackagesFoundException extends Exception {
        private static final long serialVersionUID = 8917895048621323060L;

        public NoPackagesFoundException(String message) {
            super(message);
        }
    }

    @ExceptionHandler(NotSubjectRootedSearchException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="search root must be " + SUBJECT_ROOTED_SEARCH)
    public void handleNotSubjectRootedSearchException(NotSubjectRootedSearchException e,
            HttpServletResponse response) {
        logger.error("unable to build packages for search", e);
    }

    @ExceptionHandler(SearchFailedException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason="search failed")
    public void handleSearchFailedException(SearchFailedException e, HttpServletResponse response) {
        logger.error("search failed", e.getCause());
    }

    @ExceptionHandler(NoPackagesFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public void handleNoPackagesFoundException(NoPackagesFoundException e, HttpServletResponse response) {
        logger.error("no matching packages found", e);
    }

    @ExceptionHandler(MalformedURLException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason="error accessing internal service")
    public void handleMalformedURLException(MalformedURLException e, HttpServletResponse response) {
        logger.error("package request failed", e);
    }

    private static void getSearchSubjects(final String searchID, final XDATUser user, final Collection<String> subjects)
            throws NotSubjectRootedSearchException, SearchFailedException {
        final XdatStoredSearch stored = XdatStoredSearch.getXdatStoredSearchsById(searchID, user, true);
        final String rootName = stored.getRootElementName();
        if (SUBJECT_ROOTED_SEARCH.equals(rootName)) {
            try {
                final XFTTableI table = stored.getDisplaySearch(user).execute(user.getLogin());
                if (null != table) {
                    table.resetRowCursor();
                    while (table.hasMoreRows()) {
                        final Map<?,?> row = table.nextRowHash();
                        subjects.add(row.get("subject_label").toString());
                    }
                }
            } catch (Throwable t) {
                throw new SearchFailedException(searchID, t);
            }
        } else {
            throw new NotSubjectRootedSearchException(searchID, rootName);
        }
    }

    private JSONObject buildPackagesSummary(final String project, final XDATUser user, final Collection<String> subjects)
            throws JSONException {
    	final int nallsubjs = subjects.size();
        final JSONObject root = new JSONObject();
        final JSONArray filters = new JSONArray();
        filters.put(session_types_map.getFilter(project).toJSON());
        filters.put(processing_level_map.getFilter(project).toJSON());
        filters.put(package_types_map.getFilter(project).toJSON());
        filters.put(modalities_map.getFilter(project).toJSON());
        root.put("filters", filters);
        final String packageRoot = ((HCPPackageBuilderProvider)builderProvider).getPackageRootForProject(project).toPath().toString();
        final String packageSubdir = ((HCPPackageBuilderProvider)builderProvider).getPackageSubdirForProject(project);
        final String packagePath = packageRoot + ((packageSubdir.startsWith(File.separator)) ? "" : File.separator) + packageSubdir; 
        final Map<String,PackageInfo> infoMap = (nallsubjs>0) ? builderProvider.getPackageInfo(Arrays.asList(new String[] { packagePath }),subjects) : null;
        final JSONArray packages = new JSONArray();
        for (final String id : builderProvider.getPackageNames(project)) {
            final JSONObject type = new JSONObject();
            type.put("id", id);
            type.put("label", builderProvider.getPackageLabel(project, id));
            type.put("description", builderProvider.getPackageDescription(project, id));
            final Set<String> keywords = builderProvider.getPackageKeywords(project, id);
            if (!keywords.isEmpty()) {
                type.put("keywords", keywords);
            }
            if (nallsubjs > 0) {
                type.put("subjects_total", nallsubjs);
                final PackageInfo pinfo = infoMap.get(id);
                if (pinfo != null) {
                	type.put("size", pinfo.getFileSize());
                	type.put("count", pinfo.getFileCount());
                	type.put("subjects_ok",  pinfo.getSubjectCount());
                } else {
                	type.put("size", 0);
                	type.put("count", 0);
                	type.put("subjects_ok", 0);
                	
                }
            }
            packages.put(type);
        }

        root.put("packages", packages);
        root.put("project", project);
        return root;
    }

    private JSONObject buildSubjectsSummary(final String project, final XDATUser user,
            final Collection<String> subjects, final String pkgIds)
                    throws JSONException {
        JSONArray subjsa = new JSONArray();
        List<String> pkgIdL = Arrays.asList(pkgIds.split(","));
        final String packageRoot = ((HCPPackageBuilderProvider)builderProvider).getPackageRootForProject(project).toPath().toString();
        final String packageSubdir = ((HCPPackageBuilderProvider)builderProvider).getPackageSubdirForProject(project);
        final String packagePath = packageRoot + ((packageSubdir.startsWith(File.separator)) ? "" : File.separator) + packageSubdir; 
        Long queryCount = builderProvider.getCountByPackageIds(Arrays.asList(new String[] { packagePath }),subjects,pkgIdL);
        long checkCount = 0;
        for (int i=0;i<2;i++) {
        	for (final String subject : subjects) {
	            final JSONObject subjo = new JSONObject();
	            subjo.put("id", subject);
	            int count = 0;
	            long size = 0;
	            boolean ok = true;
	            for (final String packageName : pkgIdL) {
	                final PackageBuilder builder = builderProvider.apply(project, packageName);
	                try {
	                    final Package pkg = builder.apply(project, user, subject);
	                    if (pkg.getFileCount()>0 || pkg.getSize()>0) {
	                    	checkCount++;
	                    }
	                    count += pkg.getFileCount();
	                    size += pkg.getSize();
	                    ok |= !pkg.getFailures().isEmpty();
	                } catch (AccessDeniedException ignore) {}
	            }
	            subjo.put("file_count", count);
	            subjo.put("size", size);
	            subjo.put("status", ok ? (count > 0 ? "OK" : "Data unavailable") : "ERROR");
	            subjsa.put(subjo);
	        }
        	if (checkCount==queryCount) {
        		break;
        	} else if (i==0 && checkCount<queryCount) {
        		subjsa = new JSONArray();
        		checkCount=0;
        		try {
        			XDAT.getContextService().getBean(PackageFileService.class).refreshCache();
        			//((HCPPackageBuilderProvider)builderProvider).clearCache();
        			((HCPPackageBuilderProvider)builderProvider).removeNullCacheValues();
        		} catch (ConcurrentModificationException e) {
        			// Do nothing for now, let other process operate on cache.
        		}
        	} else if (i==0 && checkCount!=queryCount) {
        		subjsa = new JSONArray();
        		checkCount=0;
        		try {
        			XDAT.getContextService().getBean(PackageFileService.class).refreshCache();
        			((HCPPackageBuilderProvider)builderProvider).clearCache();
        		} catch (ConcurrentModificationException e) {
        			// Do nothing for now, let other process operate on cache.
        		}
        	}
        }
        final JSONObject root = new JSONObject();
        root.put("subjects", subjsa);
        root.put("project", project);
        return root;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String testGet() throws JSONException {
        return buildPackagesSummary(StringUtils.trimToEmpty(XDAT.getConfigService().getConfigContents("dashboard", "project")), XDAT.getUserDetails(), Collections.<String>emptyList()).toString();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Object handlePost(final Map<String,Object> model,
            @RequestParam(value="project", required=false) final String project,
            @RequestParam(value="package", required=false) final String packageNames,
            @RequestParam(value="searchID", required=false) final String searchID,
            @RequestParam(value="subjects", required=false) final String csvSubjects,
            @RequestParam(value="destination", required=false) final String destinationPath,
            @RequestParam(value="view", required=false) final String view)
                    throws NoPackagesFoundException,
                    NotSubjectRootedSearchException,
                    SearchFailedException,
                    MalformedURLException,
                    JSONException {
        final XDATUser user = XDAT.getUserDetails();

        logger.trace("Collecting subjects for search {}", searchID);

        final Set<String> subjects = Sets.newLinkedHashSet();
        if (!Strings.isNullOrEmpty(searchID)) {
            getSearchSubjects(searchID, user, subjects);
        }
        if (!Strings.isNullOrEmpty(csvSubjects)) {
            subjects.addAll(Arrays.asList(csvSubjects.split(",")));
        }
        if (subjects.isEmpty()) {
            throw new NoPackagesFoundException("search " + searchID + " found no subjects");
        }

        // packages view doesn't require us to specify a package
        if ("packages".equalsIgnoreCase(view)) {
            model.put("json", buildPackagesSummary(project, user, subjects));
            return new ModelAndView(new JSONOrgView(), model);
        } else if (Strings.isNullOrEmpty(packageNames)) {
            throw new NoPackagesFoundException("no package specified");
        }

        long size = 0;
        logger.debug("Collecting packages {} for subjects {}", packageNames, subjects);
        final List<Package> packages = Lists.newArrayList();
        final Map<URI,Object> failures = Maps.newLinkedHashMap();
        List<String> pkgIdL = Arrays.asList(packageNames.split(","));
        final String packageRoot = ((HCPPackageBuilderProvider)builderProvider).getPackageRootForProject(project).toPath().toString();
        final String packageSubdir = ((HCPPackageBuilderProvider)builderProvider).getPackageSubdirForProject(project);
        final String packagePath = packageRoot + ((packageSubdir.startsWith(File.separator)) ? "" : File.separator) + packageSubdir; 
        Long queryCount = builderProvider.getCountByPackageIds(Arrays.asList(new String[] { packagePath }),subjects,pkgIdL);
        long checkCount=0;
        for (int i=0;i<2;i++) {
        	for (final String packageName : packageNames.split(",")) {
	            final PackageBuilder builder = builderProvider.apply(project, packageName);
	            for (final String subject : subjects) {
	                try {
	                    final Package pkg = builder.apply(project, user, subject);
	                    if (pkg.getFileCount()>0 || pkg.getSize()>0) {
	                    	checkCount++;
	                    }
	                    size += pkg.getSize();
	                    if (pkg.getPathIterator().hasNext()) {
	                        packages.add(pkg);
	                    }
	                    failures.putAll(pkg.getFailures());
	                } catch (AccessDeniedException ignore) {}
	            }
	        }
        	if (checkCount==queryCount) {
        		break;
        	} else if (i==0 && checkCount<queryCount) {
        		packages.clear();
        		checkCount=0;
        		try {
        			XDAT.getContextService().getBean(PackageFileService.class).refreshCache();
        			((HCPPackageBuilderProvider)builderProvider).removeNullCacheValues();
        		} catch (ConcurrentModificationException e) {
        			// Do nothing for now, let other process operate on cache.
        		}
        	} else if (i==0 && checkCount!=queryCount) {
        		packages.clear();
        		checkCount=0;
        		try {
        			XDAT.getContextService().getBean(PackageFileService.class).refreshCache();
        			((HCPPackageBuilderProvider)builderProvider).clearCache();
        		} catch (ConcurrentModificationException e) {
        			// Do nothing for now, let other process operate on cache.
        		}
        	}
        }

        model.put(MODEL_PATHS, packages);
        model.put(MODEL_FAILURES, failures);

        if (packages.isEmpty()) {
            throw new NoPackagesFoundException("no packages " + packageNames + " for subjects " + subjects);
        }

        final Map<String,Object> transferRequestOptions = Maps.newLinkedHashMap();
        if (!Strings.isNullOrEmpty(destinationPath)) {
            transferRequestOptions.put(AsperaTokenView.OPT_DESTINATION_ROOT, destinationPath);
        }
        transferRequestOptions.put(AsperaTokenView.OPT_COOKIE, "XDATUser=" + user.getLogin());
        final Map<String,Object> transferSpecOptions = Maps.newLinkedHashMap();
        transferSpecOptions.put("target_rate_kbps", target_rate_kbps);

        // TODO: less grotesque view handling
        if (Strings.isNullOrEmpty(view) || "aspera".equalsIgnoreCase(view)) {
            downloadLogger.info("{} downloading {} x {} ({} bytes)",
                    new Object[]{user.getLogin(), packageNames, subjects, size});
            return new ModelAndView(new AsperaTokenView(asperaNodeURL, nodeUser, nodePassword,
                    transferRequestOptions, transferSpecOptions), model);
        } else if ("subjects".equalsIgnoreCase(view)) {
            model.put("json", buildSubjectsSummary(project, user, subjects, packageNames));
            return new ModelAndView(new JSONOrgView(), model);
        } else {
            throw new UnsupportedOperationException("no handler defined for view " + view);
        }
    }
}
