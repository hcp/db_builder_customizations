/**
 * Copyright (c) 2013 Washington University School of Medicine
 */
package org.nrg.xnat.download;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.nrg.xnat.entities.PackageInfo;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface PackageBuilderProvider {
    Set<String> getPackageNames(String project);
    String getPackageDescription(String project, String packageName);
    Set<String> getPackageKeywords(String project, String packageName);
    String getPackageLabel(String project, String packageName);
    Map<String,PackageInfo> getPackageInfo(Collection<String> projectPaths,Collection<String> subjects);
    Long getCountByPackageIds(Collection<String> projectPaths,Collection<String> subjects,Collection<String> packageIds);
    PackageBuilder apply(String project, String packageName);
}
