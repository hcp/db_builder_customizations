package org.nrg.xnat.download;

import org.nrg.xdat.XDAT;
import org.nrg.xnat.entities.PackageFile;
import org.nrg.xnat.services.PackageFileService;
import org.nrg.xnat.utils.ArchiveMetaData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipFile;

/**
 * Created with IntelliJ IDEA.
 * User: mmckay01
 * Date: 11/6/13
 * Time: 4:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class RefreshPackagesRequestListener {

    private final static Logger logger = LoggerFactory.getLogger(RefreshPackagesRequestListener.class);

    public void onRefreshPackagesRequest(final RefreshPackagesRequest refreshPackagesRequest) throws Exception {
        final PackageFileService service = XDAT.getContextService().getBean(PackageFileService.class);
        final List<String> filePaths = new ArrayList<String>();
        final String packageDir = refreshPackagesRequest.getPackageDir().toString();
        long startTime = System.nanoTime();
        logger.info("Package Refresh Service started.");

        try {
            // Add packages not currently represented in the database
            Files.walkFileTree(refreshPackagesRequest.getPackageDir(), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                        throws IOException {
                	final String packageFilePath = file.toString();

                    if (packageFilePath.endsWith(".zip")) {
                    	filePaths.add(packageFilePath);
                        final PackageFile existingFile = service.getPackageFileByPath(packageFilePath);
                        //final File currFile = new File(file.toUri());
                        //long lastModified = currFile.lastModified();
                        long lastModified = attrs.lastModifiedTime().toMillis() / 1000;

                        if (existingFile == null || lastModified != existingFile.getLastModified()) {
                            //File has changed since its entry was updated in the database or is not yet in the database.
                            // long newSize = currFile.length();
                            long newSize = attrs.size();
                            int newCount = 1;
                            try {
                                ZipFile zipFile = new ZipFile(file.toString());
                                // newCount = ArchiveMetaData.getArchiveMap(packageFilePath).get(ArchiveMetaData.EntryType.File).size();
                                newCount = zipFile.size();
                            } catch (Exception e) {
                                logger.info("Exception on get count:" + packageFilePath, e);
                            }

                            if (existingFile == null) {
                            	if (packageFilePath.startsWith(packageDir)) {
                            		final String[] filePathParts = packageFilePath.replaceFirst(packageDir + "[/]*","").split("[/]+");
                            		final String subjectPart = filePathParts[0];
                            		if (!filePathParts[filePathParts.length-1].startsWith(subjectPart)) {
                            			logger.error("ERROR:  File name unexpectedly does not begin with subject label (subjectPart=" + subjectPart + ", packageFilePath=" + packageFilePath + ").  File will be skipped");
                            			return FileVisitResult.CONTINUE;
                            		}
                            		final String packagePart = filePathParts[filePathParts.length-1].replaceFirst(subjectPart + "_", "").replaceFirst(".zip$", "");
                            		final PackageFile newFile = new PackageFile(packageDir , subjectPart, packagePart, packageFilePath, newSize, newCount, lastModified);
                            		XDAT.getContextService().getBean(PackageFileService.class).create(newFile);
                            	} else {
                            		logger.error("ERROR:  File path unexpectedly does not begin with package directory (packageDir=" + packageDir + ", packageFilePath=" + packageFilePath + ").  File will be skipped");
                            	}
                            } else {
                                if (existingFile.getFileCount() != newCount || existingFile.getFileSize() != newSize) {
                                    existingFile.setFileCount(newCount);
                                    existingFile.setFileSize(newSize);
                                    existingFile.setLastModified(lastModified);
                                    XDAT.getContextService().getBean(PackageFileService.class).update(existingFile);
                                }
                            }
                        }
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
            // Remove entries for which files no longer exist
            for (PackageFile pf : service.getAll()) {
            	if (pf.getFilePath().indexOf(refreshPackagesRequest.getPackageDir().toString()) == 0 && !filePaths.contains(pf.getFilePath())) {
            		service.delete(pf);
            	}
            }
            service.refreshCache();
            XDAT.getContextService().getBean(HCPPackageBuilderProvider.class).refreshPackageFileCache();
            long endTime = System.nanoTime();
            double duration = (endTime - startTime) / 1000000000.0;
            logger.info("Package Refresh Service completed in " + duration + " seconds.");
        } catch (IOException e){
            if(refreshPackagesRequest.getSubDir()!=null){
                logger.error("IO Exception when updating the package information table for "+refreshPackagesRequest.getSubDir()+".", e);
            }
            else{
                logger.error("IO Exception when updating the package information table.", e);
            }
        } catch (Exception e){
            if(refreshPackagesRequest.getSubDir()!=null){
                logger.error("Exception updating the package information table for "+refreshPackagesRequest.getSubDir()+".", e);
            }
            else{
                logger.error("Exception updating the package information table.", e);
            }
        }
    }

}
