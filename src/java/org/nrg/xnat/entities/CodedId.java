package org.nrg.xnat.entities;

/**
 * Created by mmckay01 on 1/29/14.
 */

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.StringUtils;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.framework.orm.hibernate.annotations.Auditable;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.XFTTable;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.search.ItemSearch;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"project","subjectId"}))
public class CodedId extends AbstractHibernateEntity{

    private String project;
    private String subjectId;
    private String codedId;

    public CodedId() {
    }

    public CodedId(String project, String subjectLabel, String codedId) {
        this.project = project;
        this.codedId = codedId;
        setSubjectId(subjectLabel);
    }

    public String getProject(){
        return project;
    }

    public String getSubjectId(){
        return subjectId;
    }

    public String getCodedId(){
        return codedId;
    }

    public void setProject(String project){
        this.project = project;
    }

    public void setSubjectId(String subjectLabel){
        this.subjectId = subjectLabelToSubjectId(subjectLabel, project);
    }

    public void setCodedId(String codedId){
        this.codedId = codedId;
    }

    private String subjectLabelToSubjectId(String subjectLabel, String project){
        String subjectId = subjectLabel;
        try{
            final XDATUser user = XDAT.getUserDetails();
            String defaultProject = StringUtils.trimToEmpty(XDAT.getConfigService().getConfigContents("dashboard", "project"));
            //final XFTTable t= XFTTable.Execute("SELECT id FROM xnat_subjectData WHERE project='"+ StringUtils.trimToEmpty(XDAT.getConfigService().getConfigContents("dashboard", "project")) + "' AND label='"+ subjectLabel + "'", user.getDBName(),user.getUsername());
            final XFTTable t= XFTTable.Execute("SELECT DISTINCT s.ID, 'xnat:subjectData' AS element_name, ids,s.project AS project,projects,s.label FROM xnat_subjectData s LEFT JOIN xnat_subjectdata_addid addid ON s.id=addid.xnat_subjectdata_id LEFT JOIN xnat_projectparticipant pp ON s.id=pp.subject_id LEFT JOIN (SELECT project_group.subject_id AS id, btrim(xs_a_concat(project_group.ids || ', '::text), ', '::text) AS ids, btrim(xs_a_concat(('<'::text || project_group.projects) || '>, '::text), ', '::text) AS projects  FROM ( SELECT ((COALESCE(xnat_projectparticipant.label, xnat_projectparticipant.subject_id)::text || ' ('::text) || btrim(xs_a_concat(xnat_projectparticipant.project::text || ', '::text), ', '::text)) || ')'::text AS ids, btrim(xs_a_concat(('<'::text || xnat_projectparticipant.project::text) || '>, '::text), ', '::text) AS projects, xnat_projectparticipant.subject_id FROM xnat_projectparticipant WHERE project='"+ defaultProject + "' GROUP BY COALESCE(xnat_projectparticipant.label, xnat_projectparticipant.subject_id), xnat_projectparticipant.subject_id) project_group GROUP BY project_group.subject_id) xnat_projs ON s.id=xnat_projs.id WHERE LOWER(s.ID) = '"+ subjectLabel + "' OR  LOWER(addid) = '"+ subjectLabel + "' OR LOWER(s.label) = '"+ subjectLabel + "' OR LOWER(pp.label) = '"+ subjectLabel + "' UNION SELECT DISTINCT expt.ID,element_name,ids,expt.project,projects,expt.label FROM xnat_experimentData expt LEFT JOIN xnat_experimentData_share proj ON expt.id=proj.sharing_share_xnat_experimentda_id LEFT JOIN xdat_meta_element me ON expt.extension=me.xdat_meta_element_id LEFT JOIN ( SELECT project_group.sharing_share_xnat_experimentda_id AS id, btrim(xs_a_concat(project_group.ids || ', '::text), ', '::text) AS ids, btrim(xs_a_concat(project_group.projects || ', '::text), ', '::text) AS projects   FROM ( SELECT ((COALESCE(proj.label, sharing_share_xnat_experimentda_id)::text || ' ('::text) || btrim(xs_a_concat(proj.project::text || ', '::text), ', '::text)) || ')'::text AS ids, btrim(xs_a_concat(('<'::text || proj.project::text) || '>, '::text), ','::text) AS projects, sharing_share_xnat_experimentda_id           FROM xnat_experimentdata_share proj      LEFT JOIN xnat_experimentdata expt ON proj.sharing_share_xnat_experimentda_id::text = expt.id::text  GROUP BY COALESCE(proj.label, sharing_share_xnat_experimentda_id), sharing_share_xnat_experimentda_id) project_group  GROUP BY project_group.sharing_share_xnat_experimentda_id) xnat_projects ON expt.id=xnat_projects.id WHERE LOWER(expt.ID) = '"+ subjectLabel + "' OR LOWER(expt.label) = '"+ subjectLabel + "' OR LOWER(proj.label) = '"+ subjectLabel + "' ORDER BY element_name;", user.getDBName(),user.getUsername());
            ArrayList<List> arr = t.toArrayListOfLists();
            for(int i=0;i<arr.size();i++){
                if(((String)arr.get(i).get(4))!=null && ((String)arr.get(i).get(4)).equals("<<"+defaultProject+">>") && (String)arr.get(i).get(0)!=null){
                    subjectId = (String)arr.get(i).get(0);
                }
            }
        }catch(Exception e){
        }
        return subjectId;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null || !(object instanceof CodedId)) {
            return false;
        }
        CodedId other = (CodedId) object;
        return           (getProject().equals(other.getProject()) &&
                getSubjectId().equals(other.getSubjectId()) &&
                getCodedId().equals(other.getCodedId()));
    }


}
