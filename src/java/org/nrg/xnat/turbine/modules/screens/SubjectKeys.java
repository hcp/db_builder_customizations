package org.nrg.xnat.turbine.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.hcp.security.HcpUserDatabaseEntryUtils;

public class SubjectKeys extends SecureScreen {
    public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SecureScreen.class);

	@Override
	protected void doBuildTemplate(final RunData data, final Context context) throws Exception {
//        // Set tier to whatever is in the query string or the session variable
//        // Tier changes only if user has access to that tier for the project
//        XDATUser user = (XDATUser)data.getSession().getAttribute("user");
//        String project = (String) data.getSession().getAttribute("project");
//        // project context should be the root parent [getRootParent()]
//        // as of now, this will change the context to the SubjectKey ID
//        int requestedTier = 0;
//        int newTier = 0;
//
//        if(TurbineUtils.HasPassedParameter("tierSelect", data))
//            requestedTier = TurbineUtils.GetPassedInteger("tierSelect", data);
//
//        if(HcpUserDatabaseEntryUtils.hasAccessLevel(user, project, requestedTier))
//            newTier = requestedTier;
//
//        data.getSession().setAttribute("tier", newTier);
//
//        // See if user has restricted access within the current project context
//        if(HcpUserDatabaseEntryUtils.getAccessLevel(user, project) > 0)
//            data.getSession().setAttribute("hasRestrictedAccess", true);

        try {
            XDATUser user = (XDATUser)data.getSession().getAttribute("user");
            String sessionProject = (String) data.getSession().getAttribute("project");
            String projectContext = sessionProject; // initial value before checks, which might stay the same

            // Check if there's been any change in project context
            if(TurbineUtils.HasPassedParameter("project", data)) {
                String queryProject = (String) TurbineUtils.GetPassedParameter("project", data);
                projectContext = queryProject;
                XnatProjectdata p = XnatProjectdata.getProjectByIDorAlias(queryProject, user, false);

                // Use the parent project if there is one
                // Keep looping until we get to the root parent
                while (p.getParentproject() != null)
                    projectContext = p.getParentproject();

                if(!sessionProject.equals(projectContext)) {
                    // The project has changed
                    data.getSession().setAttribute("project", projectContext);
                    data.getSession().setAttribute("tier", 0);
                }
            }

            context.put("project", projectContext);
            String currentProject = projectContext;


            // Set tier to whatever is in the query string or the session variable
            // Tier changes only if user has access to that tier for the project
            if (TurbineUtils.HasPassedParameter("tierSelect", data)) {
                int requestedTier = TurbineUtils.GetPassedInteger("tierSelect", data);

                if (HcpUserDatabaseEntryUtils.hasAccessLevel(user, currentProject, requestedTier)) {
                    data.getSession().setAttribute("tier", requestedTier);
                }
            } // Otherwise, leave the tier alone

            // As a safeguard, check that the user has access to the currently set tier value
            int tier = (Integer) data.getSession().getAttribute("tier");

            if (!HcpUserDatabaseEntryUtils.hasAccessLevel(user, currentProject, tier))
                data.getSession().setAttribute("tier", 0);

            // See if user has any restricted access within the current project context
            if(HcpUserDatabaseEntryUtils.getAccessLevel(user, currentProject) > 0)
                data.getSession().setAttribute("hasRestrictedAccess", true);

        } catch(Exception e) {
            logger.error(e);
        }
	}

}
