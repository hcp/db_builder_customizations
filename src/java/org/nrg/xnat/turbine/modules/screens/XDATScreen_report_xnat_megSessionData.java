package org.nrg.xnat.turbine.modules.screens;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMegsessiondata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTItem;
import org.nrg.xft.search.CriteriaCollection;
import org.apache.commons.lang.StringUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;

/**
 * Created with IntelliJ IDEA.
 * User: mmckay01
 * Date: 7/30/13
 * Time: 3:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class XDATScreen_report_xnat_megSessionData extends HcpSecureReport {
    public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XDATScreen_report_xnat_megSessionData.class);

    @Override
    public void finalProcessing(RunData data, Context context) {
        // Handle tiers in super class
        super.finalProcessing(data, context);

        try{
            XnatMegsessiondata meg = new XnatMegsessiondata(item);
            String project = "";
            if(TurbineUtils.HasPassedParameter("tierSelect", data)){
                data.getSession().setAttribute("tier",TurbineUtils.GetPassedInteger("tierSelect", data));
            }
            if(TurbineUtils.HasPassedParameter("project", data)){
                project = (String)TurbineUtils.GetPassedParameter("project", data);
            }
            else{
                project = meg.getProject();
            }

            if(!project.equals((String)data.getSession().getAttribute("project")) &&!((project.equals("HCP_Subjects")||project.equals("HCP_500_RST")||project.equals("HCP_500"))&& ((((String)data.getSession().getAttribute("project")).equals("HCP_500_RST"))||(((String)data.getSession().getAttribute("project")).equals("HCP_Subjects"))||(((String)data.getSession().getAttribute("project")).equals("HCP_500")) ) )){
                data.getSession().setAttribute("project",project);
                data.getSession().setAttribute("tier", 0);
            }
            context.put("project", project);

        } catch(Exception e){}
    }
}
