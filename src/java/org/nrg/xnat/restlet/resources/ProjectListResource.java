/*
 * org.nrg.xnat.restlet.resources.ProjectListResource
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 8:40 PM
 */
package org.nrg.xnat.restlet.resources;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.action.ActionException;
import org.nrg.framework.utilities.Reflection;
import org.nrg.xdat.exceptions.IllegalAccessException;
import org.nrg.xdat.om.XdatStoredSearch;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.base.BaseXnatProjectdata;
import org.nrg.xdat.search.DisplayCriteria;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.security.SecurityManager;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.XFT;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.TypeConverter.JavaMapping;
import org.nrg.xft.TypeConverter.TypeConverter;
import org.nrg.xft.db.ViewManager;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.schema.Wrappers.GenericWrapper.GenericWrapperElement;
import org.nrg.xft.schema.Wrappers.GenericWrapper.GenericWrapperField;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.search.QueryOrganizer;
import org.nrg.xft.utils.DateUtils;
import org.nrg.xft.utils.StringUtils;
import org.nrg.xnat.helpers.prearchive.PrearcUtils;
import org.nrg.xnat.helpers.xmlpath.XMLPathShortcuts;
import org.nrg.xnat.restlet.representations.ItemXMLRepresentation;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
import java.util.*;

public class ProjectListResource extends QueryOrganizerResource {
	private static final String ACCESSIBLE = "accessible";
	private static final String DATA_ACCESSIBILITY = "data";
	private static final String DATA_READABLE = "readable";
	private static final String DATA_WRITABLE = "writable";
	private static final List<String> PERMISSIONS = Arrays.asList(SecurityManager.ACTIVATE, SecurityManager.CREATE, SecurityManager.DELETE, SecurityManager.EDIT, SecurityManager.READ);
	XFTTable table = null;

	public ProjectListResource(Context context, Request request, Response response) {
		super(context, request, response);
		
			this.getVariants().add(new Variant(MediaType.APPLICATION_JSON));
			this.getVariants().add(new Variant(MediaType.TEXT_HTML));
			this.getVariants().add(new Variant(MediaType.TEXT_XML));

			this.fieldMapping.putAll(XMLPathShortcuts.getInstance().getShortcuts(XMLPathShortcuts.PROJECT_DATA,true));
	
	}
	
	

	@Override
	public boolean allowPost() {
		return true;
	}



	@Override
	public void handlePost() {
		XFTItem item = null;
		try {
			item=this.loadItem("xnat:projectData",true);

			if(item==null){
				String xsiType=this.getQueryVariable("xsiType");
				if(xsiType!=null){
					item=XFTItem.NewItem(xsiType, user);
				}
			}

			if(item==null){
				this.getResponse().setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED, "Need POST Contents");
				return;
			}

			boolean allowDataDeletion =false;
			if(this.getQueryVariable("allowDataDeletion")!=null && this.getQueryVariable("allowDataDeletion").equalsIgnoreCase("true")){
				allowDataDeletion =true;
			}

			if(item.instanceOf("xnat:projectData")){
				XnatProjectdata project = new XnatProjectdata(item);

				if(StringUtils.IsEmpty(project.getId())){
					this.getResponse().setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED,"Requires XNAT ProjectData ID");
					return;
				}

				if(!StringUtils.IsAlphaNumericUnderscore(project.getId())){
					this.getResponse().setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED,"Invalid character in project ID.");
					return;
				}

				if(item.getCurrentDBVersion()==null){
					if(XFT.getBooleanProperty("UI.allow-non-admin-project-creation", true) || user.isSiteAdmin()){
						this.returnSuccessfulCreateFromList(BaseXnatProjectdata.createProject(project, user, allowDataDeletion,false,newEventInstance(EventUtils.CATEGORY.PROJECT_ADMIN),getQueryVariable("accessibility")));
					}else{
						this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,"User account doesn't have permission to edit this project.");
						return;
					}
				}else{
					this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Project already exists.");
				}
			}
        } catch (ActionException e) {
			this.getResponse().setStatus(e.getStatus(),e.getMessage());
        } catch (Exception e) {
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			logger.error(e);
		}
	}


	@Override
	public ArrayList<String> getDefaultFields(GenericWrapperElement e) {
		ArrayList<String> al= new ArrayList<>();
		al.add("ID");
		al.add("secondary_ID");
		al.add("name");
		al.add("description");
		al.add("pi_firstname");
		al.add("pi_lastname");
		
		return al;
	}

	public String getDefaultElementName(){
		return "xnat:projectData";
	}


	@Override
	public boolean allowGet() {
		return true;
	}

    private List<ProjectListFilterI> getMatches(Map<String,String> qsParams) throws ClassNotFoundException, IOException, InstantiationException{
        List<ProjectListFilterI> filters=null;
        List<ProjectListFilterI> matched=Lists.newArrayList();
        try {
            filters=getFilters();
        } catch (Exception e1) {
            logger.error("",e1);
        }

        if(filters!=null){
            for(ProjectListFilterI filter: filters){
                if(filter.match(qsParams)){
                    matched.add(filter);
                }
            }
        }
        return matched;
    }
	
	private List<ProjectListFilterI> getFilters() throws ClassNotFoundException, IOException, InstantiationException{
		List<Class<?>> classes = Reflection.getClassesForPackage("org.nrg.xnat.restlet.projectList.filters");

		List<ProjectListFilterI> filters=Lists.newArrayList();
		if(classes!=null && classes.size()>0){
			 for(Class<?> clazz: classes){
				 if(ProjectListFilterI.class.isAssignableFrom(clazz)){
                     try{
					    filters.add((ProjectListFilterI)clazz.newInstance());
                     } catch (java.lang.IllegalAccessException e) {

                     }
				 }
			 }
		 }
		
		return filters;
	}
    private final static List<FilteredResourceHandlerI> _defaultHandlers = Lists.newArrayList();
	static {
		_defaultHandlers.add(new DefaultProjectHandler());
		_defaultHandlers.add(new FilteredProjects());
		_defaultHandlers.add(new PermissionsProjectHandler());
	}

	public static interface ProjectListFilterI{
		public boolean match(Map<String, String> params);
		public void applyFilter(final Map<String, String> params, final CriteriaCollection andCC, final CriteriaCollection orCC, final XDATUser user) throws ServerException,ClientException;
	}

	@Override
	public Representation represent(Variant variant) {
		Representation defaultRepresentation = super.represent(variant);
		if (defaultRepresentation != null)
			return defaultRepresentation;

        Map<String,String> qsParams=this.getQueryVariableMap();

        try{
            List<ProjectListFilterI> matched=getMatches(qsParams);
        }
		catch(Exception e){
            logger.error("",e);
        }

		FilteredResourceHandlerI handler=null;
        try {
			for(FilteredResourceHandlerI filter:getHandlers("org.nrg.xnat.restlet.projectsList.extensions",_defaultHandlers)){
				if(filter.canHandle(this)){
					handler=filter;
				}
			}
        } catch (InstantiationException | java.lang.IllegalAccessException | ConcurrentModificationException e1) {
			logger.error("",e1);
		}

		try {
			if(handler!=null){
				return handler.handle(this,variant);
			}else{
				return null;
			}
		} catch (Exception e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			return null;
		}
	}

    public static class FilteredProjects implements FilteredResourceHandlerI {
		
		private final static String STRING="java.lang.String";
		private final static String DOUBLE="java.lang.Double";
		private final static String INTEGER="java.lang.Integer";
		private final static String DATE="java.util.Date";
		private final static String BOOL="java.lang.Boolean";
    	
		@Override
		public boolean canHandle(SecureResource resource) {
            boolean canHandle = false;
            Map<String,String> qsParams=((ProjectListResource)resource).getQueryVariableMap();

            try{
                List<ProjectListFilterI> matched=((ProjectListResource)resource).getMatches(qsParams);
                canHandle = (resource.containsQueryVariable(ACCESSIBLE)
                        || resource.containsQueryVariable("prearc_code")
                        || resource.containsQueryVariable("owner")
                        || resource.containsQueryVariable("member")
                        || resource.containsQueryVariable("collaborator")
                        || resource.containsQueryVariable("activeSince")
                        || resource.containsQueryVariable("recent")
                        || resource.containsQueryVariable("favorite")
                        || resource.containsQueryVariable("admin")
                        || (matched.size()>0)
                        || (resource.requested_format != null && resource.requested_format.equals("search_xml")));
            }
            catch(Exception e){
                logger.error("",e);
            }
            return canHandle;
		}

		@Override
		public Representation handle(SecureResource resource, Variant variant) throws Exception {

			final DisplaySearch ds = new DisplaySearch();
			final XDATUser user=resource.user;
			XFTTable table=null;

			try {
				ds.setUser(user);
				ds.setRootElement("xnat:projectData");
				ds.addDisplayField("xnat:projectData", "ID");
				ds.addDisplayField("xnat:projectData", "NAME");
				ds.addDisplayField("xnat:projectData", "DESCRIPTION");
				ds.addDisplayField("xnat:projectData", "SECONDARY_ID");
				ds.addDisplayField("xnat:projectData", "PI");
				ds.addDisplayField("xnat:projectData", "PROJECT_INVS");
				ds.addDisplayField("xnat:projectData", "PROJECT_ACCESS");
				ds.addDisplayField("xnat:projectData", "PROJECT_ACCESS_IMG");
                ds.addDisplayField("xnat:projectData", "PROJECT_CODED_IDS");
                ds.addDisplayField("xnat:projectData", "PROJECT_PUBLICATIONS");
				ds.addDisplayField("xnat:projectData", "INSERT_DATE");
				ds.addDisplayField("xnat:projectData", "INSERT_USER");
                ds.addDisplayField("xnat:projectData", "USER_ROLE", "Role", user.getXdatUserId());
				ds.addDisplayField("xnat:projectData", "LAST_ACCESSED", "Last Accessed", user.getXdatUserId());

				if (resource.isQueryVariableTrue("prearc_code")) {
					ds.addDisplayField("xnat:projectData", "PROJ_QUARANTINE");
					ds.addDisplayField("xnat:projectData", "PROJ_PREARCHIVE_CODE");
				}
				

                final CriteriaCollection allCC = new CriteriaCollection("AND");
				final CriteriaCollection orCC = new CriteriaCollection("OR");
				
				String dataAccess = resource.getQueryVariable(DATA_ACCESSIBILITY);
				if (dataAccess != null) {
					if (dataAccess.equalsIgnoreCase(DATA_WRITABLE)) {
						if (user.getGroup("ALL_DATA_ACCESS") == null && user.getGroup("ALL_DATA_ADMIN") == null) {
							CriteriaCollection cc = new CriteriaCollection("OR");
							DisplayCriteria dc = new DisplayCriteria();
							dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_MEMBERS");
							dc.setComparisonType(" LIKE ");
							dc.setValue("% " + user.getLogin() + " %", false);
							cc.add(dc);

							dc = new DisplayCriteria();
							dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_OWNERS");
							dc.setComparisonType(" LIKE ");
							dc.setValue("% " + user.getLogin() + " %", false);
							cc.add(dc);

							allCC.addCriteria(cc);
						}
					} else if (dataAccess.equalsIgnoreCase(DATA_READABLE)) {
                        if (user.getGroup("ALL_DATA_ACCESS") == null && user.getGroup("ALL_DATA_ADMIN") == null) {
                            CriteriaCollection cc = new CriteriaCollection("OR");
                            DisplayCriteria dc = new DisplayCriteria();
                            dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_USERS");
                            dc.setComparisonType(" LIKE ");
                            dc.setValue("% " + user.getLogin() + " %", false);
                            cc.add(dc);

                            dc = new DisplayCriteria();
                            dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_ACCESS");
                            dc.setValue("public", false);
                            cc.add(dc);
							
							dc = new DisplayCriteria();
							dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_ACCESS");
							dc.setValue("protected", false);
							cc.add(dc);

                            allCC.addCriteria(cc);
                        }
                    } else {
                        throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "The value specified for the " + DATA_ACCESSIBILITY + " parameter is invalid: " + dataAccess + ". Must be one of " + DATA_READABLE + " or " + DATA_WRITABLE + ".");
					}
				}
				
				// Subset results based on filter parameters.  NOTE:  Columns parameter not yet implemented.
				final CriteriaCollection filterCritCC = new CriteriaCollection("AND");
				
				if(resource.fieldMapping.size()>0){
					for(final String key: resource.fieldMapping.keySet()){
						if(!key.equals("xsiType") && resource.hasQueryVariable(key)){
							filterCritCC.add(processQueryCriteria(resource.fieldMapping.get(key),resource.getQueryVariable(key)));
						}
					}
				}
				
				for(final String key:resource.getQueryVariableKeys()){
					if(key.indexOf("/")>-1){
						filterCritCC.add(processQueryCriteria(key,resource.getQueryVariable(key)));
					}
				}
				
				if(resource.isQueryVariable("req_format", "form", false))
				{
					if(resource.fieldMapping.size()>0){
						for(final String key: resource.fieldMapping.keySet()){
							if(resource.hasBodyVariable(key)){
								filterCritCC.add(processQueryCriteria(resource.fieldMapping.get(key),resource.getBodyVariable(key)));
							}
						}
					}
					
					for(final String key:resource.getBodyVariableKeys()){
						if(key.indexOf("/")>-1){
							filterCritCC.add(processQueryCriteria(key,resource.getBodyVariable(key)));
						}
					}
					
				}
				if (filterCritCC.numClauses()>0) {
					allCC.addCriteria(filterCritCC);
				}
				
				final String access = resource.getQueryVariable(ACCESSIBLE);
				if (access != null) {
					if (access.equalsIgnoreCase("true")) {
						if (user.getGroup("ALL_DATA_ACCESS") == null && user.getGroup("ALL_DATA_ADMIN") == null) {
							final CriteriaCollection cc = new CriteriaCollection("OR");
							DisplayCriteria dc = new DisplayCriteria();
                            dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_USERS");
							dc.setComparisonType(" LIKE ");
							dc.setValue("% " + user.getLogin() + " %", false);
							cc.add(dc);
							
							dc = new DisplayCriteria();
							dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_ACCESS");
							dc.setValue("public", false);
							cc.add(dc);
							
							allCC.addCriteria(cc);
						}
					}else{
						final CriteriaCollection cc = new CriteriaCollection("OR");
						DisplayCriteria dc = new DisplayCriteria();
						dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_USERS");
						dc.setComparisonType(" NOT LIKE ");
						dc.setValue("% " + user.getLogin() + " %", false);
						cc.add(dc);
						
						dc = new DisplayCriteria();
						dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_USERS");
						dc.setComparisonType(" IS ");
						dc.setValue(" NULL ", false);
						dc.setOverrideDataFormatting(true);
						cc.add(dc);
						
						allCC.addCriteria(cc);
					}
				}

                final String owner = resource.getQueryVariable("owner");
				if (owner != null) {
					if (owner.equalsIgnoreCase("true")) {
						final CriteriaCollection cc = new CriteriaCollection("OR");
						final DisplayCriteria dc = new DisplayCriteria();
						dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_OWNERS");
						dc.setComparisonType(" LIKE ");
						dc.setValue("% " + user.getLogin() + " %", false);
						cc.add(dc);
						
						orCC.addCriteria(cc);
					} else{
						final CriteriaCollection cc = new CriteriaCollection("OR");
						final DisplayCriteria dc = new DisplayCriteria();
						dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_USERS");
						dc.setComparisonType(" NOT LIKE ");
						dc.setValue("% " + user.getLogin() + " %", false);
						cc.add(dc);
						
						orCC.addCriteria(cc);
					}
				}

                final String project_access = ((ProjectListResource)resource).getQueryVariable("project_access");
                if(project_access!=null){
                    if(project_access.equalsIgnoreCase("public")||project_access.equalsIgnoreCase("private")||project_access.equalsIgnoreCase("protected")){
                        final CriteriaCollection cc = new CriteriaCollection("OR");
                        final DisplayCriteria dc = new DisplayCriteria();
                        dc.setSearchFieldByDisplayField("xnat:projectData","PROJECT_ACCESS");
                        dc.setValue(project_access,false);
                        cc.add(dc);

                        orCC.addCriteria(cc);
                    }
                }
                if (resource.getQueryVariable("admin") != null) {
					if (resource.isQueryVariableTrue("admin")) {
						if (user.checkRole(PrearcUtils.ROLE_SITE_ADMIN)) {
							final CriteriaCollection cc = new CriteriaCollection("OR");
							final DisplayCriteria dc = new DisplayCriteria();
							dc.setSearchFieldByDisplayField("xnat:projectData", "ID");
							dc.setComparisonType(" IS NOT ");
							dc.setValue(" NULL ", false);
							dc.setOverrideDataFormatting(true);
							cc.add(dc);
							orCC.addCriteria(cc);
						}			
					}
					else {
						
					}
				}

                String member = resource.getQueryVariable("member");
				if (member != null) {
					if (member.equalsIgnoreCase("true")) {
						CriteriaCollection cc = new CriteriaCollection("OR");
						DisplayCriteria dc = new DisplayCriteria();
						dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_MEMBERS");
						dc.setComparisonType(" LIKE ");
						dc.setValue("% " + user.getLogin() + " %", false);
						cc.add(dc);
						
						orCC.addCriteria(cc);
					} else{
						CriteriaCollection cc = new CriteriaCollection("OR");
						DisplayCriteria dc = new DisplayCriteria();
						dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_MEMBERS");
						dc.setComparisonType(" NOT LIKE ");
						dc.setValue("% " + user.getLogin() + " %", false);
						cc.add(dc);
						
						orCC.addCriteria(cc);
					}
				}

                String collaborator = resource.getQueryVariable("collaborator");
				if (collaborator != null) {
					if (collaborator.equalsIgnoreCase("true")) {
						CriteriaCollection cc = new CriteriaCollection("OR");
						DisplayCriteria dc = new DisplayCriteria();
						dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_COLLABS");
						dc.setComparisonType(" LIKE ");
						dc.setValue("% " + user.getLogin() + " %", false);
						cc.add(dc);
						
						orCC.addCriteria(cc);
					} else{
						CriteriaCollection cc = new CriteriaCollection("OR");
						DisplayCriteria dc = new DisplayCriteria();
						dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_COLLABS");
						dc.setComparisonType(" NOT LIKE ");
						dc.setValue("% " + user.getLogin() + " %", false);
						cc.add(dc);
						
						orCC.addCriteria(cc);
					}
				}

                final String activeSince = resource.getQueryVariable("activeSince");
				if (activeSince != null) {
					try {
						final Date d = DateUtils.parseDateTime(activeSince);
						
						final DisplayCriteria dc = new DisplayCriteria();
						dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_LAST_WORKFLOW");
						dc.setComparisonType(">");
						dc.setValue(d, false);
						orCC.add(dc);
					} catch (RuntimeException e) {
						resource.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
					}
				}

                final String recent = resource.getQueryVariable("recent");
				if (recent != null) {
					try {
						final DisplayCriteria dc = new DisplayCriteria();
						dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_LAST_ACCESS");
						dc.setComparisonType(" LIKE ");
						dc.setValue("% " + user.getLogin() + " %", false);
						orCC.addCriteria(dc);
					} catch (RuntimeException e) {
						resource.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
					}
				}

                final String favorite = resource.getQueryVariable("favorite");
                if (favorite != null) {
                    try {
                        final DisplayCriteria dc = new DisplayCriteria();
                        dc.setSearchFieldByDisplayField("xnat:projectData", "PROJECT_FAV");
                        dc.setComparisonType(" LIKE ");
                        dc.setValue("% " + user.getLogin() + " %", false);
                        orCC.addCriteria(dc);
                    } catch (RuntimeException e) {
                        resource.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
                    }
                }

                final Map<String,String> qsParams=((ProjectListResource)resource).getQueryVariableMap();

                final List<ProjectListFilterI> matched=((ProjectListResource)resource).getMatches(qsParams);

				for(ProjectListFilterI filter: matched){
					filter.applyFilter(qsParams, allCC, orCC,user);
				}

				if(orCC.size() > 0)
					allCC.addCriteria(orCC);

				if(allCC.size() > 0)
					ds.addCriteria(allCC);
				
				
				ds.setSortBy("SECONDARY_ID");

                if (resource.requested_format == null || !resource.requested_format.equals("search_xml")) {
					table = (XFTTable) ds.execute(user.getLogin());
				}
			} catch (ClientException e) {
				logger.error("", e);
                resource.getResponse().setStatus(e.getStatus());
				return null;
			} catch (ServerException e) {
				logger.error("", e);
                resource.getResponse().setStatus(e.getStatus());
				return null;
			} catch (IllegalAccessException e) {
				logger.error("", e);
                resource.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
				return null;
			}catch (Exception e) {
				logger.error("", e);
                resource.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
				return null;
			}

            Hashtable<String, Object> params = new Hashtable<>();
			params.put("title", "Projects");
			params.put("xdat_user_id", user.getXdatUserId());
	
			final MediaType mt = resource.overrideVariant(variant);

            if (resource.requested_format != null && resource.requested_format.equals("search_xml")) {

				final XdatStoredSearch xss = ds.convertToStoredSearch("");

				if (xss != null) {
					final ItemXMLRepresentation rep = new ItemXMLRepresentation(xss.getItem(), MediaType.TEXT_XML);
					rep.setAllowDBAccess(false);
					return rep;
				} else {
					resource.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
					return new StringRepresentation("", MediaType.TEXT_XML);
				}
			} else {
				if (table != null)
					params.put("totalRecords", table.size());
				return resource.representTable(table, mt, params);
			}
        }
		
		
		private CriteriaCollection processQueryCriteria(String xPath, String values){
			final CriteriaCollection cc= new CriteriaCollection("OR");
			try {
				final GenericWrapperField gwf =GenericWrapperElement.GetFieldForXMLPath(xPath);
				final String type=gwf.getType(new TypeConverter(new JavaMapping("")));
				
				if(type.equals(STRING)){
					cc.add(this.processStringQuery(xPath, values));
				}else if(type.equals(DOUBLE)){
					cc.add(this.processNumericQuery(xPath, values));
				}else if(type.equals(INTEGER)){
					cc.add(this.processNumericQuery(xPath, values));
				}else if(type.equals(DATE)){
					cc.add(this.processDateQuery(xPath, values));
				}else if(type.equals(BOOL)){
					cc.add(this.processBooleanQuery(xPath, values));
				}
			} catch (XFTInitException e) {
				logger.error("",e);
			} catch (ElementNotFoundException e) {
				logger.error("",e);
			} catch (FieldNotFoundException e) {
				logger.error("",e);
			}
			return cc;
		}
		
		
		private CriteriaCollection processStringQuery(String xmlPath, String values){
			final ArrayList<String> al=StringUtils.CommaDelimitedStringToArrayList(values);
			final CriteriaCollection cc= new CriteriaCollection("OR");
			for(String value:al){
				if(value.indexOf("%")>-1 || value.indexOf("*")>-1){
					value=StringUtils.ReplaceStr(value, "*", "%");
					cc.addClause(xmlPath, "LIKE", value);
				}else{
					cc.addClause(xmlPath, value);
				}
			}
			return cc;
		}
		
		private CriteriaCollection processDateQuery(String column, String dates){
			final ArrayList<String> al=StringUtils.CommaDelimitedStringToArrayList(dates);
			final CriteriaCollection cc= new CriteriaCollection("OR");
			for(final String date:al){
				if(date.indexOf("-")>-1){
					String date1=null;
					try {
						date1=DateUtils.parseDate(date.substring(0,date.indexOf("-"))).toString();
					} catch (ParseException e) {
						date1=date.substring(0,date.indexOf("-"));
					}
		
					String date2=null;
					try {
						date2=DateUtils.parseDate(date.substring(date.indexOf("-")+1)).toString();
					} catch (ParseException e) {
						date2=date.substring(date.indexOf("-")+1);
					}

					final CriteriaCollection subCC = new CriteriaCollection("AND");
					subCC.addClause(column, ">=", date1);
					subCC.addClause(column, "<=", date2);
					cc.add(subCC);
				}else{
					String date1=null;
					try {
						date1=DateUtils.parseDate(date).toString();
					} catch (ParseException e) {
						date1=date;
					}
					cc.addClause(column, date1);
				}
			}
			return cc;
		}
		
		private CriteriaCollection processNumericQuery(String column, String values){
			final ArrayList<String> al=StringUtils.CommaDelimitedStringToArrayList(values);
			final CriteriaCollection cc= new CriteriaCollection("OR");
			for(final String date:al){
				if(date.indexOf("-")>-1){
					final String date1=date.substring(0,date.indexOf("-"));
		
					final String date2=date.substring(date.indexOf("-")+1);
					final CriteriaCollection subCC = new CriteriaCollection("AND");
					subCC.addClause(column, ">=", date1);
					subCC.addClause(column, "<=", date2);
					cc.add(subCC);
				}else{
					cc.addClause(column, date);
				}
			}
			return cc;
		}
		
		private CriteriaCollection processBooleanQuery(String column, String values){
			final ArrayList<String> al=StringUtils.CommaDelimitedStringToArrayList(values);
			final CriteriaCollection cc= new CriteriaCollection("OR");
			for(final String value:al){
				cc.addClause(column, value);
			}
			return cc;
		}		
		
    }
	
    public static class PermissionsProjectHandler implements FilteredResourceHandlerI {

		@Override
		public boolean canHandle(SecureResource resource) {
            return resource.containsQueryVariable("permissions");
		}

		@Override
		public Representation handle(SecureResource resource, Variant variant) throws Exception {
            final ArrayList<String> columns = new ArrayList<>();
            columns.add("id");
            columns.add("secondary_id");

            final XFTTable table = new XFTTable();
            table.initTable(columns);

            final String permissions = resource.getQueryVariable("permissions");
            if (StringUtils.IsEmpty(permissions)) {
                throw new Exception("You must specify a value for the permissions parameter.");
            } else if (!PERMISSIONS.contains(permissions)) {
                throw new Exception("You must specify one of the following values for the permissions parameter: " + Joiner.on(", ").join(PERMISSIONS));
            }

            final String dataType = resource.getQueryVariable("dataType");
            final Hashtable<Object, Object> projects = resource.user.getCachedItemValuesHash("xnat:projectData", null, false, "xnat:projectData/ID", "xnat:projectData/secondary_ID");
            for (final Object key : projects.keySet()) {
                final String projectId = (String) key;
                // If no data type is specified, we check both MR and PET session data permissions. This is basically
                // tailored for checking for projects to which the user can upload imaging data.
                final boolean canEdit = StringUtils.IsEmpty(dataType) ? resource.user.hasAccessTo(projectId) : resource.user.canAction(dataType + "/project", projectId, permissions);
                if (canEdit) {
                    table.insertRowItems(projectId, projects.get(projectId));
                }
            }
            table.sort("secondary_id", "ASC");
            return resource.representTable(table, resource.overrideVariant(variant), null);
        }
    }

    public static class DefaultProjectHandler implements FilteredResourceHandlerI{

		@Override
		public boolean canHandle(SecureResource resource) {
			return true;
		}

		@Override
		public Representation handle(SecureResource resource, Variant variant) throws Exception {
			ProjectListResource projResource=(ProjectListResource)resource;
            XFTTable table;
			XDATUser user=resource.user;

			try {
                final String re = projResource.getRootElementName();

				final QueryOrganizer qo = new QueryOrganizer(re, user, ViewManager.ALL);

				projResource.populateQuery(qo);

				if (resource.containsQueryVariable("restrict") && user.getGroup("ALL_DATA_ADMIN") == null) {
					final String restriction = resource.getQueryVariable("restrict");
					if (restriction.equals(SecurityManager.EDIT) || restriction.equals(SecurityManager.DELETE)) {
						final List<Object> ps = user.getAllowedValues("xnat:projectData", "xnat:projectData/ID", restriction);
						final CriteriaCollection cc = new CriteriaCollection("OR");
						for (Object p : ps) {
							cc.addClause("xnat:projectData/ID", p);
						}
						qo.setWhere(cc);
					}
				}
				
				final String query = qo.buildQuery();

                table = XFTTable.Execute(query, user.getDBName(), resource.userName);

                table = projResource.formatHeaders(table, qo, re + "/ID", "/data/projects/");
			} catch (IllegalAccessException e) {
                logger.error("", e);
                resource.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
				return null;
			} catch (Exception e) {
                logger.error("", e);
				resource.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
				return null;
			}

            final MediaType mt = resource.overrideVariant(variant);
			final Hashtable<String, Object> params = new Hashtable<>();
			if (table != null) {
				params.put("totalRecords", table.size());
			}
            return resource.representTable(table, mt, params);
		}
	}

}
