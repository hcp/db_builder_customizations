/*
 * DataDictionaryRestlet
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.restlet.extensions;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xnat.entities.datadictionary.Attribute;
import org.nrg.xnat.entities.datadictionary.Csv;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * DataDictionaryRestlet
 *
 * @author rherri01
 * @since 1/25/13
 */
@XnatRestlet(value = {"/services/ddict", "/services/ddict/{TIER}", "/services/ddict/{TIER}/{CATEGORY}", "/services/ddict/{TIER}/{CATEGORY}/{ASSESSMENT}", "/services/ddict/{TIER}/{CATEGORY}/{ASSESSMENT}/{ATTRIBUTE}", "/services/ddict/{TIER}/{CATEGORY}/{ASSESSMENT}/{ATTRIBUTE}/validate/{VALUE}"}, secure = false)
public class DataDictionaryRestlet extends SecureResource {
    private static final String URL_ENCODING = "UTF-8";
    public DataDictionaryRestlet(Context context, Request request, Response response) throws Exception {
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));

        _service = XDAT.getContextService().getBean(DataDictionaryService.class);
	_tier = URLDecoder.decode(StringUtils.defaultString((String) getRequest().getAttributes().get("TIER")),
		URL_ENCODING);
	_category = URLDecoder.decode(StringUtils.defaultString((String) getRequest().getAttributes().get("CATEGORY")),
		URL_ENCODING);
	_assessment = URLDecoder.decode(
		StringUtils.defaultString((String) getRequest().getAttributes().get("ASSESSMENT")), URL_ENCODING);
	_attribute = URLDecoder.decode(
		StringUtils.defaultString((String) getRequest().getAttributes().get("ATTRIBUTE")), URL_ENCODING);
	_value = URLDecoder.decode(StringUtils.defaultString((String) getRequest().getAttributes().get("VALUE")),
		URL_ENCODING);
    }

    @Override
    public Representation represent(Variant variant) throws ResourceException {
        try {
            if (_tier.equals("categories")) {
                if (StringUtils.isBlank(_category)) {
                    return new StringRepresentation(_service.getCategoriesString());
                } else {
                    return new StringRepresentation(_service.getAssessmentsString(_category));
                }
            } else if (_tier.equals("attributes")) {
            	final List<Attribute> attributes;
                if (StringUtils.isBlank(_category)) {
                    attributes = _service.getAttributes();
                } else if (StringUtils.isBlank(_assessment)) {
		            // attributes = _service.getAssessmentsString(_category);
                    throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "It doesn't really make sense to get attributes with a category but no assessment.");
                } else if (StringUtils.isBlank(_attribute)) {
                    attributes =_service.getAttributes(_category, _assessment);
                } else if (StringUtils.isBlank(_value)) {
                    attributes = new ArrayList<Attribute>();
                    attributes.add(_service.getAttribute(_category, _assessment, _attribute));
                } else {
                    return new StringRepresentation(_service.validate(_category, _assessment, _attribute, _value));
                }
                return attributeRepresentation(attributes);
            } else if (_tier.equals("csvs")) {

                final List<Csv> csvs;
                if (StringUtils.isBlank(_category)) {
                    csvs = _service.getCsvs();
                } else if (StringUtils.isBlank(_assessment)) {
                    csvs = _service.getCsvs(_category);
                } else{
                    csvs =_service.getCsvs(_category, Integer.parseInt(_assessment));
                }
                return csvRepresentation(csvs);

            } else {
                throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Unknown attribute tier: " + _tier);
            }
        } catch (IOException exception) {
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, exception);
        }
    }

    private Representation csvRepresentation(final List<Csv> csvs) throws IOException {
        if(getRequestedMediaType() == MediaType.TEXT_HTML) {
            return csvHtmlRepresentation(csvs);
        }
        else {
            return csvJsonRepresentation(csvs);
        }
    }

    private Representation csvJsonRepresentation (final List<Csv> csvs) throws IOException {
        String json;
        json = _service.csvToJson(csvs);
        Representation r = new StringRepresentation(json);
        r.setMediaType(MediaType.APPLICATION_JSON);
        return r;
    }

    private Representation csvHtmlRepresentation(final List<Csv> csvs) {
        try {
            final VelocityContext context = new VelocityContext();
            context.put("attributes", csvs);
            context.put("StringUtils", new StringUtils());
            final String html = AdminUtils.populateVmTemplate(context, "/screens/DataDictionary.vm");
            final Representation r = new StringRepresentation(html);
            r.setMediaType(MediaType.TEXT_HTML);
            return r;
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Representation attributeRepresentation(final List<Attribute> attributes) throws IOException {
	if(getRequestedMediaType() == MediaType.TEXT_HTML) {
	    return htmlRepresentation(attributes);
	}
	else {
	    return jsonRepresentation(attributes);
	}
    }
    
    private Representation jsonRepresentation(final List<Attribute> attributes) throws IOException {
	String json;
	if(attributes != null && attributes.size() == 1) {
	    json = _service.toJson(attributes.get(0));
	}
	else {
	    json = _service.toJson(attributes);
	}
	Representation r = new StringRepresentation(json);
	r.setMediaType(MediaType.APPLICATION_JSON);
	return r;
    }

    private Representation htmlRepresentation(final List<Attribute> attributes) {
	try {
	    final VelocityContext context = new VelocityContext();
	    context.put("category", _category);
	    context.put("attributes", attributes);
	    context.put("StringUtils", new StringUtils());
	    final String html = AdminUtils.populateVmTemplate(context, "/screens/DataDictionary.vm");
	    final Representation r = new StringRepresentation(html);
	    r.setMediaType(MediaType.TEXT_HTML);
	    return r;
	}
	catch(Exception e) {
	    throw new RuntimeException(e);
	}
    }

    private final DataDictionaryService _service;
    private final String _tier;
    private final String _category;
    private final String _assessment;
    private final String _attribute;
    private final String _value;
}
