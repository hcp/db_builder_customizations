//Copyright 2012 Radiologics, Inc
//Author: Tim Olsen <tim@radiologics.com>
package org.nrg.xnat.restlet.extensions;

import com.amazonaws.util.json.JSONObject;
import com.google.common.collect.Maps;
import com.noelios.restlet.ext.servlet.ServletCall;


import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.turbine.util.TurbineException;
//import org.json.JSONObject;
//import net.sf.json.JSONObject;
import org.nrg.action.ActionException;
import org.nrg.xdat.presentation.PresentationA;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.security.XdatStoredSearch;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.MaterializedView;
import org.nrg.xft.db.MaterializedViewI;
import org.nrg.xft.db.PoolDBUtils;
import org.nrg.xft.db.views.H2MetaTable;
import org.nrg.xft.db.views.service.MaterializedViewServiceI;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.StringUtils;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.presentation.CSVPresenter;
import org.nrg.xnat.restlet.presentation.RESTHTMLPresenter;
import org.nrg.xnat.restlet.representations.StandardTurbineScreen;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;

import java.sql.SQLException;
import java.util.*;

/**
 * @author tim@radiologics.com
 *
 * Restlet used to filter and group cached search results.
 */
@XnatRestlet("/services/search/filter/{CACHED_SEARCH_ID}")
public class StoredSearchFilteredRestlet  extends SecureResource {
	static org.apache.log4j.Logger logger = Logger.getLogger(StoredSearchFilteredRestlet.class);
	String tableName=null;
	String columnName=null;
    int tier=0;
	
	Integer offset=null;
	Integer rowsPerPage=null;
	String sortBy=null;
	String sortOrder="ASC";
	
	
	/**
	 * @param context standard
	 * @param request standard
	 * @param response standard
	 */
	public StoredSearchFilteredRestlet(Context context, Request request, Response response) {
		super(context, request, response);
			
		tableName=(String)getParameter(request,"CACHED_SEARCH_ID");
        Integer restrictedQueryVariable=(containsQueryVariable("restricted"))?Integer.parseInt(getQueryVariable("restricted")):null;
        if(restrictedQueryVariable!=null){
            this.tier=restrictedQueryVariable;
        }

		if(PoolDBUtils.HackCheck(tableName)){
		    AdminUtils.sendAdminEmail(user,"Possible SQL Injection Attempt", "SORT BY:" + sortOrder);
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
			return;
		}
		
		columnName=(String)getQueryVariable("columns");
		
		if (this.getQueryVariable("offset")!=null){
			try {
				offset=Integer.valueOf(this.getQueryVariable("offset"));
			} catch (NumberFormatException e) {
				response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
				return;
			}
		}
		
		if (this.getQueryVariable("limit")!=null){
			try {
				rowsPerPage=Integer.valueOf(this.getQueryVariable("limit"));
			} catch (NumberFormatException e) {
				response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
				return;
			}
		}
		
		if (this.getQueryVariable("sortBy")!=null){
			sortBy=this.getQueryVariable("sortBy");
			if(PoolDBUtils.HackCheck(sortBy)){
		     AdminUtils.sendAdminEmail(user,"Possible SQL Injection Attempt", "SORT BY:" + sortOrder);
				response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
				return;
			}
			sortBy=StringUtils.ReplaceStr(sortBy, " ", "");
		}
		
		if (this.getQueryVariable("sortOrder")!=null){
			sortOrder=this.getQueryVariable("sortOrder");
			if(PoolDBUtils.HackCheck(sortOrder)){
		     AdminUtils.sendAdminEmail(user,"Possible SQL Injection Attempt", "SORT ORDER:" + sortOrder);
				response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
				return;
			}
			sortOrder=StringUtils.ReplaceStr(sortOrder, " ", "");
		}
					
		this.getVariants().add(new Variant(MediaType.APPLICATION_JSON));
		this.getVariants().add(new Variant(MediaType.TEXT_HTML));
		this.getVariants().add(new Variant(MediaType.TEXT_XML));
	}



	/**
	 * @param table name
	 * @param user object
	 * @return materialized view with that table name
	 * @throws Exception from database interactoin
	 */
	private MaterializedViewI getView(String table, UserI user) throws Exception{
        final String handler = getQueryVariable(MaterializedView.CACHING_HANDLER,MaterializedView.DEFAULT_MATERIALIZED_VIEW_SERVICE_CODE);
        MaterializedViewI mv= MaterializedView.getViewManagementService(handler).getViewByTablename(tableName, user);
		if(mv!=null){
			return mv;
		}
		
		mv=MaterializedView.getViewBySearchID(tableName, user, handler);

		if(mv!=null){
			return mv;
		}
		
		if(tableName.startsWith("@")){
			//special handling
			//build default listing
			String dv = "listing";
			DisplaySearch ds = new DisplaySearch();
			ds.setUser(user);
			ds.setDisplay(dv);
			ds.setRootElement(tableName.substring(1));
			XdatStoredSearch search=ds.convertToStoredSearch(tableName);
			search.setId(tableName);
			
			ds.setPagingOn(false);
			
			String query = ds.getSQLQuery(null);
			query = StringUtils.ReplaceStr(query,"'","*'*");
			query = StringUtils.ReplaceStr(query,"*'*","''");

            MaterializedViewServiceI mvService = MaterializedView.getViewManagementService(handler);

			mv = mvService.createView(user);
			if(search.getId()!=null && !search.getId().equals("")){
				mv.setSearch_id(search.getId());
			}
			mv.setSearch_sql(query);
			mv.setSearch_xml(search.getItem().writeToFlatString(0));
			MaterializedView.save(mv,MaterializedView.DEFAULT_MATERIALIZED_VIEW_SERVICE_CODE);
			
			return mv;
		}else{
			return null;
		}
	}

    @Override
    public boolean allowDelete() {
        return true;
    }

    @Override
    public void removeRepresentations() throws ResourceException {
        final String handler = getQueryVariable(MaterializedView.CACHING_HANDLER,MaterializedView.DEFAULT_MATERIALIZED_VIEW_SERVICE_CODE);
        try{
            MaterializedViewI mv= MaterializedView.getViewManagementService(handler).getViewByTablename(tableName, user);
            H2MetaTable.deleteTable(mv,tableName);
        }
        catch (Exception e) {
            logger.error("", e);
        }
    }

    @Override
    public boolean allowPost() {
        return true;
    }


    /* (non-Javadoc)
	 * @see org.restlet.resource.Resource#postRepresentation(org.restlet.resource.Variant)
	 */
    @SuppressWarnings("rawtypes")
	@Override
    public void handlePost() {
        Map<String, String> bodyVariables = getBodyVariableMap();

        final String handler = getQueryVariable(MaterializedView.CACHING_HANDLER,MaterializedView.DEFAULT_MATERIALIZED_VIEW_SERVICE_CODE);
        MediaType mt = overrideVariant(null);
        Hashtable<String,Object> params=new Hashtable<String,Object>();
        if(tableName!=null){
            params.put("ID", tableName);
        }
        if(columnName!=null){
            params.put("columns", columnName);
        }

        MaterializedViewI mv = null;
        XFTTable table=null;

        try {
            // We need the full table representation of the MV to build the subject list used for downloads and CSVs
            // Arbitrarily using 5000 as an upper limit for rowsPerPage
            mv = getView(tableName, user);
            XFTTable t = mv.getData(null, 0, 5000, buildWhere(mv));

            createSubjectsListForDownloader(t);

            if(mv.getUser_name().equals(user.getLogin()) || handler.equals("filter")){
                if(columnName!=null){
                    List<String> columns=StringUtils.CommaDelimitedStringToArrayList(columnName);
                    List<String> all_columns=mv.getColumnNames();
                    List<String> badColumns = new ArrayList<String>();
                    for(String column:columns){
                        if(!all_columns.contains(column)){
                            badColumns.add(column);
                        }
                    }
                    if (badColumns.size() > 0) {
                        throw new Exception("Invalid column in request: " + org.apache.commons.lang.StringUtils.join(badColumns, ", "));
                    }

                    table=mv.getColumnsValues(columnName,buildWhere(mv));
                }else{
                    table=mv.getData((sortBy!=null)?sortBy + " " + sortOrder:null, offset, rowsPerPage, buildWhere(mv));

                    params.put("totalRecords", mv.getSize(buildWhere(mv)));

                    if (mt!=null && (mt.equals(SecureResource.APPLICATION_XLIST))){
                        table=formatTable((new RESTHTMLPresenter(TurbineUtils.GetRelativePath(ServletCall.getRequest(this.getRequest())),null,user,sortBy,this.tier)),mv,table, user);
                    }else if (mt!=null && (mt.equals(MediaType.APPLICATION_EXCEL) || mt.equals(SecureResource.TEXT_CSV)) && !this.isQueryVariableTrue("includeHiddenFields")){
                        Integer restrictedQueryVariable=(containsQueryVariable("restricted"))?Integer.parseInt(getQueryVariable("restricted")):null;
                        String prefix = "";
                        if(restrictedQueryVariable==null){
                            table=formatTable(new CSVPresenter(),mv,table, user);
                        }
                        else if(restrictedQueryVariable>0){
                            table=formatTable(new CSVPresenter("true"),mv,table, user);
                            prefix="RESTRICTED_";
                        }
                        else{
                            table=formatTable(new CSVPresenter("false"),mv,table, user);
                            prefix="unrestricted_";
                        }
                        // Let's default to removing commas here because that's the currently desired Connectome behavior.
                        if(!this.isQueryVariableFalse("removeDelimitersFromFieldValues")){
                            while (table.hasMoreRows()) {
                                Object[] cols = table.nextRow();
                                for (int i=0;i<cols.length;i++) {
                                    if (cols[i] instanceof String && ((String)cols[i]).indexOf(',')>=0) {
                                        cols[i]=((String)cols[i]).replaceAll(", ?"," ");
                                    }
                                }
                            }
                        }
                        //copied from CSVScreen
                        java.util.Date today = java.util.Calendar.getInstance(java.util.TimeZone.getDefault()).getTime();
                        String fileName=prefix+user.getUsername() + "_" + (today.getMonth() + 1) + "_" + today.getDate() + "_" + (today.getYear() + 1900) + "_" + today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds() + ".csv";
                        this.setContentDisposition(fileName, false);
                    }else if (mt!=null && (mt.equals(MediaType.APPLICATION_JSON))){
                    	final XFTTable junkTable =  mv.getColumnsValues("subject_label",buildWhere(mv));
                    	final ArrayList subjectList = junkTable.convertColumnToArrayList("subject_label");
                    	params.put("subjectList", org.apache.commons.lang.StringUtils.join(subjectList, ','));
                    }
                }
            }
        } catch (ActionException e) {
            this.getResponse().setStatus(e.getStatus());
            table = new XFTTable();
        } catch (SQLException e) {
            logger.error("",e);
            this.getResponse().setStatus(Status.CLIENT_ERROR_GONE);
            table = new XFTTable();
        } catch (Exception e) {
            logger.error("",e);
            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
            table = new XFTTable();
        }


        try {
            this.getResponse().setEntity(representTable(table,mt,params));
        } catch (Exception e) {
            logger.error("",e);
            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
        }
    }

    /* (non-Javadoc)
	 * @see org.restlet.resource.Resource#getRepresentation(org.restlet.resource.Variant)
	 */
    @Override
	public Representation getRepresentation(Variant variant) {
		final String handler = getQueryVariable(MaterializedView.CACHING_HANDLER,MaterializedView.DEFAULT_MATERIALIZED_VIEW_SERVICE_CODE);
		MediaType mt = overrideVariant(variant);
		
		Hashtable<String,Object> params=new Hashtable<String,Object>();
		if(tableName!=null){
			params.put("ID", tableName);
		}
		if(columnName!=null){
			params.put("columns", columnName);
		}
		
		XFTTable table=null;
		
		try {
			// We need the full table representation of the MV to build the subject list used for downloads and CSVs
			// Arbitrarily using 5000 as an upper limit for rowsPerPage
			MaterializedViewI mv = getView(tableName, user);
			XFTTable t = mv.getData(null, 0, 5000, buildWhere(mv));

			createSubjectsListForDownloader(t);

			if(mv.getUser_name().equals(user.getLogin()) || handler.equals("filter")){
				if(columnName!=null){
                    List<String> columns=StringUtils.CommaDelimitedStringToArrayList(columnName);
                    List<String> all_columns=mv.getColumnNames();
                    List<String> badColumns = new ArrayList<String>();
                    for(String column:columns){
                        if(!all_columns.contains(column)){
                            badColumns.add(column);
                        }
                    }
                    if (badColumns.size() > 0) {
                        throw new Exception("Invalid column in request: " + org.apache.commons.lang.StringUtils.join(badColumns, ", "));
                    }
					
					table=mv.getColumnsValues(columnName,buildWhere(mv));
				}else{
					table=mv.getData((sortBy!=null)?sortBy + " " + sortOrder:null, offset, rowsPerPage, buildWhere(mv));

					params.put("totalRecords", mv.getSize(buildWhere(mv)));
					
					if (mt!=null && (mt.equals(SecureResource.APPLICATION_XLIST))){
						table=formatTable((new RESTHTMLPresenter(TurbineUtils.GetRelativePath(ServletCall.getRequest(this.getRequest())),null,user,sortBy,this.tier)),mv,table, user);
				    }else if (mt!=null && (mt.equals(MediaType.APPLICATION_EXCEL) || mt.equals(SecureResource.TEXT_CSV)) && !this.isQueryVariableTrue("includeHiddenFields")){
                        Integer restrictedQueryVariable=(containsQueryVariable("restricted"))?Integer.parseInt(getQueryVariable("restricted")):null;
                        String prefix = "";
                        if(restrictedQueryVariable==null){
                            table=formatTable(new CSVPresenter(),mv,table, user);
                        }
                        else if(restrictedQueryVariable>0){
                            table=formatTable(new CSVPresenter("true"),mv,table, user);
                            prefix="RESTRICTED_";
                        }
                        else{
                            table=formatTable(new CSVPresenter("false"),mv,table, user);
                            prefix="unrestricted_";
                        }
                        // Let's default to removing commas here because that's the currently desired Connectome behavior.
                        if(!this.isQueryVariableFalse("removeDelimitersFromFieldValues")){
                        	while (table.hasMoreRows()) {
                                    Object[] cols = table.nextRow();
                                    for (int i=0;i<cols.length;i++) {
                                            if (cols[i] instanceof String && ((String)cols[i]).indexOf(',')>=0) {
                                                    cols[i]=((String)cols[i]).replaceAll(", ?"," ");
                                            }
                                    }
                            }
                        }
                        //copied from CSVScreen
                        java.util.Date today = java.util.Calendar.getInstance(java.util.TimeZone.getDefault()).getTime();
                        String fileName=prefix+user.getUsername() + "_" + (today.getMonth() + 1) + "_" + today.getDate() + "_" + (today.getYear() + 1900) + "_" + today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds() + ".csv";
                        this.setContentDisposition(fileName, false);
				    }
				}
			}
		} catch (ActionException e) {
			this.getResponse().setStatus(e.getStatus());
			table = new XFTTable();
		} catch (SQLException e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.CLIENT_ERROR_GONE);
			table = new XFTTable();
		} catch (Exception e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			table = new XFTTable();
		}

		
		try {
			if(hasQueryVariable("requested_screen")){
				String tag="t_"+Calendar.getInstance().getTimeInMillis();
				this.getHttpSession().setAttribute(tag, table);
				params.put("hideTopBar",isQueryVariableTrue("hideTopBar"));
				params.put("table_tag", tag);
				return new StandardTurbineScreen(MediaType.TEXT_HTML, getRequest(), user, getQueryVariable("requested_screen"), params);
			}else{
				return representTable(table,mt,params);
			}
		} catch (TurbineException e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			return null;
		}
	}

	private void createSubjectsListForDownloader(XFTTable t) {
		// We don't want to build the list from the MR Sessions tab
		if (! t.getColumns()[0].equalsIgnoreCase("subject_label")) {
			return;
		}
		final List<String> subjects = new ArrayList<>();

		while(t.hasMoreRows()) {
			final String subject = t.nextRow()[0].toString();
			subjects.add(subject);
		}

		String subjects_str = org.apache.commons.lang.StringUtils.join(subjects, ",");
		if(!subjects_str.isEmpty()) {
			getHttpSession().setAttribute("downloadSubjects", subjects_str);
		}
	}


	private static XFTTable formatTable(PresentationA presenter, MaterializedViewI mv, XFTTable table, XDATUser user) throws Exception{
		DisplaySearch ds = mv.getDisplaySearch(user);
		ds.getSQLQuery(presenter);
    	
    	presenter.setRootElement(ds.getRootElement());
		presenter.setDisplay(ds.getDisplay());
		presenter.setAdditionalViews(ds.getAdditionalViews());
		return (XFTTable)presenter.formatTable(table,ds,false);
	}
	
	/**
	 * @param mv materialized view
	 * @return map representing the where clause
	 * @throws Exception from MaterializedViewForFilter.getColumnNames()
	 */
	public Map<String,Object> buildWhere(MaterializedViewI mv) throws Exception{
		Map<String,Object> map=Maps.newHashMap();
		
		List<String> columns=mv.getColumnNames();

		for(Map.Entry<String,Object> entry:this.getQueryVariablesAsMap().entrySet()){
            String upperKey = entry.getKey().toUpperCase();
            boolean hasIt = columns.contains(upperKey);
			if(hasIt){
				map.put(entry.getKey().toUpperCase(), entry.getValue());
			}
		}
		
		for(Map.Entry<String,String> entry:this.getBodyVariableMap().entrySet()){
            String upperKey = entry.getKey().toUpperCase();
            boolean hasIt = columns.contains(upperKey);
            if(hasIt){
                map.put(entry.getKey().toUpperCase(), StringEscapeUtils.unescapeXml(entry.getValue()));
            }

		}
		
		return map;
	}
}
