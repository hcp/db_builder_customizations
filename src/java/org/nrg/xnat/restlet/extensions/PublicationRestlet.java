package org.nrg.xnat.restlet.extensions;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xnat.entities.Publication;
import org.nrg.xnat.entities.datadictionary.Attribute;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.nrg.xnat.services.publication.PublicationService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

@XnatRestlet({"/services/publication"})
public class PublicationRestlet extends SecureResource {
    private final PublicationService requests = XDAT.getContextService().getBean(PublicationService.class);
    static Logger logger = Logger.getLogger(PublicationRestlet.class);

    public PublicationRestlet(Context context, Request request, Response response) throws Exception {
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));
    }

    @Override public boolean allowDelete() { return false; }
    @Override public boolean allowPut()    { return false; }
    @Override public boolean allowGet()    { return true; }
    @Override public boolean allowPost()   { return false;  }

    @Override public void handleGet(){
        try {
            List<Publication> publications = null;
            String project = SecureResource.getQueryVariable("project", getRequest());
            if (!StringUtils.isBlank(project)) {
                publications = requests.getPublicationsForProject(project);
            }
            this.getResponse().setEntity(jsonRepresentation(publications));
        } catch (Throwable exception) {
            logger.error("Failed to get publication.", exception);
        }
    }
    
    private Representation jsonRepresentation(final List<Publication> publications) throws IOException {
        String json = requests.toJson(publications);
        Representation r = new StringRepresentation(json);
        r.setMediaType(MediaType.APPLICATION_JSON);
        return r;
    }
}
