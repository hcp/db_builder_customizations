/**
 * Copyright 2011 Washington University
 */
package org.nrg.xnat.restlet.extensions;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.hcp.security.HcpLdapHelper;
import org.nrg.hcp.security.HcpLdapHelper.DataUseException;
import org.nrg.hcp.security.HcpLdapHelper.DUTStatus;
import org.nrg.hcp.security.HcpLdapHelper.HcpLdapException;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.security.XDATUser.UserNotFoundException;
import org.nrg.xft.XFTTable;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.Variant;

import javax.servlet.http.HttpSession;

/**
 * Checks acceptance of data use terms
 * @author Michael Hodge <hodgem@mir.wustl.edu>
 *
 */
@XnatRestlet("/services/datause")
public final class DataUseTermsResource extends SecureResource {

	//public static final String USER_KEY = "user";
	//public static final String GROUP_KEY = "group";
	//public static final String PROJECT_KEY = "project";

	static org.apache.log4j.Logger logger = Logger
			.getLogger(DataUseTermsResource.class);

	public static final String TERMS_KEY = "terms";
	public static final String ACCEPT_KEY = "acceptTerms";
	public static final String USER_KEY = "username";

	public DataUseTermsResource(final Context context, final Request request, final Response response) {
		super(context, request, response);
	}

	@Override
	public boolean allowGet() { return true; }

	@Override
	public boolean allowPost() { return true; }

	@Override
	public boolean allowPut() { return true; }

	@Override
	public boolean allowDelete() { return true; }

	@Override
	public void handleGet() {
		String termsP = this.getQueryVariable(TERMS_KEY);
		String usernameP = this.getQueryVariable(USER_KEY);

		// If no terms provided, return user's full access table
		if (termsP == null) {
			Variant var = new Variant(MediaType.APPLICATION_JSON);
			this.returnRepresentation(this.getRepresentation(var), Status.SUCCESS_OK);
		}

		if (usernameP != null && termsP != null) {
			if (! (user.isSiteAdmin() || isWhitelisted())) {
				logger.info(user.getLogin() + " does not have permission to check other user's Data Use Terms.");
				this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN, "You do not have permission to check other user's Data Use Terms.");
				return;
			}
			// Check terms for differnt user than the one authenticated
			try {
				XDATUser requestedUser = new XDATUser(usernameP);
				this.checkDataUse(requestedUser, termsP);
			} catch (UserNotFoundException e) {
				logger.info(e);
				this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND, "Username " +  '"' + usernameP + '"' + " not found.");
			} catch (Exception e) {
				logger.error(e);
			}
		} else {
			// Check terms for authenticated user
			this.checkDataUse(user, termsP);
		}
	}

	private void checkDataUse(XDATUser user, String termsP) {
		HashMap<String,DUTStatus> dutMap;
		if (getHttpSession().getAttribute("DUTMap")!=null) {
			dutMap=(HashMap<String, DUTStatus>)getHttpSession().getAttribute("DUTMap");

		} else {
			dutMap=new HashMap<String,DUTStatus>();
			getHttpSession().setAttribute("DUTMap", dutMap);
		}
		try {
			if (dutMap.containsKey(termsP)) {
				this.returnString(String.valueOf(dutMap.get(termsP)),Status.SUCCESS_OK);
			} else {
				DUTStatus dutStatus = HcpLdapHelper.AcceptedDataUse(user,termsP);
				logger.debug("DUT Acceptance -- termsP="  + termsP + "  --  dutStatus=" + dutStatus + " -- user=" + user.getLogin());
				if (!dutStatus.equals(DUTStatus.MOD)) {
					dutMap.put(termsP, dutStatus);
				}
				this.returnString(String.valueOf(dutStatus),Status.SUCCESS_OK);
			}
		} catch (DataUseException e) {
			this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Processing exception likely due to invalid value for \"terms\".");
		} catch (HcpLdapException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void handlePost() {
		String termsP = this.getQueryVariable(TERMS_KEY);
		String acceptP = this.getQueryVariable(ACCEPT_KEY);

		// Only allow adding groups via REST for Open Access terms
		String accessConfig = XDAT.getConfigService().getConfigContents("ldap", "access.json");
		JSONArray accessList = null;
		Boolean allowDataUseAcceptance = false;

		try {
			JSONObject obj = new JSONObject(accessConfig);
			accessList = obj.getJSONArray("accessList");
		} catch (JSONException e) {
			logger.error(e);
			return;
		}

		// Go though access rules and grant access if the terms exists and are open
		for (int i = 0; i < accessList.length(); i++) {
			String terms = null;
			Boolean openAccess = false;
			try {
				terms = accessList.getJSONObject(i).getString("name");
				openAccess = accessList.getJSONObject(i).getBoolean("openAccess");
			} catch (JSONException e) {
				logger.error(e);
				return;
			}

			if (termsP.equalsIgnoreCase(terms) && openAccess) {
				allowDataUseAcceptance = true;
				break;
			}
		}

		if (! allowDataUseAcceptance) {
			logger.warn("Attempt to add to RESTRICTED group via REST for terms " + termsP);
			this.getResponse().setStatus(Status.CLIENT_ERROR_UNAUTHORIZED, "You are not authorized to grant access for the specified \"terms\".");
			return;
		}

		HashMap<String,DUTStatus> dutMap;
		if (getHttpSession().getAttribute("DUTMap")!=null) {
			dutMap=(HashMap<String, DUTStatus>)getHttpSession().getAttribute("DUTMap");
		} else {
			dutMap=new HashMap<String,DUTStatus>();
			getHttpSession().setAttribute("DUTMap", dutMap);
		}
		// In the unlikely case users were to make direct REST calls, use acceptTerms to require acknowledgment of acceptance
		if (acceptP==null || !acceptP.equalsIgnoreCase("true")) {
			this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_ACCEPTABLE,"PUT/POST requests must pass " + ACCEPT_KEY + " parameter (" + ACCEPT_KEY + "=true).");
			return;
		}
		try {
			dutMap.put(termsP, DUTStatus.TRUE);
			this.returnString(String.valueOf(HcpLdapHelper.RecordDataUseAcceptance(user,termsP)),Status.SUCCESS_OK);
		} catch (DataUseException e) {
			this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Processing exception likely due to invalid value for \"terms\".");
		} catch (HcpLdapException e) {
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Server-side Active Directory Error");
			e.printStackTrace();
		}

		// Update the tiersMap in the http session to reflect new terms
		HcpLdapHelper.updateUserTiersMap(getHttpSession());
	}

	@Override
	public void handlePut() {
		handlePost();
	}

	@Override
	public synchronized void handleDelete() {
		// Note:  Method needs to be synchronized so all calls return same (non-null) dutMap session instance once first instance is created
		String termsP = this.getQueryVariable(TERMS_KEY);
		HashMap<String,DUTStatus> dutMap;
		if (getHttpSession().getAttribute("DUTMap")!=null) {
			dutMap=(HashMap<String, DUTStatus>)getHttpSession().getAttribute("DUTMap");
		} else {
			dutMap=new HashMap<String,DUTStatus>();
			getHttpSession().setAttribute("DUTMap", dutMap);
		}
		try {
			dutMap.put(termsP, DUTStatus.FALSE);
			this.returnString(String.valueOf(HcpLdapHelper.RemoveDataUseAcceptance(user,termsP)),Status.SUCCESS_OK);
		} catch (DataUseException e) {
			this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Processing exception likely due to invalid value for \"terms\".");
		} catch (HcpLdapException e) {
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Server-side Active Directory Error");
			e.printStackTrace();
		}
	}

	@Override
	public Representation getRepresentation(Variant variant) {

		XFTTable tiersTable = new XFTTable();
		tiersTable.initTable(new String[]{"group_id"});
		Hashtable<String, Object> params = new Hashtable<String, Object>();
		Map<String, Object> tiers;

		try {
			HcpLdapHelper ldap = HcpLdapHelper.getInstance();
			tiers = ldap.getUserTiers(user);
		}
		catch (HcpLdapException e) {
			logger.error(e);
			return null;
		}

		// TODO: come up with service representation
		for (String key : tiers.keySet()) {
			tiersTable.insertRow(new Object[]{key});
		}

		if(tiersTable != null) {
			params.put("totalRecords", tiersTable.size());
		}
		params.put("title", "Data Use");
		return this.representTable(tiersTable, overrideVariant(variant), params);
	}
}