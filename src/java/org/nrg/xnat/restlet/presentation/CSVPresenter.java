//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * XDAT � Extensible Data Archive Toolkit
 * Copyright (C) 2005 Washington University
 */
/*
 * Created on Jan 5, 2005
 *
 */
package org.nrg.xnat.restlet.presentation;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.collections.DisplayFieldCollection;
import org.nrg.xdat.display.DisplayFieldReferenceI;
import org.nrg.xdat.display.DisplayManager;
import org.nrg.xdat.display.ElementDisplay;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xft.XFTTable;
import org.nrg.xft.XFTTableI;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.schema.design.SchemaElementI;
import org.nrg.xft.utils.StringUtils;

import org.nrg.xdat.presentation.PresentationA;
import org.nrg.xnat.entities.datadictionary.Attribute;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;

/**
 * @author Tim
 *

 */
public class CSVPresenter extends PresentationA {
    static org.apache.log4j.Logger logger = Logger.getLogger(CSVPresenter.class);
    private String restricted;

    public CSVPresenter() {
        this.restricted = null;
    }

    public CSVPresenter(String restricted) {
        this.restricted = restricted;
    }

    public String getVersionExtension(){return "csv";}

    public XFTTableI formatTable(XFTTableI table,DisplaySearch search) throws XFTInitException,ElementNotFoundException
    {
        return formatTable(table,search,true);
    }

    public XFTTableI formatTable(XFTTableI table,DisplaySearch search,boolean allowDiffs) throws XFTInitException,ElementNotFoundException
    {
        logger.debug("BEGIN CVS FORMAT");
        XFTTable csv = new XFTTable();
        ElementDisplay ed = DisplayManager.GetElementDisplay(getRootElement().getFullXMLName());
        ArrayList visibleFields = this.getVisibleFields(ed,search);

        //int fieldCount = visibleFields.size() + search.getInClauses().size();

        ArrayList columnHeaders = new ArrayList();
        ArrayList diffs = new ArrayList();

        if (search.getInClauses().size()>0)
        {
            for(int i=0;i<search.getInClauses().size();i++)
            {
                columnHeaders.add("");
            }
        }

        DataDictionaryService dictService = XDAT.getContextService().getBean(DataDictionaryService.class);

        //POPULATE HEADERS

        Iterator fields = visibleFields.iterator();
        int counter = search.getInClauses().size();
        while (fields.hasNext())
        {
            DisplayFieldReferenceI df = (DisplayFieldReferenceI)fields.next();
            if (allowDiffs)
            {
                if (!diffs.contains(df.getElementName()))
                {
                    diffs.add(df.getElementName());
                    SchemaElementI foreign = SchemaElement.GetElement(df.getElementName());
                    if (search.isMultipleRelationship(foreign))
                    {
                        String temp = StringUtils.SQLMaxCharsAbbr(search.getRootElement().getSQLName() + "_" + foreign.getSQLName() + "_DIFF");
                        Integer index = ((XFTTable)table).getColumnIndex(temp);
                        if (index!=null)
                        {
                            columnHeaders.add("Diff");
                        }
                    }
                }
            }

            if (!df.isHtmlContent())
            {
                String value = df.getHeader();
                if ("ID".equals(value)){
                    // Workaround for MS SYLK issue:
                    // http://support.microsoft.com/kb/215591
                    // http://nrg.wustl.edu/fogbugz/default.php?418
                    value = value.toLowerCase();
                }
                try{
                    Attribute currAttribute = dictService.getAttributeByFieldId(df.getElementName(), df.getDisplayField().getId());

                    if(this.restricted==null){//If all columns are desired
                        columnHeaders.add(value);
                    }
                    else if(this.restricted.equals("true") && (currAttribute.getRestricted()||(currAttribute.getFieldId().equals("SUBJECT_LABEL"))) ){//If only restricted columns are desired and this is a restricted column
                        columnHeaders.add(value);
                    }
                    else if(this.restricted.equals("false") && !currAttribute.getRestricted()){//If only non-restricted columns are desired and this is a non-restricted column
                        columnHeaders.add(value);
                    }
                }catch (DisplayFieldCollection.DisplayFieldNotFoundException e) {
                    logger.error("",e);
                }
            }
        }
        csv.initTable(columnHeaders);

        //POPULATE DATA
        table.resetRowCursor();

        while (table.hasMoreRows())
        {
            Hashtable row = table.nextRowHash();
            Object[] newRow = new Object[columnHeaders.size()];
            fields = visibleFields.iterator();

            diffs = new ArrayList();
            if (search.getInClauses().size()>0)
            {
                for(int i=0;i<search.getInClauses().size();i++)
                {
                    Object v = row.get("search_field"+i);
                    if (v!=null)
                    {
                        newRow[i] = v;
                    }else{
                        newRow[i] = "";
                    }
                }
            }

            counter = search.getInClauses().size();
            while (fields.hasNext())
            {
                DisplayFieldReferenceI dfr = (DisplayFieldReferenceI)fields.next();
                if(!dfr.isHtmlContent())
                {

                    try {
                        if (allowDiffs)
                        {
                            if (!diffs.contains(dfr.getElementName()))
                            {
                                diffs.add(dfr.getElementName());
                                SchemaElementI foreign = SchemaElement.GetElement(dfr.getElementName());
                                if (search.isMultipleRelationship(foreign))
                                {
                                    String temp = StringUtils.SQLMaxCharsAbbr(search.getRootElement().getSQLName() + "_" + foreign.getSQLName() + "_DIFF");
                                    Integer index = ((XFTTable)table).getColumnIndex(temp);
                                    if (index!=null)
                                    {
                                        String diff = "";
                                        Object d = row.get(temp.toLowerCase());
                                        if (d!=null)
                                        {
                                            diff=  (String)d.toString();
                                        }else{
                                            diff="";
                                        }
                                        newRow[counter++]=diff;
                                    }
                                }
                            }
                        }

                        Attribute currAttribute = dictService.getAttributeByFieldId(dfr.getElementName(), dfr.getDisplayField().getId());

                        if((this.restricted==null)||(this.restricted.equals("true") && (currAttribute.getRestricted()||(currAttribute.getFieldId().equals("SUBJECT_LABEL"))))||(this.restricted.equals("false") && !currAttribute.getRestricted())){
                            Object v = null;
                            if (dfr.getElementName().equalsIgnoreCase(search.getRootElement().getFullXMLName()))
                            {
                                v = row.get(dfr.getRowID().toLowerCase());
                            }else{
                                v = row.get(dfr.getElementSQLName().toLowerCase() + "_" + dfr.getRowID().toLowerCase());
                            }
                            if (v != null)
                            {
                                newRow[counter] = v;
                            }
                            counter++;
                        }
                    } catch (XFTInitException e) {
                        logger.error("",e);
                    } catch (ElementNotFoundException e) {
                        logger.error("",e);
                    } catch (DisplayFieldCollection.DisplayFieldNotFoundException e) {
                        logger.error("",e);
                    }
                }
            }
            csv.insertRow(newRow);
        }


        logger.debug("END CVS FORMAT");
        return csv;
    }
}


