/*
 * org.nrg.xnat.restlet.representations.XMLTableRepresentation
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 9:04 PM
 */
package org.nrg.xnat.restlet.representations;

import org.nrg.xft.XFTTable;
import org.restlet.data.MediaType;
import org.restlet.resource.OutputRepresentation;
import org.apache.log4j.Logger;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;
import java.util.Map;

public class PubMedXMLRepresentation extends OutputRepresentation {
    URL url = null;
    static Logger logger = Logger.getLogger(PubMedXMLRepresentation.class);

	public PubMedXMLRepresentation(URL url) {
		super(MediaType.APPLICATION_XML);
		this.url=url;
	}

	@Override
	public void write(OutputStream os) throws IOException {
        try {
            URLConnection urlc = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
            OutputStreamWriter sw = new OutputStreamWriter(os);
            BufferedWriter writer = new BufferedWriter(sw);
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                writer.write(inputLine);
            }
            in.close();
            writer.flush();
            }
        catch (Exception e) {
            logger.error("Error writing PubMed XML.", e);
        }
	    
	}

}
