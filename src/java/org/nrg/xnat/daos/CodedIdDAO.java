package org.nrg.xnat.daos;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xnat.entities.CodedId;
import org.springframework.stereotype.Repository;

/**
 * Created by mmckay01 on 1/29/14.
 */
@Repository
public class CodedIdDAO extends AbstractHibernateDAO<CodedId> {
}
