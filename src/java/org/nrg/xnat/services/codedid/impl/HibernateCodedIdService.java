package org.nrg.xnat.services.codedid.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnat.daos.CodedIdDAO;
import org.nrg.xnat.services.codedid.CodedIdService;
import org.nrg.xnat.entities.CodedId;
import org.springframework.stereotype.Service;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by mmckay01 on 1/29/14.
 */
@Service
public class HibernateCodedIdService extends AbstractHibernateEntityService<CodedId, CodedIdDAO> implements CodedIdService {
    @Override
    @Transactional
    public List<CodedId> getCodedIdSetForProject(String project) {
        CodedId example = new CodedId();
        example.setProject(project);
        return _dao.findByExample(example, EXCLUSION_PROPERTIES);
    }

    private static final String[] EXCLUSION_PROPERTIES = new String[] { "id", "created", "disabled", "enabled", "timestamp", "codedId", "subjectId" };

    private static final Log _log = LogFactory.getLog(HibernateCodedIdService.class);

    @Inject
    private CodedIdDAO _dao;

    @Inject
    private DataSource _datasource;
}
