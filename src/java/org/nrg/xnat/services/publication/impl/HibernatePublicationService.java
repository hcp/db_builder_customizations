package org.nrg.xnat.services.publication.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnat.daos.PublicationDAO;
import org.nrg.xnat.services.publication.PublicationService;
import org.nrg.xnat.entities.Publication;
import org.springframework.stereotype.Service;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by mmckay01 on 1/29/14.
 */
@Service
public class HibernatePublicationService extends AbstractHibernateEntityService<Publication, PublicationDAO> implements PublicationService {

    @Override
    @Transactional
    public List<Publication> getPublicationsForProject(String project) {
        Publication example = new Publication();
        example.setProject(project);
        return _dao.findByExample(example, EXCLUSION_PROPERTIES);
    }

    @Override
    @Transactional
    public List<Publication> getPublicationsByIdAndProject(String id, String project) {
        Publication example = new Publication();
        example.setProject(project);
        example.setPmid(id);
        return _dao.findByExample(example, EXCLUSION_PROPERTIES_FOR_TITLE);
    }

    @Override
    public String toJson(final List<Publication> publications) throws IOException{
        return _mapper.writeValueAsString(publications);
    }

    private static final String[] EXCLUSION_PROPERTIES = new String[] { "id", "created", "disabled", "enabled", "timestamp", "articleTitle", "authors", "publication", "date", "pmid", "doi" };
    private static final String[] EXCLUSION_PROPERTIES_FOR_TITLE = new String[] { "id", "created", "disabled", "enabled", "timestamp", "articleTitle", "authors", "publication", "date", "doi" };

    private static final Log _log = LogFactory.getLog(HibernatePublicationService.class);

    @Inject
    private PublicationDAO _dao;

    @Inject
    private DataSource _datasource;

    private final ObjectMapper _mapper = new ObjectMapper() {{
        getDeserializationConfig().set(DeserializationConfig.Feature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
        getDeserializationConfig().set(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        getDeserializationConfig().set(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        getDeserializationConfig().set(DeserializationConfig.Feature.WRAP_EXCEPTIONS, true);
        getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        getSerializationConfig().set(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
    }};
}
