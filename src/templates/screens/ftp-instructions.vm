<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Human Connectome Project</title>
<meta http-equiv="Description" content="How to download Public HCP data over FTP, using your free HCP account." />
<meta http-equiv="Keywords" content="Human Connectome Project, Public Data release, FTP download, FAQ" />
<link href="$content.getURI('/style/hcpv3.css')" rel="stylesheet" type="text/css" />
<link href="$content.getURI('/style/data.css')" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="$content.getURI('/scripts/global.js')"></script>
<script type="text/javascript" src="$content.getURI('/scripts/connectomedb-reg.js')"></script>
<script type="text/javascript" src="$content.getURI('/scripts/jquery.14.min.js')"></script> 
<script type="text/javascript" src="$content.getURI('/scripts/jquery.easing.1.2.js')"></script>

</head>

<body><!-- site container -->
<div id="site_container">

<div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
</div>

<!-- Header -->
<div id="header"><img src="$content.getURI('/images/img/header-bg.png')" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />
  <map name="Header" id="Header">
    <area shape="rect" coords="14,7,404,123" href="$content.getURI('/')" alt="Human Connectome Project Logo" />
  </map>
</div> 
<!-- end Header -->

<!-- top level navigation --><!-- #BeginLibraryItem "/Library/Top Nav.lbi" --><div id="topnav">
<ul id="nav">
	<li><a href="$content.getURI('/')">Home</a></li>
    <li><a href="/about/">About the Project</a>
        <ul>
          <li><a href="/about/project/">Project Overview &amp; Components</a></li>
            <li><a href="/about/hcp-investigators.html">HCP Investigators</a></li>
            <li><a href="/about/hcp-colleagues.html">HCP Colleagues</a></li>
            <li><a href="/about/teams.html">Operational Teams</a></li>
            <li><a href="/about/external-advisory-panel.html">External Advisory Panel</a></li>
            <li><a href="/about/publications.html">Publications</a></li>
            <li><a href="/about/pressroom/">News &amp; Updates</a></li>
        </ul>
    </li>
    <li><a href="/data/">Data</a></li>
	<li><a href="/documentation/">Documentation</a>
         <ul>
            <li><a href="/documentation/tutorials/">Tutorials</a></li>
        </ul>
     </li>
     <li><a href="/connectome/">Using the Connectome</a>
     	<ul>
        	<li><a href="/connectome/connectome-workbench.html">What is Connectome Workbench</a></li>
            <li><a href="/connectome/get-connectome-workbench.html">Get Connectome Workbench Beta</a></li>
        </ul>
      </li>
     <li><a href="/contact/">Contact Us</a>
     	<ul class="subnav">
        	<li><a href="/careers/">Career Opportunities</a></li>
        	<li> <a href="/contact/collaboration-request.php">Collaboration Request</a></li>
            <li> <a href="/contact/feature-request.php">Feature Request</a></li>
        </ul>
    </li>
	 <li style="float: right; margin-right:30px;"><a href="/collaborate/">Collaboration Extranet<img src="$content.getURI('/images/img/lock.png')" width="11" height="16" style="margin: 0 0 -1px 5px; border:none;"></a>
<ul class="subnav">
         <li><a href="http://wiki.humanconnectome.org/" target="_new">HCP Wiki</a></li>
        </ul>
    </li> 
  </ul>
</div><!-- #EndLibraryItem --><!-- End Nav -->

<!-- banner -->
<div id="bannerOpen" class="bannerContainer hidden">
  <span>
    <a style="background:url(/images/img/close.png) no-repeat 2px center #000;" onClick="closeBanner();" title="Hide banner">Close</a>
  </span>
  <iframe height="300" width="100%" frameborder="0" scrolling="no" src="http://humanconnectome.org/data/banner/"></iframe>
</div>
<div id="bannerCollapsed" class="bannerContainer">
  <span>
    <a style="background:url(/images/img/open.png) no-repeat 2px center #000;" onClick="openBanner();" title="Show banner">Open</a>
  </span>
  <iframe height="36" width="100%" frameborder="0" scrolling="no" src="/data/banner/collapsed/"></iframe>
</div>
<!-- end banner -->

<!-- Breadcrumbs -->
<div id="breadcrumbs">
  <p>&raquo; <a href="/data/">data</a> &gt; <a href="/data/faq/">frequently asked questions</a> &gt; FTP download instructions</p></div>
<!-- end breadcrumbs -->

<!-- "Middle" area contains all content between the top navigation and the footer -->
<div id="middle">
  <div id="content">
  
  
      <h1 style="margin-top:0;">FTP Access to Public HCP Data</h1>
      <div class="infoBox">
      	<p>Note: You must have an HCP Account and have accepted the Terms of Use for each data set.</p>
      </div>
      <h3>Your FTP Credentials</h3>
      <div style="background-color:#f0f0f0; padding:10px; border: 1px solid #ccc; margin-bottom: 1em;">
      <table style="background-color: #f0f0f0;" width="100%" cellpadding="6">
      	<tr bgcolor="#ccc" style="font-weight:bold">
        	<td>FTP Access:</td>
            <td>ftp.humanconnectome.org</td>
        </tr>
        <tr>
        	<td>Login:</td><td>Your registered HCP Username</td>
        </tr>
        <tr>
        	<td>Password:</td><td>Your registered HCP Password</td>
		</tr>
        <tr>
        	<td>Directory:</td><td>/</td>
        </tr>
        <tr>
        	<td>Connection:</td><td>SFTP / FTP over SSL</td>
        </tr>
      </table>
      </div>
      <p><strong>Need an HCP Account? <a href="https://db.humanconnectome.org">Register at db.humanconnectome.org</a></strong></p>
      <h3 style="margin-top:2em;">Configuring your FTP Client</h3>
      <p>The Human Connectome Project FTP site will work with any major FTP client, as long as it supports SFTP connections. (Virtually all of them do.) Here is a demo using <a href="http://filezilla-project.org/">FileZilla</a>, which is a free and open-source FTP client that is available for Mac OS, Windows or Linux.</p>
      <p><strong>Step 1: Set up your connection</strong></p>
      <ul>
      	<li><p>Click on FILE &gt; SITE MANAGER</p>
        	<p><img src="$content.getURI('/images/img/tutorial/FTP instructions/data-filezilla-filemanager.png')" /></p></li>
        <li><p>Click on "New Site"</p>
        	<p><img src="$content.getURI('/images/img/tutorial/FTP instructions/filezilla-new-site.png')" /></p></li>
        <li><p>Enter your FTP credentials, as listed in the table above.</p>
        	<p><img src="$content.getURI('/images/img/tutorial/FTP instructions/filezilla-enter-credentials.png')" /></p></li>
        <li><p>Click "Connect" to connect and you will be logged in. You may be asked to confirm a security key on your first login. If so, say yes. </p>
        	<p><img src="$content.getURI('/images/img/tutorial/FTP instructions/filezilla-logged-in.png')" /></p></li>
        <li><p>Once you are logged in, you will be able to browse data <strong>for each project that you have permission for</strong>. To get permission, you must accept the terms of use for that project via <strong><a href="https://db.connectome.org/">the ConnectomeDB website</a></strong>. </p></li>
        <li><p>As you browse the data sets, you can transfer data from the FTP site to your local disk by dragging the contents of a folder from right to left. (Note: you cannot post data to the public HCP FTP site.)</p></li>
    </ul> 
        
         
  <!-- Primary page Content -->
</div> 
  <!-- middle -->

<div id="sidebar-rt">
      <div id="search">
      <!--   If Javascript is not enabled, nothing will display here -->	
      </div> <!-- Search box -->
      <div id="sidebar-content">
          <div class="sidebox databox">
              <h2 style="border-top:1px solid #ccc">Frequently Asked Questions</h2>
              <ul>
              	<li>How do I get access to the data?</li>
                <li>Why should I be interested in the data?</li>
                <li>Can I publish my findings on this data?</li>
              </ul>
          </div>
          <div class="sidebox-bottom"></div>
          <!-- /sidebox -->
          
          <div class="sidebox databox">
          	<h2>Data Release Calendar</h2>
            <p><strong>Oct 1, 2012: Initial Data Release</strong></p>
            <ul>
            	<li>15 subjects</li>
            </ul>
            <p><strong>Feb 15, 2013: Q1 Data Release</strong></p>
            <ul>
            	<li>~80 total subjects</li>
                <li>~25 open access</li>
            </ul>
            <p><strong>May 15, 2013: Q2 Data Release</strong></p>
            <ul>
            	<li>~180 subjects total</li>
                <li>~50 open access</li>
            </ul>
          </div>
           <div class="sidebox-bottom"></div>
         
      </div>
  </div>

<div id="footer"><!-- #BeginLibraryItem "/Library/Footer text.lbi" -->
<p style="font: 12px/20px Arial, Helvetica, sans-serif; text-align:justify;"><span style="font: 18px Georgia, 'Times New Roman', Times, serif"><a href="http://www.wustl.edu/" title="Washington University in Saint Louis" target="_new">Washington University in Saint Louis</a> - <a href="http://www.umn.edu/" title="University of Minnesota" target="_new">University of Minnesota</a></span><br />
<a href="http://www.ox.ac.uk/" title="Oxford University" target="_new">Oxford University</a> - <a href="http://www.slu.edu/" title="Saint Louis University (SLU)" target="_new">Saint Louis University</a> - <a href="http://www.advancedmri.com/" title="Advanced MRI Technologies (AMRIT)" target="_new">Advanced MRI Technologies</a> - <a href="http://www.iu.edu/" title="Indiana University" target="_New">Indiana University</a> - <a href="http://www.unich.it/unichieti/appmanager/unich_en/university_en?_nfpb=true&_pageLabel=P15800195411249373765813" title="University d'Annunzio" target="_new">University d’Annunzio</a><br />
<a href="http://www.esi-frankfurt.de/" title="Ernst Strungmann Institute" target="_new">Ernst Strungmann Institute</a> - <a href="http://www2.warwick.ac.uk/" title="Warwick University" target="_new">Warwick University</a> - <a href="http://berkeley.edu/" title="University of California at Berkeley" target="_new">University of California at Berkeley</a> - <a href="http://www.ru.nl/donders/" title="The Donders Institute" target="_new">Radboud University Nijmegen</a></p>
<p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/" title="National Institutes of Health (NIH)" target="_new">National Institutes of Health</a>, and all information in this site is available to the public domain. No Protected Health Information has been published on this site. Last updated <script type="text/javascript">document.write(document.lastModified);</script>. 
</p>

<p><a href="/privacy/">Privacy</a> |  <a href="/sitemap/">Site Map</a> | <a href="/consortia/" style="color:#33c; font-weight:bold">NIH Blueprint for Neuroscience Research: the Human Connectome Project</a> | Follow <a href="http://twitter.com/HumanConnectome"><strong>@HumanConnectome</strong></a> on Twitter</p> 

<!-- #EndLibraryItem --></div>

</div>
<!-- end container -->

<script src="http://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
/*
*  How to restrict a search to a Custom Search Engine
*  http://www.google.com/cse/
*/

google.load('search', '1');

function OnLoad() {
  // Create a search control
  var searchControl = new google.search.SearchControl();

  // Add in a WebSearch
  var webSearch = new google.search.WebSearch();

  // Restrict our search to pages from our CSE
  webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

  // Add the searcher to the SearchControl
  searchControl.addSearcher(webSearch);

  // tell the searcher to draw itself and tell it where to attach
  searchControl.draw(document.getElementById("search"));

  // execute an inital search
  // searchControl.execute('');
}

google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18630139-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
