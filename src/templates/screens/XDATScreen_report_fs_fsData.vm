<!-- begin XDATScreen_report_fs_fsData -->
<script type="text/javascript" src="$content.getURI('/scripts/datasetUtils/datasetUtils.js')"></script>
$page.setTitle("Freesurfer Analysis Details")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)
#if ($data.getParameters().getString("popup"))
	#set ($popup = $data.getParameters().getString("popup") )
	#set ($popup = "false")
#end

<div id="header">
  <div class="subject-page header">
	<h1 id="page-title">Freesurfer Analysis Details: $!item.getProperty('label')</h1>
	<!-- borrow breadcrumb trail from xdat-templates/default.vm -->
	#parse("/screens/Breadcrumb-screen.vm")
  </div>
</div>

<div name="experiment-toolbar" id="toolbar-positioner">
  <div id="toolbar-container" class="subjectKey">
	<div id="toolbar"> <!-- toolbar listing order from right to left -->

	  <div class="toolset vertical"> <!-- get data -->
		<div class="toolset-header">Actions</div>
		<div class="toolset-content">
		  #set($dashboardUri = "app/template/SubjectDashboard.vm?subject=${subject.getLabel()}")
		  <div class="tool" onclick="window.location.assign('$content.getURI($dashboardUri)')">
			  <img src="$content.getURI('images/tool-dashboard.png')" />
			  <label>Go to Dashboard</label>
		  </div>

		  <div class="tool" onclick="return popupWithProperties('/app/template/Fs_fsData_QC_64.vm/search_value/$item.getProperty("ID")/search_element/fs%3AfsData/search_field/fs%3AfsData.ID','','width=950,height=600,status=yes,resizable=yes,scrollbars=yes')">
			<img src="$content.getURI('images/tool-view-snapshots.png')" />
			<label>View Snapshots</label>
		  </div>
		  
		</div>
	  </div>

	  <div class="toolset current-selection"> <!-- primary (left-aligned) -->
		<div class="toolset-header">Current Selection</div>
		<div class="toolset-content">
		  <h3 class="subject-group-icon selection-title">$!subject.GenderText, aged $!subject.getDisplayField("AGE_RANGE")</h3>
		  <p><strong>Accession ID:</strong> $!subject.getId()</p>
		  <p><strong>FreeSurfer Version:</strong> $!item.getStringProperty("fs:fsData/fsversion")</p>
		  <p><strong>Last Updated:</strong> $!subject.InsertDate</p>
		</div>
	  </div>

	</div>
  </div> <!-- end toolbar container -->
</div> <!-- end #toolbar-positioner -->

##<script type="text/javascript" src="$content.getURI('/scripts/data-access.js')"></script>
<script type="text/javascript" src="$content.getURI('/scripts/HCP/dataAccess.js')"></script>
<script>
	// clean up appearance of toolbar, after all dynamic elements have been loaded.
	$(document).ready(function(){
		toolbarCleanup(); 
	});
</script>
	
<div class="report-info-block">

	<h2 class="expts_header">View Snapshots</h2>
	<p><strong><a href="javascript:void()" onclick="return popupWithProperties('/app/template/Fs_fsData_QC_64.vm/search_value/$item.getProperty("ID")/search_element/fs%3AfsData/search_field/fs%3AfsData.ID','','width=950,height=600,status=yes,resizable=yes,scrollbars=yes')">Launch Snapshot Viewer</a></strong></p>
	
	<h2 class="expts_header">FreeSurfer Summary</h2>
	<table width="100%">
		<thead>
			<tr><th>Measure</th><th>Value</th></tr>
		</thead>
		<tbody>
			<tr><td>Estimated inter-cranial vol.</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/etiv"),0)</td></tr>
			<tr><td>Brain segmentation vol.</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/brainsegvol"),0)</td></tr>
			<tr><td>Brain segmentation vol. w/o ventricles</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/brainsegvolnotvent"),0)</td></tr>
			<tr><td>Brain segmentation vol. w/o ventricles from surf</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/brainsegvolnotventsurf"),0)</td></tr>
			<tr><td>Left hemi. cortical gray matter vol.</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/lhcortexvol"),0)</td></tr>
			<tr><td>Right hemi. cortical gray matter vol.</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/rhcortexvol"),0)</td></tr>
			<tr><td>Total cortical gray matter vol. (based on surface-stream) </td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/cortexvol"),0)</td></tr>
			<tr><td>Subcortical gray matter vol.</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/subcortgrayvol"),0)</td></tr>
			<tr><td>Total gray matter vol.</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/totalgrayvol"),0)</td></tr>
			<tr><td>Supratentorial vol.</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/supratentorialvol"),0)</td></tr>
			<tr><td>Supratentorial w/o ventricals vol.</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/supratentorialvolnotvent"),0)</td></tr>
			<tr><td>Supratentorial w/o ventricals voxel count</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/supratentorialvolnotventvox"),0)</td></tr>
			<tr><td>Left hemi. cortical white matter vol.</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/lhcorticalwhitemattervol"),0)</td></tr>
			<tr><td>Right hemi. cortical white matter vol.</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/rhcorticalwhitemattervol"),0)</td></tr>
			<tr><td>Total cortical white matter vol.</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/corticalwhitemattervol"),0)</td></tr>
			<tr><td>Mask vol.</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/maskvol"),0)</td></tr>
			<tr><td>Ratio of BrainSegVol to eTIV</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/brainsegvol2etiv"),3)</td></tr>
			<tr><td>Ratio of MaskVol to eTIV</td><td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/maskvol2etiv"),3)</td></tr>
			<tr><td>Number of defect holes in lh prior to fixing</td><td>$!turbineUtils.formatNumber($!item.getIntegerProperty("fs:fsData/measures/volumetric/lhsurfaceholes"),0)</td></tr>
			<tr><td>Number of defect holes in rh prior to fixing</td><td>$!turbineUtils.formatNumber($!item.getIntegerProperty("fs:fsData/measures/volumetric/rhsurfaceholes"),0)</td></tr>
			<tr><td>Total number of defect holes in surfaces prior to fixing</td><td>$!turbineUtils.formatNumber($!item.getIntegerProperty("fs:fsData/measures/volumetric/surfaceholes"),0)</td></tr>
		</tbody>
	</table>

<!-- BEGIN fs_fsData VOLUMETRIC MEASURES -->
#set($fs_fsData_region_11_NUM_ROWS=$item.getChildItems("fs:fsData/measures/volumetric/regions/region").size() - 1)
#if($fs_fsData_region_11_NUM_ROWS>=0)
	
	<h2 class="expts_header">Subcortical Volume Segmentation</h2>
	<table width="100%">
		<thead>
		<tr>
			<th>Region Name</th>
			<th>Seg Id</th>
			<th>Hemisphere</th>
			<th>NVoxels</th>
			<th>Volume</th>
			<th>Mean</th>
			<th>Range</th>
		</tr>			
		</thead>
		<tbody>				
	
	#foreach($fs_fsData_region_11_COUNTER in [0..$fs_fsData_region_11_NUM_ROWS])
		<!-- BEGIN fs_fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER] -->
		<tr>
			<td>$!item.getStringProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/name")</td>
			<td>$!item.getStringProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/SegId")</td>
			<td>$!item.getStringProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/hemisphere")</td>
			<td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/NVoxels"),0)</td>
			<td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/Volume"),0)</td>
			<td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/normMean"),4) +-$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/normStdDev"),4)</td>
			<td>$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/normRange"),0)&nbsp;($!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/normMin"),0)-$!turbineUtils.formatNumber($!item.getDoubleProperty("fs:fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER]/normMax"),0))</td>
		</tr>
		<!-- END fs_fsData/measures/volumetric/regions/region[$fs_fsData_region_11_COUNTER] -->
	#end
		</tbody>
	</table>
#end
	<!-- END fs_fsData VOLUMETRIC MEASURES-->
		
	<!-- BEGIN fs_fsData SURFACE MEASURES-->
	<h2 class="expts_header">Surface Measures</h2>

#set($fs_fsData_hemisphere_12_NUM_ROWS=$item.getChildItems("fs:fsData/measures/surface/hemisphere").size() - 1)
#if($fs_fsData_hemisphere_12_NUM_ROWS>=0)

	#set ($hemis = $om.getMeasures_surface_hemisphere())
	#set ($hemis_NUM_ROWS = $hemis.size() - 1)
	#foreach($fs_fsData_hemisphere_COUNTER in [0..$hemis_NUM_ROWS])
		#set ($hemi = $hemis.get($fs_fsData_hemisphere_COUNTER))
		#set ($hemi_regions = $hemi.getRegions_region())
		#set ($hemi_regions_NUM_ROWS = $hemi_regions.size() - 1)
		<p><strong>Hemisphere: $hemi.getName() NumVert: $!turbineUtils.formatNumber($hemi.getNumvert(),0) SurfArea: $!turbineUtils.formatNumber($hemi.getSurfarea(),0)</strong></p>
		<table width="100%">
			<thead>
			<tr>
				<th>Region Name</th>
				<th>NumVert</th>
				<th>SurfArea</th>
				<th>GrayVol</th>
				<th>Thickness</th>
				<th>MeanCurv</th>
				<th>GausCurv</th>
				<th>FoldInd</th>
				<th>CurvInd</th>
			</tr>			
			</thead>
			
			<tbody>				
		#foreach($fs_fsData_hemisphere_regions_COUNTER in [0..$hemi_regions_NUM_ROWS])
		#set ($region = $!hemi_regions.get($fs_fsData_hemisphere_regions_COUNTER))
			<tr>
				<td>$!region.getName()</td>
				<td>$!turbineUtils.formatNumber($!region.getNumvert(),0)</td>
				<td>$!turbineUtils.formatNumber($!region.getSurfarea(),0)</td>
				<td>$!turbineUtils.formatNumber($!region.getGrayvol(),0)</td>
				<td>$!turbineUtils.formatNumber($!region.Thickavg,3) +-$!turbineUtils.formatNumber($!region.Thickstd,3)</td>
				<td>$!turbineUtils.formatNumber($!region.getMeancurv(),3)</td>
				<td>$!turbineUtils.formatNumber($!region.getGauscurv(),3)</td>
				<td>$!turbineUtils.formatNumber($!region.getFoldind(),3)</td>
				<td>$!turbineUtils.formatNumber($!region.getCurvind(),3)</td>
			</tr>
		#end
		</tbody>
		</table>
	#end
#end

<!-- END fs_fsData SURFACE MEASURES-->

<!-- end XDATScreen_report_fs_fsData -->
