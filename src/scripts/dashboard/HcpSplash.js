/*jslint vars: true, browser: true, white: true, evil : true */
/*global XNAT, $, alert, YAHOO, serverRoot */
XNAT.app.HcpSplash = new function() {"use strict";

	var that = this;

	this.siteFiltersLog = [];

	this.populateCannedSubjectLists = function() {
		this.loadSiteFilters = {
			success : function(o) {
                var response = YAHOO.lang.JSON.parse(o.responseText);
                that.siteFiltersLog = eval(response.ResultSet.Result[0].contents);
				this.populateSubjectList("Single Subject", "SingleSubject");
				this.populateSubjectList("5 Unrelated Subjects", "FiveUnrelatedSubjects");
				this.populateSubjectList("10 Unrelated Subjects", "TenUnrelatedSubjects");
				this.populateSubjectList("20 Unrelated Subjects", "TwentyUnrelatedSubjects");
			//	this.populateSubjectList("50 Unrelated Subjects", "FiftyUnrelatedSubjects");
            //  this.populateSubjectList("80 Unrelated Subjects", "EightyUnrelatedSubjects");
				this.populateSubjectList("100 Unrelated Subjects", "HundredUnrelatedSubjects");
				this.populateSubjectList("MEG Subjects", "MEGSubjects");
	    		closeModalPanel("HcpSplashModal");
			},
			failure : function(e) {
	    		closeModalPanel("HcpSplashModal");
				if (404 === e.status) {
					alert("There are no site-wide subject filters set up in the system.");
				} else {
					alert("Failed to load the site-wide subject filters");
				}
			},
            cache : false, // Turn off caching for IE
			scope : this
		};

		YAHOO.util.Connect.asyncRequest('GET', serverRoot + '/REST/config/dashboard/subject/filters.json?format=json&nocache=' + ((new Date()).getTime()), this.loadSiteFilters, null, this);
	};

	this.populateSubjectList = function(listDescription, listField) {
		var i;
		for ( i = 0; i < this.siteFiltersLog.length; i = i + 1) {
			if (this.siteFiltersLog[i].desc === listDescription) {
				var csvSubjectList = this.extractSubjectListFromFilter(this.siteFiltersLog[i].filter);
				$("#" + listField).val(csvSubjectList);
			}
		}
	};

	this.extractSubjectListFromFilter = function(filter) {
		var keyIndex = filter.indexOf("SUBJECT_LABEL==");
		if (keyIndex === -1) {
			alert("Internal filter syntax may have changed, did not find the subject labels.  Buttons will not work correctly");
			return "";
		}
		return filter.substr(keyIndex).replace("SUBJECT_LABEL==", "").replace(/\|/g, ",");
	};
	
	this.submitDownloadRequest = function(subjectListField) {
		var subjectList = $("#" + subjectListField).val();
		$("#subjects").val(subjectList);
		$("#query").val("Subject = (".concat(subjectList.replace(/,/g, "|")).concat(")"));
		$("#downloadSubjectGroupForm").submit();
	};
	
    this.loadUserCustomGroups = function () {
        this.loadUserCustomGroupsCallback = {
            success: function (o) {
                var userCustomGroups = YAHOO.lang.JSON.parse(o.responseText);

			    var gSelect = $('#custom_group_selector');
			    gSelect.html('');//clear any previous groups
			    //gSelect.append($('<option></option>').prop("value", "Open Custom Group...").text("Open Custom Group..."));

				if(!that.userCustomGroupsContainsOnlyMostRecentDownloadGroup(userCustomGroups)) {
					var i = 0;
				    for (i = 0; i < userCustomGroups.length; i=i+1) {
				        gSelect.append($('<option class="group"></option>').prop("value", userCustomGroups[i].desc).text(userCustomGroups[i].desc.getExcerpt(100)));
				    	$("#dataset_tool_custom_group").show();
				    }
				}
                xMenuCreate(gSelect);
            },
            failure: function (e) {
            	if(e.status !== 404) {
            		// You may not have any user-defined filters, and that's OK.  You're beautiful, just the way you are.
            		alert("Something went wrong loading the user's custom groups");
            	}
            },
            cache : false, // Turn off caching for IE
            scope: this
        };

        YAHOO.util.Connect.asyncRequest('GET', serverRoot + '/REST/user/cache/resources/searchlog/files/filters.json?nocache=' + ((new Date()).getTime()), this.loadUserCustomGroupsCallback, null, this);

    };
    
    this.userCustomGroupsContainsOnlyMostRecentDownloadGroup = function(userCustomGroups) {
    	return userCustomGroups.length === 1 && XNAT.app.HcpUtil.isUserMostRecentDownloadGroupName(userCustomGroups[0].desc); 
    };
    
    this.loadPhase2Data = function() {
		this.populateCannedSubjectLists();
		this.loadUserCustomGroups();
    };

	$(document).ready(function() {
		$('button.exploreButton').click(function() { 
			var subjectGroupName;
			if(this.id === "exploreSingleSubject") {
				subjectGroupName = "Single Subject";
			}
			else if(this.id === "exploreFiveSubjects") {
				subjectGroupName = "5 Unrelated Subjects";
			}
			else if(this.id === "exploreTenSubjects") {
				subjectGroupName = "10 Unrelated Subjects";
			}
			else if(this.id === "exploreTwentySubjects") {
				subjectGroupName = "20 Unrelated Subjects";
			}
			else if(this.id === "exploreFortySubjects") {
				subjectGroupName = "40 Unrelated Subjects";
			}
			else if(this.id === "exploreFiftySubjects") {
				subjectGroupName = "50 Unrelated Subjects";
			}
            else if(this.id === "exploreEightySubjects") {
                subjectGroupName = "80 Unrelated Subjects";
            }
			else if(this.id === "exploreHundredSubjects") {
                subjectGroupName = "100 Unrelated Subjects";
            }
			else if(this.id === "exploreMEGSubjects") {
                subjectGroupName = "MEG Subjects";
            }
			$("#subjectGroupName").val(subjectGroupName);
			$("#exploreSubjectGroupForm").submit();
		});
	
		$('button.downloadButton').click(function() {
			that.submitDownloadRequest(this.id.replace("Download", ""));
		});
		
		$("#custom_group_selector").change(function () {
		    if ($(this).find(":selected").hasClass("group")) {
				$("#subjectGroupName").val($(this).val());
				$("#exploreSubjectGroupForm").submit();
		    }
		});
	});
};
