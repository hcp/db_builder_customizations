// JavaScript Document

/* Simple jQuery slider, used on the Download Packages confirmation page 
 * requires: simple-slider.css
 */ 
 
var autoSlide = true; 
function moveSlides(dir) {
	var activeSlide = $('#simple-slider li.active'); 
	var numSlides = $('#simple-slider li').length;
	if (dir === 'left') {
		// activate slide to the left, rotating to far right as needed
		if ( $(activeSlide).index() >0 ) {
			nextSlide = $(activeSlide).prev('li');
		} else {
			nextSlide = $('#simple-slider li:last-child');
		}
	} else { 
		// activate slide to the right, rotating to far left as needed
		if ( ($(activeSlide).index()+1) < numSlides ) {
			nextSlide = $(activeSlide).next('li');
		} else {
			nextSlide = $('#simple-slider li:first-child');
		}
	}
	$(activeSlide).fadeOut(250).removeClass('active');
	$(nextSlide).fadeIn(250).addClass('active');
	showCount();
}
function showCount() {
	var activeSlide = $('#simple-slider li.active'); 
	var numSlides = $('#simple-slider li').length;
	var counter =''; 
	for (i=0; i<numSlides; i++) {
		counter += (i === $(activeSlide).index()) ? ' <i>&bull;</i> ' : ' &bull; ';
	}
	$('#slide-counter').html(counter);
}
$(document).ready(function(){ 
	showCount();
	$('#slider-container').hover(function(){
		// on mouse enter
		$('.slider-toggle').fadeIn(50);
	}, function(){
		// on mouse leave
		$('.slider-toggle').fadeOut(50);	
	});
});
if (autoSlide) { setInterval(function() { moveSlides('right'); },8000); }