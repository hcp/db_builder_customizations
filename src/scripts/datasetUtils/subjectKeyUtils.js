// set projectId as a global variable
var projectId = XNAT.data.context.projectID; 


// update SubjectKey Status
var updateCompleteness = function(){
	var newCompleteness = "";
	var onclickFunction = "/app/template/SubjectKey2.vm?projectID="+projectId;

	if((canEdit)&&(accessibility!="protected" || (isAdmin=="true"))){
		$("#removeDataset").attr("onclick","openDeleteModal();");
		$("#removeDataset").removeClass("disabled");
	}
	else{
		$("#removeDataset").removeAttr("onclick");
		$("#removeDataset").addClass("disabled");
	}
	if(accessibility=="protected"){
		newCompleteness = 'Subject Key has been shared. You can still edit its info and related publications. However, you must <a href="mailto:support@humanconnectome.org">contact support</a> if you want to delete it or modify its coded IDs.';
		$("#publishDataset").addClass("disabled");
		$("#publishDataset").removeAttr("onclick");
		$("#uploadGroup").addClass("disabled");
		$("#uploadGroup").removeAttr("onclick");
	}
	else if(codedIdCount>0){
		if(pubsActive>0){
			newCompleteness = 'Status: You have Coded IDs uploaded to this Subject Key, and you have successfully linked one or more articles. ';
			if(canEdit=="true"){
				newCompleteness += '<br />Final Step: <a href="#" onclick="publish();" title="Share Subject Key.">Make Subject Key visible to HCP users</a>. (Restricted Access is required to view).';
				$("#publishDataset").removeClass("disabled");
				$("#publishDataset").attr("onclick","publish();");
			}
		}
		else{
			newCompleteness = 'Status: You have Coded IDs uploaded to this Subject Key.';
			if(canEdit=="true"){
				newCompleteness += '<br />Next Steps: <a href="#" onclick="openPubModal();">Link to article</a> or <a href="#" onclick="publish();" title="Share Subject Key.">make Subject Key visible to HCP users</a>. (Restricted Access is required to view).';
				$("#publishDataset").removeClass("disabled");
				$("#publishDataset").attr("onclick","publish();");
			}
		}
	}
	else if(pubsActive>0){
		newCompleteness = 'Status: You have successfully linked one or more articles to this Subject Key. ';
		if(canEdit=="true"){
			newCompleteness += '<br />Next Step: <a href="#" onclick="window.location.assign(\''+onclickFunction+'\')">Upload Coded IDs into Subject Key</a>.';
		}
		$("#publishDataset").addClass("disabled");
		$("#publishDataset").removeAttr("onclick");
	}
	else{
		newCompleteness = 'Status: You have successfully created a Subject Key.';
		if(canEdit=="true"){
			newCompleteness = ' Next Step: <a href="#" onclick="window.location.assign(\''+onclickFunction+'\')">Upload Coded IDs into Subject Key</a>.';
		}
		$("#publishDataset").addClass("disabled");
		$("#publishDataset").removeAttr("onclick");
	}
	$( "#completeness").html(newCompleteness);
};

var pubSuccess=function(o){
	xModalLoadingClose();
	xModalMessage('Subject Key Shared', "This subject key is now shared.");
	accessibility="protected";
	updateCompleteness();
};
var pubFailure=function(o){
	xModalLoadingClose();
	if(o.status==401){
		xModalMessage('Session Expired', sessExpMsg);
		window.location=serverRoot+"/app/template/Login.vm";
	}else{
		xModalMessage('Subject Key Publication Error', 'Error ' + o.status + ': Change failed.');
	}
};

// publish, or "share" Subject Key
var pubCallback = {
    success:pubSuccess,
    failure:pubFailure,
    cache:false, // Turn off caching for IE
    scope:this
};

publish = function(){
	if(!($("#publishDataset").hasClass("disabled"))){
		var confPubOptions = {} ;
		confPubOptions.action = function(){
			xModalLoadingOpen({title:'Please wait...'});
			$("#publishDataset").removeAttr("onclick");
			$("#publishDataset").addClass("disabled");
			YAHOO.util.Connect.asyncRequest('PUT',serverRoot + "/REST/projects/"+projectId+"/accessibility/protected?XNAT_CSRF="+csrfToken,pubCallback,null,this);
		};
		confPubOptions.cancel= 'show';
		xModalMessage('Share Subject Key', 'Do you want to allow all users with restricted access to view this Subject Key? Once you share a Subject Key, you can no longer remove it or edit its Coded IDs. Any such changes will have to be made by a ConnectomeDB Admin.', 'Share', confPubOptions);
	}
};

var hasDatasetsSuccess=function(o){
    var datasetArray = YAHOO.lang.JSON.parse(o.responseText).ResultSet.Result;
    if(datasetArray.length==0){
        xModalMessage('Please log out', 'When creating your first subject key, you may experience problems if you do not log out and back in at this point before uploading Coded IDs into your Subject Key.');
    }
};

var hasDatasetsFailure= {}

var hasDatasetsCallback= {
    success:hasDatasetsSuccess,
    failure:hasDatasetsFailure,
    cache:false, // Turn off caching for IE
    scope:this
}
	
// display Subject Key permalink
openLinkModal = function(){
	xModalMessage('Subject Key Link', 'Select and copy this URL to link to your subject key. Note: Only users with restricted data access can view this subject key and they will only be able to view it once you have shared it. <br><input style="width: 100%" type="text" name="link" value="'+location.hostname + '/data/projects/'+ projectId+'" readonly>');
};


// delete Subject Key
var delSuccess=function(o){
	xModalLoadingClose();
	var delSucOptions = {} ;
	delSucOptions.action = function(){
		window.location=serverRoot+"/app/template/SubjectKeys.vm";
	};
	xModalMessage('Subject Key Removed', "This subject key has been permanently deleted.","OK",delSucOptions);
};

var delFailure=function(o){
	xModalLoadingClose();
	if(o.status==401){
		xModalMessage('Session Expired', sessExpMsg);
		window.location=serverRoot+"/app/template/Login.vm";
	}else{
		xModalMessage('Subject Key Delete Error', 'Error ' + o.status + ': Delete failure.');
	}
};

var deleteDataset = {
	success:delSuccess,
	failure:delFailure,
	cache:false, // Turn off caching for IE
	scope:this
};

openDeleteModal = function(){
	var delOptions = {} ;
	delOptions.action = function(){
		xModalLoadingOpen({title:'Please wait...'});
		YAHOO.util.Connect.asyncRequest('DELETE',serverRoot + "/data/projects/"+projectId,deleteDataset,null,this);
	};
	delOptions.cancel= 'show';
	xModalMessage('Remove Subject Key', 'Are you sure you want to permanently delete this subject key?', 'Remove', delOptions);
};

openDeleteModalForProject = function(pId){
    var delOptions = {} ;
    delOptions.action = function(){
        xModalLoadingOpen({title:'Please wait...'});
        YAHOO.util.Connect.asyncRequest('DELETE',serverRoot + "/data/projects/"+pId,deleteDataset,null,this);
    };
    delOptions.cancel= 'show';
    xModalMessage('Remove Subject Key', 'Are you sure you want to permanently delete this subject key?', 'Remove', delOptions);
};

var whatModal=function() {
	xModalMessage('Defining "Subject Key" and "Coded ID"', '<p>A <strong>Coded ID</strong> is a new label to describe a subject without allowing readers to connect that subject back to its HCP ID. For example, "Subject A" could be a Coded ID for HCP Subject 100307.</p><p>A <strong>Subject Key</strong> is a collection of Coded IDs and the HCP IDs for each subject you cite. Only HCP users with restricted data access may view these Subject Keys.</p>');
}

var howModal=function() {
	xModalMessage('How do I create a Subject Key?', '<p>ConnectomeDB allows users to create a Subject Key on our server by uploading a simple spreadsheet containing pairings of HCP Subject IDs with Coded IDs.  To conform to the proper format, <a href="/app/template/DownloadSubjectKey.vm">download the CSV Template</a>, enter your project-specific pairings, and rename and save the csv file.</p><p>Switch your Current Data View to "<strong>Restricted (1)</strong>" (from the default "Open Access"), then press "<strong>Create New Subject ID Key</strong>". This will let you provide more information about the Subject Key, link it to related publications, and make it viewable by other HCP users who have restricted access.</p>', "OK", {scroll:true});
}

var elevateTier=function(redirect) {
	titleStr = "Restricted Access Required"
	contentStr = 'Subject keys offer translation between published restricted subject data and open access subject IDs. ' +
		'To view or create a subject key, you must have restricted data access and be viewing this site in restricted data mode.'

	var modal_opts = {
		width:     450,  // box width when used with 'fixed' size, horizontal margin when used with 'custom' size
		height:    280,  // box height when used with 'fixed' size, vertical margin when used with 'custom' size
		title:     titleStr,
		content: "<div style='float:left;padding: 20px 10px 20px 0; width: 64px'><img src='/style/furniture/icon-lock-64px.png' alt='Lock'></div>" +
		"<div style='float:right; width:300px;'><h4>Restricted Data Access Required!</h4><p>" + contentStr + "</p></div>",
		ok: 'show',
		okLabel: 'Switch to Restricted View',
		okAction: function(){
			window.location.assign(redirect);
		},
		cancel: 'show',
		cancelLabel: 'Cancel',
		cancelAction: function(){
//                jq("select#tierSelect").val(0);
		} ,
		closeBtn: 'hide'
	};

	xmodal.open(modal_opts);
}

var dismissTierWarning=function() {
	titleStr = "Restricted Access Required"
	contentStr = 'Subject keys offer translation between published restricted subject data and open access subject IDs. ' +
		'To view or create a subject key, you must have restricted data access and be viewing this site in restricted data mode.'

	var modal_opts = {
		width:     450,  // box width when used with 'fixed' size, horizontal margin when used with 'custom' size
		height:    280,  // box height when used with 'fixed' size, vertical margin when used with 'custom' size
		title:     titleStr,
		content: "<div style='float:left;padding: 20px 10px 20px 0; width: 64px'><img src='/style/furniture/icon-lock-64px.png' alt='Lock'></div>" +
		"<div style='float:right; width:300px;'><h4>Restricted Data Access Required!</h4><p>" + contentStr + "</p>" +
		'<p><a href="http://www.humanconnectome.org/data/data-use-terms/restricted-access.html" target="_blank">View Restricted Access Data Use Terms</a></p></div>',
		ok: 'show',
		okLabel: 'OK',
		okAction: function(){
		},
		cancel: 'hide',
		closeBtn: 'hide'
	};

	xmodal.open(modal_opts);
}

var switchTierModal=function(dataset) {
	if (hasRestrictedAccess == 'true') {
		// Create a link in the modal that allows the user to elevate tier and go straight to the subject key page
		return '<a href="javascript:" onclick=elevateTier("/data/projects/'+dataset.id+'?tierSelect=1");>'+dataset.name+'</a>';
	}
	else {
		return '<a href="javascript:" onclick=dismissTierWarning();>'+dataset.name+'</a>';
	}
}