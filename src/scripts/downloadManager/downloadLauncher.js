/*jslint browser: true, white: true, vars: true */
/*global XNAT, AW, jQuery, showMessage */
function AsperaDownloadLauncher() {"use strict";

	this.launchDownload = function(url, formData, sessionName) {

		var asperaWeb = new AW4.Connect({
			id : 'aspera_web_transfers'
		});

		var fileControls = {
			'connect_settings' : {
				'allow_dialogs' : 'yes'
			}
		};
		fileControls.handleTransferEvents = function(event, obj) {
			switch (event) {
				case AW4.Connect.EVENT.TRANSFER:
					break;
				default:
					break;
			}
		};
		fileControls.downloadFile = function(el) {
			document.getElementById('transfer_spec').innerHTML = JSON.stringify(fileControls.transfer_spec, null, "    ");
			var response = asperaWeb.startTransfer(fileControls.transfer_spec, fileControls.connect_settings);
		};

		jQuery.post(url, formData, function(data) {
			fileControls.transfer_spec = data;
			asperaWeb.addEventListener(AW4.Connect.EVENT.TRANSFER, fileControls.handleTransferEvents);
			asperaWeb.initSession();
			fileControls.downloadFile(this);
		}).error(function(jqxhr, textStatus, errorThrown) {
			showMessage("page_body", "Error", errorThrown);
		});
	};
}
