/**
 *
 * HCP Package Download script for filtering, queueing, and downloading data packages for Human Connectome Project data releases.
 * @author Mark M. Florida - Washington University School of Medicine - floridam@wustl.edu
 * @version 2.0
 * @namespace HCP.app.downloadPackages
 *
 */

// set up namespace for downloadPackages
if (typeof HCP == 'undefined') var HCP={};
if (typeof HCP.app == 'undefined') HCP.app={};
if (typeof HCP.app.downloadPackages == 'undefined') HCP.app.downloadPackages={};

//HCP.app.downloadPackages.url = 'x'; // uncomment to use for testing errors

(function( _dP, _jq ){

    if (typeof _jq == 'undefined') {
        if (window.console && window.console.log){
            window.console.log('jQuery is required.');
        }
        return;
    }

    // dp gets mapped(?) back to HCP.app.downloadPackages
    var dp = _dP || {};

    // _dp (leading underscore) is only used locally
    var _dp = {};

    // make sure $===jQuery
    var $ = _jq;

    // cache existing DOM objects
    var $body, $container, $filters, $packages, $download_packages;

    // container for local functions
    var _fn = {};

    // store the list of subjects
    var csvSubjects;

    dp._url = dp.url; // || ''; // copy to _url for kicks and maybe easy(er) debugging?
    dp.fn = dp.fn || {};

    _dp.templates = {}; // container for HTML templates

    _dp.keywords = {};
    _dp.keywords.all = [];
    _dp.keywords.matched = [];
    _dp.keywords.children = {};
    _dp.keywords.orphans = []; // catch any keywords from the package listing that aren't available as a filter option
    _dp.keywords.defaults = {};

    _dp.filter_names = [];

    $.ajaxSetup({cache:false});


    //////////////////////////////////////////////////
    // UTILITIES
    //////////////////////////////////////////////////


    if (!Array.isArray) {
        Array.isArray = function(arg) {
            return Object.prototype.toString.call(arg) === '[object Array]';
        };
    }


    //////////////////////////////////////////////////


    function pushUnique( _val, _array ){
        if (Array.isArray(_array) && _array.indexOf(_val) === -1){
            _array.push(_val);
        }
    }


    //////////////////////////////////////////////////


    function dedupeArray( _array ){
        var new_array=[];
        var len = _array.length;
        if (len > 1){
            for (var i = 0; i < len; i++) {
                pushUnique(_array[i], new_array);
            }
        }
        else {
            new_array = _array ;
        }
        _array = new_array;
        return _array;
    }


    //////////////////////////////////////////////////


    function mergeArrays( _source, _targetArray ){
        var source = (Array.isArray(_source)) ? _source : [];
        var target = _targetArray || [];
        var len = source.length;
        if (len > 1){
            for (var i = 0; i < len; i++){
                //pushUnique(source[i], target);
                target.push(source[i]);
            }
        }
        else {
            target = source ;
        }
        _targetArray = target;
        // de-dupe output just in case
        return dedupeArray(_targetArray) ;
    }


    //////////////////////////////////////////////////


    function isFunction( funcName ){
        return (typeof funcName === 'function')
    }


    //////////////////////////////////////////////////


    function convertKeywords( _keywords ){
        // makes keywords a space-separated list
        // with underscores replacing spaces in keyword name
        return _keywords.join(',').replace(/\s/g,'_').replace(/,/g,' ');
    }


    //////////////////////////////////////////////////


    function addCommas(nStr) {
        // add commas to numbers
        nStr += '';
        var
            x = nStr.split('.'),
            x1 = x[0],
            x2 = x.length > 1 ? '.' + x[1] : ''
            ;
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function roundNumber(num, dec) {
        // convert number to file size in KB, MB, GB - rounded to 2 decimal places
        return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    }

    function sizeFormat(fs) {
        if (fs >= 1073741824) {
            return roundNumber(fs / 1073741824, 2) + ' GB';
        }
        if (fs >= 1048576) {
            return roundNumber(fs / 1048576, 2) + ' MB';
        }
        if (fs >= 1024) {
            return roundNumber(fs / 1024, 0) + ' KB';
        }
        return fs + ' B';
    }


    //////////////////////////////////////////////////





    // just do it all on DOM ready
    $(function(){

        $body = $(document.body);
        $container = $('#packages_container'); // contains *ALL* package download stuff

        // use classes for filters in case we want them in more than one place
        $filters = $('.filters');

        // div containing the list of packages
        $packages = $('#packages');
        $download_packages = $('.download_packages');

        var $total_packages = $container.find('.total_packages');
        var $total_count = $container.find('.total_count');
        var $total_size = $container.find('.total_size');

        csvSubjects = $('input[name="subject"]').map(function () {
            return $(this).val();
        }).get().join(',');

        openModalPanel("DownloadPackagesModal", "Loading data...");
        //xModalLoadingOpen();
        //

        _fn.dpGetJSON = function( opts ){
            // helper for getting JSON for the packages

            opts = opts || {};
            //opts.url = opts.url || dp.url;
            // pass the 'done' function (as 'ajaxDone', 'done', or 'success' - maybe I should just pick ONE?
            opts.ajaxDone = opts.ajaxDone || opts.done || opts.success;
            // pass the 'fail' function (as 'ajaxFail', 'fail', or 'error' - maybe I should just pick ONE?
            opts.ajaxFail = opts.ajaxFail || opts.fail || opts.error;

            // can't do anything without a url, so just exit
            //if (typeof opts.url == 'undefined') return ;
            opts.interval = opts.interval || 100; // change interval as desired - 10 ms is pretty fast
            //$.extend(true, opts, _opts);

            //var _url = dp.url || serverRoot + "/admin/download?project="+curr_proj+"&subjects=" + csvSubjects + "&view=packages&XNAT_CSRF=" + csrfToken;
            var _url = dp.url || serverRoot + "/admin/download";

            var formData = {};
            formData.project=curr_proj;
            formData.subjects=csvSubjects;
            formData.view="packages";
            formData.XNAT_CSRF=csrfToken;

            // options for jQuery's $.ajax() method
            var ajaxOpts = {
                //type : "GET",
                //url : serverRoot + "/scripts/downloadManager/downloadPackagesJSON.json",
                type : "POST",
                url : _url,
                data : formData,
                cache: false,
                async: true,
                context: this,
                dataType: 'json'
            };

            if (isFunction(opts.beforeRequest)) opts.beforeRequest(opts);

            // if we pass an 'ajax' property, that will override 'ajaxOpts'
            opts.ajax = $.extend(true, ajaxOpts, opts.ajax || {});

            // start the ajax stuff
            var doAJAX = $.ajax(ajaxOpts);

            doAJAX.done( function( data, textStatus, jqXHR ) {
                if ( !isFunction( opts.ajaxDone ) ) return ;
                // waitForData waits for response, then executes ajaxDone();
                // at least that's the idea
                var waitForData = setInterval(function(){
                    if ( data > '' ) {
                        clearInterval(waitForData);
                        opts.ajaxDone( data, textStatus, jqXHR );
                        //closeModalPanel("DownloadPackagesModal");
                        //xModalLoadingClose();
                    }
                }, opts.interval);
            });

            doAJAX.fail( function( data, textStatus, error ) {
                if ( !isFunction( opts.ajaxFail ) ) return ;
                var waitForData = setInterval(function(){
                    if ( data > '' ) {
                        clearInterval(waitForData);
                        opts.ajaxFail( data, textStatus, error );
                        //closeModalPanel("DownloadPackagesModal");
                        //xModalLoadingClose();
                    }
                }, opts.interval);
            });
            //jsonRequest(opts);
        };


        function noneValid(_what,_thing){
            var no_subjects = {
                width: 340,
                height: 180,
                title: ' ',
                content: 'There are no valid ' + _what + ' for this ' + _thing + '.',
                // footer not specified, will use default footer
                ok: 'show',  // show the 'ok' button
                okLabel: 'OK',
                cancel: 'hide' // do NOT show the 'Cancel' button
            };
            xModalOpenNew(no_subjects);
        }


        function calculateTotals(){

            var total_selected = 0,
                total_hidden = 0,
                total_count = 0,
                total_size = 0 ;

            var pkg_count = 0,
                pkg_size = 0 ;

            var total_pkgs = $container.find('.package').length;
            var total_matched = $container.find('.package.matched').length;
            //var total = $('.package.selected.matched');

            var matched_selected = 0 ;

            $container.find('.package.selected').each(function(){

                var pkg_count = parseInt($(this).find('.pkg_count').data('pkg-count'), 10);
                var pkg_size = parseInt($(this).find('.pkg_size').data('pkg-size'), 10);

                total_count = total_count + pkg_count ;
                total_size = total_size + pkg_size ;

                total_selected++ ;

                // if a package *doesn't* have a "matched" class,
                // add it to the count of hidden packages
                if (!($(this).hasClass('matched'))){
                    total_hidden++ ;
                }

            });

            $('.package.matched.selected').each(function(){
                matched_selected++ ;
            });

            $total_packages.data('count-packages', total_selected).text(addCommas(total_selected));
            $total_count.data('count-total', total_count).text(addCommas(total_count));
            $total_size.data('size-total', total_size).text(sizeFormat(total_size));

            if (total_selected === 1){
                $total_packages.next('.plural').hide();
            }
            else {
                $total_packages.next('.plural').show();
            }
            if (total_count === 1){
                $total_count.next('.plural').hide();
            }
            else {
                $total_count.next('.plural').show();
            }

            if (total_count > 0) {
                $('.buttons .download, .buttons .reset').prop('disabled', false).removeAttr('disabled');
            } else {
                $('.buttons .download, .buttons .reset').prop('disabled', true).attr('disabled','disabled');
            }

            if (total_hidden > 0){
                $('.totals .hidden_packages').text('' +
                    ' (' + total_hidden + ' hidden)' +
                    '');
            }
            else {
                $('.totals .hidden_packages').text('');
            }

            if (total_matched === 0){
                $('#no_match').show();
            }
            else {
                $('#no_match').hide();
            }

            if ((total_matched === 0) || (total_selected === total_pkgs) || (matched_selected === total_matched)){
                $('.buttons .select_all').prop('disabled', true).attr('disabled','disabled');
            }
            else {
                $('.buttons .select_all').prop('disabled', false).removeAttr('disabled');
            }

        }


        // do we filter before or after they're on the DOM?
        function filterPackages( _matches ){

            var exclusive = [];
            var inclusive = [];
            var matches = [];
            var relations = [];

            var selector = '.filter.options';

            var $selects = $filters.find('select' + selector);

            $selects.each(function(){

                var $select = $(this);

                // value of THIS select
                var sel_val = $select.val();
                // push this to use for filtering packages
                exclusive.push(sel_val);

                // let's start filtering the filters

                // this select's selected option
                var $selected = $select.find('option:selected');

                // this selected option's relations
                var sel_rel = $selected.data('related').split(',');

                var temp=[];

                if (relations.length > 0){
                    sel_rel.map(function(rel){
                        if (relations.indexOf(rel) !== -1){
                            temp.push(rel);
                        }
                    });
                    relations = temp;
                }
                else {
                    relations = sel_rel;
                }

                // what do we do next?
                var $next = $select.nextAll(selector).first();

                if ($next.is('select.select')){
                    // disable ALL options so we can enable only the ones we want
                    $next.find('option').prop('disabled',true);
                    $('option[value="' + relations.join('"], option[value="') + '"]').prop('disabled',false);
                    $next.find('option').each(function(){
                        var $option = $(this);
                        if ($option.is(':disabled') && $option.is(':selected')){
                            $next.find('option').first().not(':disabled').prop('selected',true);
                        }
                    });
                    $next.triggerHandler('change');
                }
                if ($next.is('ul.multi')){
                    $next.find('a.multi.filter.button').not('.child').addClass('hidden');
                    $('a.multi' + ((relations.length>0) ? '.' + relations.join(', a.multi.') : "")).removeClass('hidden');
                    $next.find('a.multi.filter.button.hidden').removeClass('active');
                }

            });

            // if there are active filters, just match those
            if ($filters.find('a.multi.active').not('.child').length){
                $filters.find('a.multi.active').not('.child').each(function(){
                    var $this = $(this);
                    var $multi = $this.closest('ul.multi');
                    var keyword = $this.data('keyword');
                    if ($multi.find('a.child.active').length){
                        $multi.find('a.child.active').each(function(){
                            inclusive.push(keyword + '.' + $(this).data('keyword'));
                        });
                    }
                    else {
                        inclusive.push(keyword);
                    }
                });
            }
            // otherwise match them all, excluding children
            else {
                $filters.find('a.multi').not('.child').each(function(){
                    inclusive.push($(this).data('keyword'));
                });
                $filters.find('a.multi.child').removeClass('active');
            }

            inclusive.map(function(i){
                matches.push('.package.' + i + '.' + exclusive.join('.'));
            });

            $packages.find('.package').removeClass('matched');
            $packages.find(matches.join(', ')).addClass('matched');

            if (!$packages.find('.package.matched').length //&&
                ){
                $packages.find('.no_matches').show();
            }
            else {
                $packages.find('.no_matches').hide();
            }

            showHidePackageTypeSelect();

            calculateTotals();
        }

        function showHidePackageTypeSelect() {
            // Only show Package Type selector for certain projects,
            // session types, and processing levels

            var projects = ["HCP_900"]
            var sessionTypes = ["MRI"]
            var processingLevels = ["preprocessed", "analysis"]

            var selectedSessionType = $('#select_session_types').find('option:selected').val();
            var selectedLevel = $('#select_processing_level').find('option:selected').val();
            var selectedPackageType = $('#select_package_types').find('option:selected').val();

            if (projects.indexOf(XNAT.data.context.projectName) > -1
                  && sessionTypes.indexOf(selectedSessionType) > -1
                  && processingLevels.indexOf(selectedLevel) > -1) {
                $('#select_package_types').show().prev('p').show();
            } else {
                $('#select_package_types').hide().prev('p').hide();
            }

            // Show Extension banner only when Package Type is Extension
            if (selectedPackageType === "patch") {
                $('#package-note').slideDown(200);
            } else {
                $('#package-note').slideUp(200);
            }
        }


        function dpResetFilters(){

            $filters.find('select.filter.options').each(function(){
                var $this = $(this);
                var _default = $this.find('option.default').val();
                $this.val(_default).triggerHandler('change');
            });

            // if the filters have been loaded once, we want to remove 'active' from 'multi' filters
            if (window.dpFiltersLoaded === true){
                $filters.find('a.multi.filter.button').removeClass('active');
                $filters.find('ul.multi.options div.child').addClass('hidden');
            }
            else {
                window.dpFiltersLoaded = true;
            }

            filterPackages();
        }
        // make 'dpResetFilters' available globally as 'dpResetFilters'
        window.dpResetFilters = dpResetFilters;










        //////////////////////////////////////////////////
        // WORKING WITH TEMPLATES AND PAGE ELEMENTS:
        //////////////////////////////////////////////////

        (function(fn){

            // get HTML from templates container and define templates for
            // each item that will use a template
            _fn.dpGetTemplates = function( _container, _callback ){

                _container = _container || $('#dp-templates');

                var $templates = (_container.jquery) ? _container : $(_container);

                function getHTML(selector){
                    return $templates.find(selector).html();
                }

                _dp.templates.filterLeft = getHTML('.filter-left-template');
                //_dp.templates.selectOption = getHTML('.filter-select-option-template');
                //_dp.templates.multiOption = getHTML('.filter-multi-option-template');
                _dp.templates.noMatch = getHTML('#no_match');
                _dp.templates.package = getHTML('.package-template');
                _dp.templates.packageKeywords = getHTML('.package-keywords-template');
                // should probably just use the string as the join value when building the list of keywords
                _dp.templates.packageKeywordsSeparator = '<b>|</b>';
                _dp.templates.packagePreviews = getHTML('.package-previews-template');

                if (isFunction(_callback)) _callback(_dp.templates);

            };

            _fn.showElements = function(_speed){

                _speed = _speed || 200;

                // if there's only one filter and its type is "text"
                // just show ALL of the packages
                if (dp.filters.length === 1 && dp.filters[0].type === 'text'){
                    $packages.find('.package').addClass('matched');
                    $filters.find('button.reset_filters, hr').hide();
                }
                // otherwise run the reset function which
                // will run the filter function which
                // will run the calculate function
                else {
                    dpResetFilters();
                }

                $('#loading_packages').fadeOut(_speed/2);
                $filters.fadeIn(_speed);
                $packages.fadeIn(_speed);
                $('#download_box').fadeIn(_speed);
                $('.buttons .download, .buttons .reset').prop('disabled',true).attr('disabled','disabled');
            };

        })(dp.fn);









        //////////////////////////////////////////////////
        // WORKING WITH FILTERS OBJECT:
        //////////////////////////////////////////////////

        (function(fn){

            // putting these in an IIFE for organization

            function dataAttr(option, prop){
                var html='';
                if ( option.hasOwnProperty(prop) && option[prop].length > 0 ){
                    html += '' +
                        ' data-' + prop + '="' + option[prop].join(',') + '"';
                }
                return html;
            }


            _fn.buildSelectOptions = function( _filter ){
                var html = '';
                var options = _filter.options;

                function loopSelectOptions(option){

                    var html='';
                    var classes, selected, selArray;
                    html += '<option title="' + option.label + '" value="' + option.name + '"';
                    classes=[_filter.name, option.name];
                    selected = window.dpSelected || '';

                    if ( selected > '' ){
                        selArray = selected.split(',');
                        if (selArray.indexOf(option.name) > -1){
                            classes.push('default');
                        }
                    }
                    else if ( option.hasOwnProperty('isDefault') && option['isDefault'] === true ){
                        classes.push('default');
                    }

                    html += ' class="' + classes.join(' ') + '"';
                    html += dataAttr(option, 'related');
                    html += dataAttr(option, 'children');
                    html += '>' + option.label + '</option>';

                    return html;
                }

                var len = options.length;

                for ( var i = 0; i < len; i++ ){
                    html += loopSelectOptions(options[i]);
                }
                return html;
            };


            _fn.buildMultiOptions = function( _filter ){

                var html = '';
                var isChild = false;

                var classnames = function(){
                    return [ 'filter', _filter.type, _filter.name ];
                };

                // if it's a child
                if (_filter.type.split(' ').indexOf('child') > -1){
                    isChild = true;
                }
                else {
                    html += '<ul class="options ' + classnames().join(' ') + '">';
                }

                var options = _filter.options;
                var len = options.length;

                function loopMultiOptions(option){

                    var html='';
                    var classes, selected, selArray;

                    classes = classnames();
                    classes.push(option.name + ' button');
                    selected = window.dpSelected || '';

                    if ( selected > '' ){
                        selArray = selected.split(',');
                        if (selArray.indexOf(option.name) > -1){
                            classes.push('default active');
                        }
                    }
                    else if ( option.hasOwnProperty('isDefault') && option['isDefault'] === true ){
                        classes.push('default active');
                    }

                    html += '' +
                        '<li class="option"><a' +
                        ' href="javascript:"' +
                        ' id="' + _filter.name + '_' + option.name + '"' +
                        ' class="' + classes.join(' ') + '"' +
                        ' data-keyword="' + option.name + '"' +

                        dataAttr(option, 'related') +

                        '>' + option.label + '</a>';

                    if ( option.hasOwnProperty('children') && option['children'].length > 0 ){
                        var children = option['children'];
                        var childrenLen = children.length;
                        for ( var child, ii = 0; ii < childrenLen; ii++ ){
                            child = children[ii];
                            html += '<div class="multi child hidden">' +
                                '<p class="filter title">' + child.label + '</p>' +
                                '<ul class="multi options">';
                            html +=  _fn.buildMultiOptions(child);
                            html += '</ul>' +
                                '</div>';
                        }
                    }
                    html += '' +
                        '</li>';
                    return html;
                }

                for ( var i = 0; i < len; i++ ){
                    html += loopMultiOptions(options[i]);
                }
                if (isChild === false){
                    html += '</ul>';
                }
                return html;
            };


            _fn.buildTextOptions = function ( _filter ) {

                var html = '<ul class="text multi options">';
                var options = _filter.options;

                function loopTextOptions(option){
                    return '' +
                        '<li title="' + option.label + '"' +
                        ' class="' + ['text', 'option', _filter.name, option.name].join(' ') + '"' +
                        ' data-keyword="' + option.name + '"' +
                        //dataAttr(option, 'related') +
                        '>' + option.label + '</li>';
                }

                var len = options.length;

                for ( var i = 0; i < len; i++ ){
                    html += loopTextOptions(options[i]);
                }

                html += '</ul>';

                return html;

            };


            _fn.buildFilterBoxes = function ( _filter ){

                var classes = [_filter.type, _filter.name];
                var options = '';

                var filterType = _filter.type.split(' ');

                if (filterType.indexOf('select') !== -1){
                    classes.push('filter options');
                    //options += '<input type="hidden" class="select filter data" data-for="' + _filter.name + '" data-selected="' + getDefaults(_filter)[0] + '" value="' + getDefaults(_filter)[0] + '">';
                    //options += '<input type="hidden" class="select filter related" data-for="' + _filter.name + '" value="' + _filter.related.join(',') + '">';
                    options += '<select id="select_' + _filter.name + '" class="' + classes.join(' ') + '" name="' + _filter.name + '" title="' + _filter.label + '">';
                    options += _fn.buildSelectOptions( _filter );
                    options += '</select>';
                }
                else if (filterType.indexOf('multi') !== -1){
                    //options += '<input type="hidden" class="multi filter data" data-for="' + _filter.name + '" data-selected="' + getDefaults(_filter).join(',') + '" value="' + getDefaults(_filter).join(',') + '">';
                    _filter.options.classnames = ['button', _filter.name];
                    options += _fn.buildMultiOptions( _filter );
                }
                else if (_filter.type === 'text') {
                    options += _fn.buildTextOptions( _filter );
                }

                var html = '';

                //html += '<div class="filter_container">';

                html +=
                    _dp.templates.filterLeft.
                        replace( /__FILTER_LABEL__/g, _filter.label).
                        //replace( /__CLASSNAMES__/g, classes.join(' ')).
                        replace(/__FILTER_OPTIONS__/g, options);

                //html += '</div>';

                return html;

            };


            // MASTER FUNCTION FOR "filters" OBJECT
            _fn.processFilters = function( _filters ){
                var html = '';
                var len = _filters.length;
                for (var filter, i = 0; i < len; i++){
                    filter = _filters[i];
                    // save ALL filter names
                    if ( filter.hasOwnProperty('name') ){
                        pushUnique(filter.name, _dp.filter_names);
                    }
                    html += _fn.buildFilterBoxes( filter );
                }
                $('#filters').find('> div').prepend(html);
            };
        })(dp.fn);





        //////////////////////////////////////////////////
        // WORKING WITH PACKAGES OBJECT:
        //////////////////////////////////////////////////

        (function(fn){


            _fn.getPackageIDs = function( _packages ){

                var len = _packages.length;
                var package_ids = [];

                for ( var pkg, i = 0; i < len; i++ ){
                    pkg = _packages[i];

                    if (pkg.hasOwnProperty('id')){
                        package_ids.push(pkg.id);
                    }
                }

                return package_ids;

            };


            _fn.buildPackageKeywords = function( _keywords ){
                var keywords = [];
                var len = _keywords.length;
                for (var keyword, i=0; i < len; i++){
                    keyword = _keywords[i];
                    keywords.push('' +
                        //'<a href="javascript:"' +
                        '<b' +
                        ' class="' + keyword + ' keyword filter' + '"' +
                        ' data-option="' + keyword + '"' +
                        '>' + keyword.replace(/_/g,' ') +
                        '</b>' +
                        //'</a>' +
                        '');
                }
                return 'keywords: ' + keywords.join(', ');
            };


            _fn.buildPackage = function( pkg ){
                var keywords = convertKeywords(pkg.keywords);
                var keywords_split = keywords.split(' ');
                var classnames=['package'];
                var previews=''; // previews are generated on-the-fly after clicking "i" icon
                var filters=[];
                keywords_split.map(function(val, i, arr){
                    classnames.push(val);
                });
                _dp.keywords.all = mergeArrays(keywords_split, _dp.keywords.all);
                if (pkg.subjects_ok === 0) classnames.push('disabled');
                return _dp.templates.package.
                    replace(/__PACKAGE_KEYWORDS__/g, keywords_split.join(',')).
                    replace(/__PACKAGE_ID__/g, pkg.id).
                    replace(/__CLASSNAMES__/g, classnames.join(' ')).
                    replace(/__PACKAGE_LABEL__/g, pkg.label).
                    replace(/__SUBJECTS_COUNT_OK__/g, pkg.subjects_ok).
                    replace(/__SUBJECTS_COUNT_OK_FORMATTED__/g, addCommas(pkg.subjects_ok)).
                    replace(/__SUBJECTS_COUNT_ALL__/g, pkg.subjects_total).
                    replace(/__SUBJECTS_COUNT_ALL_FORMATTED__/g, addCommas(pkg.subjects_total)).
                    replace(/__PACKAGE_FILE_COUNT__/g, pkg.count).
                    replace(/__PACKAGE_FILE_COUNT_FORMATTED__/g, addCommas(pkg.count)).
                    replace(/__PACKAGE_FILE_SIZE__/g, pkg.size).
                    replace(/__PACKAGE_FILE_SIZE_FORMATTED__/g, sizeFormat(pkg.size)).
                    replace(/__PACKAGE_DESCRIPTION__/g, pkg.description).
                    // the following use another template
                    replace(/___PACKAGE_KEYWORD_FILTERS___/g, _fn.buildPackageKeywords(pkg.keywords)).
                    replace(/___PACKAGE_PREVIEWS___/g, previews)
            };


            _fn.processPackages = function( pkgs ){
                var html='';
                var len = pkgs.length;
                html += _dp.templates.noMatch;
                for ( var pkg, i = 0; i < len; i++ ){
                    pkg = pkgs[i];
                    html += _fn.buildPackage( pkg );
                }
                // very last step
                $packages.html(html);
            };

        })(dp.fn);










        //////////////////////////////////////////////////
        // MAIN INIT FUNCTION
        //////////////////////////////////////////////////

        dp.fn.init = _fn.init = function(){

            _fn.dpGetJSON({
                //url: _url,
                //async: true,
                //interval: 100,
                done: function(data){

                    dp.filters = data.filters;
                    dp.packages = data.packages;

                    _fn.processFilters(data.filters);
                    _fn.processPackages(data.packages);

                    dp.keywords = _dp.keywords;
                    dp.filter_names = _dp.filter_names;

                    // the showElements method calls dpResetFilters()
                    // which calls filterPackages() after resetting
                    // THEN it fades in the package stuff
                    _fn.showElements();

                    closeModalPanel("DownloadPackagesModal");

                },
                fail: function(data, status, error){
                    if (window.console && window.console.log){
                        console.log('status: ' + status);
                        console.log('error: ' + error);
                    }
                    closeModalPanel("DownloadPackagesModal");
                    xModalMessage('Error','Status: ' + status + '. <br><br> Error: ' + error + '.');
                }
            });

        };
        //////////////////////////////////////////////////









        //////////////////////////////////////////////////
        // CALL THE MAIN FUNCTIONS
        //////////////////////////////////////////////////

        // put the HTML for the templates in the _dp.templates object
        _fn.dpGetTemplates();

        // last but not least - the main 'init()' function
        _fn.init();

        //////////////////////////////////////////////////










        //////////////////////////////////////////////////
        // EVENT FUNCTIONS
        //////////////////////////////////////////////////

        $filters.on('mouseup', 'a.multi.filter.button', function(){
            var $this = $(this);
            var keyword = $this.data('keyword');
            if ($this.hasClass('active')){
                $this.removeClass('active');
                $this.next('div.child').addClass('hidden');
                $this.next('div.child').find('a.multi.filter.button').removeClass('active');
            }
            else {
                $this.addClass('active');
                $this.next('div.child').removeClass('hidden');
            }
            filterPackages(/*'.' + keyword */);
            return false;
        });


        $filters.on('change', 'select.filter.options', function(){
            $filters.find('ul.multi.options div.child').addClass('hidden');
            filterPackages();
            return false;
        });


        $packages.on('mouseover', '.add_to_queue', function () {
            $(this).closest('.package').find('.add_to_queue').addClass('hover');
        });
        $packages.on('mouseout', '.add_to_queue', function () {
            $(this).closest('.package').find('.add_to_queue').removeClass('hover');
        });


        $packages.on('click', '.add_to_queue', function() {
            var $this_package = $(this).closest('.package');
            // if it's not 'disabled' then add to queue

            if ($this_package.find('.subjects_ok').text() === '0'){
                noneValid('subjects','package');
                return false;
            }

            if (!($this_package.hasClass('disabled'))){
                if ($this_package.hasClass('selected')){
                    $this_package.removeClass('selected');
                }
                else {
                    $this_package.addClass('selected');
                }
                $this_package.find('.add_to_queue').removeClass('hover');
                calculateTotals();
            }
            else {
                noneValid('subjects','package');
            }
            return false;
        });


        $packages.on('click', '.expand_details', function() {

            var $this_pkg = $(this).closest('.package');
            var pkg_id = $this_pkg.attr('id');

            // if the subject preview is not loaded, get the stuff
            if (!($this_pkg.find('.pkg_preview').hasClass('loaded'))) {
            	var formData = {};
            	formData.project=curr_proj;
            	formData.package=pkg_id;
            	formData.subjects=csvSubjects;
            	formData.view="subjects";
            	formData.XNAT_CSRF=csrfToken;
                $.ajax({
                    type : "POST",
                    //url : serverRoot + "/admin/download?project="+curr_proj+"&package=" + pkg_id + "&subjects=" + csvSubjects + "&view=subjects&XNAT_CSRF=" + csrfToken,
                    //data : "",
                    url : serverRoot + "/admin/download",
                    data : formData,
                    dataType : "json",
                    context : this,
                    success : function(json) {
                        var html='';
                        $.each(json.subjects, function(){
                            var
                                sub_id     = this.id,
                                sub_status = this.status,
                                sub_count  = this.file_count,
                                sub_size   = this.size
                                ;
                            html += _dp.templates.packagePreviews.
                                replace(/__SUBJECT_ID__/g, sub_id).
                                replace(/__SUBJECT_STATUS__/g, sub_status).
                                replace(/__SUBJECT_FILE_COUNT__/g, sub_count).
                                replace(/__SUBJECT_FILE_COUNT_FORMATTED__/g, addCommas(sub_count)).
                                replace(/__SUBJECT_FILE_SIZE__/g, sub_size).
                                replace(/__SUBJECT_FILE_SIZE_FORMATTED__/g, sizeFormat(sub_size));
                        });
                        $this_pkg.find('.preview_details').append(html);
                        $this_pkg.find('.pkg_preview').addClass('loaded').slideToggle('fast');

	                if ($(this).hasClass('active')){
	                    $(this).removeClass('active');
	                }
	                if (!$(this).hasClass('open')){
	                    $(this).addClass('open');
	                }

                    },

                    error : function(jqXHR) {
                        if(404 === jqXHR.status) {
                            noneValid('subjects','package');
                        } else {
                            var modal_content = "Error " + jqXHR.status + ": " + jqXHR.responseText ;
                            xModalMessage('Error',modal_content);
                        }
                    }
                });

                if ($(this).hasClass('active')){
                    $(this).removeClass('active');
                }
                else {
                    $(this).addClass('active');
                }
                if ($(this).hasClass('open')){
                    $(this).removeClass('open');
                }

            }
            // if the subject preview is already loaded, just show it
            else {
                $this_pkg.find('.pkg_preview').slideToggle('fast');
                if ($(this).hasClass('open')){
                    $(this).removeClass('open');
                }
                else {
                    $(this).addClass('open');
                }
            }
        });


        $('.buttons .select_all').click(function(){
            $('.package.matched').not('.disabled').addClass('selected');
            calculateTotals();
        });


        $('.buttons .reset').click(function() {
            $('.package.selected').removeClass('selected');
            $('.totals .total_count').data('count-total', 0).text('0');
            $('.totals .total_size').data('size-total', 0).text('0 B');
            calculateTotals();
        });


        $container.on('click','.download_packages_button',function() {
            var total_packages = parseInt($total_packages.data('count-packages'), 10);
            var total_count = parseInt($total_count.data('count-total'), 10);
            var total_size = parseInt($total_size.data('size-total'), 10);
            var $package_selected = $('.package.selected/*.matched*/');
            var selected_packages = [];
            var selected_package_labels = [];
            if ($package_selected.length) {
                $package_selected.each(function() {
                    var this_id = $(this).attr('id');
                    var this_label = $(this).find('.package_title').text(); // using text from package label, no need for 'package-label' attribute
                    selected_packages.push(this_id);
                    selected_package_labels.push(this_label);
                });
                $("#packageIdCsv").val(selected_packages.join(','));

                var total_packages_display, total_files_display ;
                if (total_packages === 1){
                    total_packages_display = '1 package' ;
                }
                else {
                    total_packages_display = total_packages + ' packages' ;
                }
                if (total_count === 1){
                    total_files_display = '1 file' ;
                }
                else {
                    total_files_display = '<b style="font-weight:bold;color:#080;">' + addCommas(total_count) + ' total files</b>' ;
                }

                var dialog_content =
                    "<h3>You've selected " + total_packages_display + " for download:</h3>" +
                    '<ul class="package_list">' +
                    '' + '<li>' + selected_package_labels.join('</li><li>') + '</li>' +
                    '</ul>' +
                    '<p>This download has ' + total_files_display + ', with a total size of <b style="font-weight:bold;color:#080;">' + sizeFormat(total_size) + '</b>.</p>' +
                    '';

                xModalOpenNew({
                    //width:     550,
                    //height:    350,
                    id:      'confirm_download',
                    title:    'Confirm Download',
                    content:   dialog_content,
                    footerContent: '<b style="display:block;position:absolute;left:2px;top:18px;font-weight:normal;font-size:14px;color:#666;white-space:nowrap;">Proceed with download?</b>',
                    okLabel: 'Download Now',
                    okAction: function(){
                        $("#downloadPackagesForm").submit();
                    },
                    okClose: 'no'
                });
            }
        });


        // show the filter criteria from previous page
        $('#subject_filters').find('a').click(function(){
            $(this).closest('div').find('ul').slideToggle('fast');
        });


        $container.on('click', '.reset_filters', function(){
            dpResetFilters();
        });

    });


})( HCP.app.downloadPackages, jQuery );

