
if (typeof CDB === "undefined") var CDB = {};

CDB.aspera = {
	scriptsLoaded: 0
};

var currentProto;
if (window.location.protocol.indexOf('file') != -1) {
	currentProto = "http:";
} else {
	currentProto = window.location.protocol;
}

CDB.aspera.cloudfrontPath = currentProto + "//d3gcli72yxqn2z.cloudfront.net";
CDB.aspera.installerPath = CDB.aspera.cloudfrontPath + "/connect";

CDB.aspera.loadAsperaScripts = function() {
	// Let's get an updated version of scripts when on login page, otherwise, let's get cached version
    	var isLogin = (window.location.href.indexOf("\/Login.vm")>=0); 
	$.ajax({
		type: "GET",
		url:  CDB.aspera.cloudfrontPath + ((isLogin) ? "?_=" + (new Date()).getTime().toString() : ""),
		async: false,
		success: function() {
			$.ajaxSetup({ cache: !isLogin });
			console.log("loading remote aspera scripts");
			$.getScript(CDB.aspera.installerPath + "/asperaweb-2.js", function(data, textStatus, jqxhr) { CDB.aspera.checkScriptsLoaded(); });
			$.getScript(CDB.aspera.installerPath + "/v4/asperaweb-4.js", function(data, textStatus, jqxhr) { CDB.aspera.checkScriptsLoaded(); });
			$.getScript(CDB.aspera.installerPath + "/connectversions.js", function(data, textStatus, jqxhr) { CDB.aspera.checkScriptsLoaded(); });
			$.getScript(CDB.aspera.installerPath + "/connectinstaller-2.js", function(data, textStatus, jqxhr) { CDB.aspera.checkScriptsLoaded(); });
			$.ajaxSetup({ cache: false });
		},
		error: function() {
			$.ajaxSetup({ cache: !isLogin });
			$.getScript(serverRoot + "/scripts/aspera/asperaweb-2.js", function(data, textStatus, jqxhr) { CDB.aspera.checkScriptsLoaded(); });
			$.getScript(serverRoot + "/scripts/aspera/asperaweb-4.js", function(data, textStatus, jqxhr) { CDB.aspera.checkScriptsLoaded(); });
			$.getScript(serverRoot + "/scripts/aspera/connectversions.js", function(data, textStatus, jqxhr) { CDB.aspera.checkScriptsLoaded(); });
			$.getScript(serverRoot + "/scripts/aspera/connectinstaller-2.js", function(data, textStatus, jqxhr) { CDB.aspera.checkScriptsLoaded(); });
			$.ajaxSetup({ cache: false });
		}
	});
}

CDB.aspera.checkScriptsLoaded = function() {
	    CDB.aspera.scriptsLoaded++;
	    if (CDB.aspera.scriptsLoaded >= 4) {
	    	console.log("CDB.aspera.scriptsLoaded - " + CDB.aspera.scriptsLoaded + " - Script load complete");
		$(document).trigger("asperaScriptsLoaded");
	    }
}

CDB.aspera.initializeInstaller = function() {
    if (typeof CDB.aspera.connectInstaller != "undefined") {
        return;
    }
    CDB.aspera.connectInstaller = new AW.ConnectInstaller(CDB.aspera.installerPath);
    CDB.aspera.connectInstaller.init({
    	install: function() {
                    console.log("Aspera check");
                    // Perform the aspera check
                    var asperaWeb = new AW4.Connect();
                    var statusEventListener = function (eventType, data) {
                        switch(data) {
                          case AW4.Connect.STATUS.INITIALIZING: {
                              console.log("Aspera Status:  Launching");
                              break;
                          };
                          case AW4.Connect.STATUS.FAILED: {
                              console.log("Aspera Status: Not running");
                              showAsperaPluginWarning();
                              break;
                          };
                          case AW4.Connect.STATUS.OUTDATED: {
				var modalOpts = {
					width: 420,
					height:  210,
					id: "xmodal-outdated-plugin",	
					title:  "Outdated Plugin",
					content: "The browser check process reports that your Aspera plugin is outdated.  Aspera inegration likely will not work with your " +
							"current version of the plugin.  Please update your plugin.",
 					ok: 'show',
					okLabel: 'Update Plugin',
					okAction: function(modl){
						dismissAsperaMessage(false);
				          	installAsperaPlugin();
			 		},
					okClose: true,
					cancel: 'Cancel',
					cancelLabel: 'Remind Me Later',
					cancelAction: function(modl){
						dismissAsperaMessage(false);
			 		},
					closeBtn: 'hide'
			 	};
				xmodal.open(modalOpts);
				showAsperaPluginWarning();
                              console.log("Aspera Status:  Not up to date");
                              break;
                          };
                          case AW4.Connect.STATUS.RUNNING: {
                              console.log("Aspera Status:  Running");
                              hideAsperaPluginMessage(true);
                              break;
                          }
                          default:
                              console.log("Aspera Status:  Other");
                          ;
                        }
                    };
                    asperaWeb.addEventListener(AW4.Connect.EVENT.STATUS, statusEventListener);
                    asperaWeb.initSession();
        },
    	minVersion: '3.6'
    });
}

